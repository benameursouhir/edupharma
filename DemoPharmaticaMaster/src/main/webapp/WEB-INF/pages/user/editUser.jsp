<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>organisation - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/datepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/colorpicker.css" />

		<!-- fonts -->

		
		<script type="text/javascript">

		
		 function test(str) {
             return /^ *[0-9]+ *$/.test(str);
         }
			function bonmail(mailteste)

			{
				var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');

				return(reg.test(mailteste));
			}
			 function telmobil(str) {
		            return /^(2|9|5) *[0-9]{7} *$/.test(str);
		        }
			 function telfix(str) {
		            return /^(7|3) *[0-9]{7} *$/.test(str);
		        }
function valider ( )
{
    if ( document.formulaire.nom.value == "" )
    {
        alert ( "Veuillez entrer votre nom !" );
        valid = false;
return valid;
    }
    if ( telmobil(document.formulaire.telmobile.value) == "" )
    {
        alert ( "Veuillez entrer votre telmobile !" );
        valid = false;
return valid;
    }

    if (telfix( document.formulaire.fax.value) == "" )
    {
        alert ( "Veuillez entrer votre fax !" );
        valid = false;
return valid;
    }

    if ( telfix(document.formulaire.telfix.value) == "" )
    {
        alert ( "Veuillez entrer votre telfix!" );
        valid = false;
return valid;
    }
    if ( bonmail(document.formulaire.email.value) == "" )
    {
        alert ( "Veuillez entrer votre email!" );
        valid = false;
return valid;
    } 

    if ( document.formulaire.password.value == "" )
    {
        alert ( "Veuillez entrer votre mot de passe!" );
        valid = false;
return valid;
    } 
    if ( document.formulaire.type.value == "" )
    {
        alert ( "Veuillez entrer votre type!" );
        valid = false;
return valid;
    } 


    
    
    if(document.formulaire.adr.value == ""){
    	if(document.formulaire.nomRue.value == ""){
      		 alert ( "Veuillez entrer votre nom de rue " );
      	        valid = false;
      	   return valid;
          	}
    	if(document.formulaire.ville.value == ""){
    		 alert ( "Veuillez entrer votre ville!" );
    	        valid = false;
    	   return valid;
        	}
    	if(document.formulaire.codePostal.value == ""){
   		 alert ( "Veuillez entrer votre code postal!" );
   	        valid = false;
   	   return valid;
       	}
    	
    	       

    }
    
   
  
    
  
}
</script>
		
	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>

		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/organisationn"%>">Organisation</a>
							</li>
							<li class="active">Modifier</li>
						</ul><!-- .breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
							   Organisation
								<small>
									<i class="icon-double-angle-right"></i>
									Modifier
								</small>
								<small>
								<p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font></p>
                    </small> 
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
 <fieldset  name="Update organisation" style="width:350px">
								<form:form   action="${pageContext.request.contextPath}/updateUser/${organisation.orgId}" method="Post" commandName="organisation" class="form-horizontal" name="formulaire" onsubmit="return valider()" >
									<div class="form-group" style="width: 700px;">
									
                	<form:label class="col-sm-3 control-label no-padding-right" path="orgId"></form:label>
                	<div class="col-sm-9">
                    <form path="orgId"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>

							
									<table>
									<tr>
									<td>
									<div class="form-group">
										
                	<form:label class="col-sm-3 control-label no-padding-right" path="nom">nom</form:label>
                	<div class="col-sm-9">
                    <form:input path="nom"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
</td>
<td>
									<div class="space-4"></div>
									
									<div class="form-group">
									 <table><tr><td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="telmobile" >Telphone mobile:</form:label>
                	
                	</td> <td>
                	<br>
                    <p><font color="red" size="2" face="Georgia, Arial" >
                    <c:out value="${message3}"/></font></p></td></tr></table>
                    <div class="col-sm-9">
                    <form:input path="telmobile" style="width:200px" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 
           	</td>
           	<td>	
									<div class="form-group">
									<table><tr><td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="fax">Numero fax</form:label>
                	</td> <td>
                	<br>
                    <p><font color="red" size="2" face="Georgia, Arial" >
                    <c:out value="${message4}"/></font></p></td></tr></table>
                	<div class="col-sm-9">
                    <form:input path="fax"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 
</td>
</tr>
<tr>
<td>
									<div class="space-4"></div>
							
<div class="form-group">
	<table><tr><td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="telfix">tel fix:</form:label>
                	</td> <td>
                	<br>
                    <p><font color="red" size="2" face="Georgia, Arial" >
                   </td></tr></table>
                	<div class="col-sm-9">
                    <form:input path="telfix"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>

			</td>
			<td>
									<div class="space-4"></div>
									

									<div class="form-group">
								
                	<form:label class="col-sm-3 control-label no-padding-right" path="type">Type:</form:label>
                	
                	<div class="col-sm-9">
                   
											
												<select name="type">
									<option value="${organisation.type }">${organisation.type }</option>
									<option value="">--------</option>
                                    <option value="pharmacie">  Pharmacie  </option>
                                    <option value="fournisseur"> Fournisseur </option>
                                   
  
                                              </select>
										</div>
           		 </div>
                           
                   </td>
                   <td>
                        
									<div class="space-4"></div>



							
<div class="form-group">
<table><tr><td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="email">Email:</form:label>
                	</td> <td>
                	<br>
                    <p><font color="red" size="2" face="Georgia, Arial" >
                    <c:out value="${message5}"/></font></p></td></tr></table>
                	<div class="col-sm-9">
                    <form:input path="email"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>




           		 </td>
           		 </tr>
           		 
          <tr>
<td>
									

			</td>
			<td>
									<div class="space-4"></div>
							
<div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="password">Mot de passe:</form:label>
                	<div class="col-sm-9">
                    <form:input path="password" type="password"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
                           
                   </td>
                   <td>
                        
						 		 </td>
           		 </tr> 		 
           		 
           		 
           		 
           		 
           		 <tr>
           <td colspan="3">
           <div class="space-4"></div>
           <div class="page-content">
						<div class="page-header">
						<h4>
       Adresse:</h4>
           </div></div>
           </td>
           </tr> 
           		<tr>
<td>
									<div class="space-4"></div>
							
<div class="form-group">
                	<label class="col-sm-3 control-label no-padding-right" >Nom de rue:</label>
                	<div class="col-sm-9">
                    <input  type="text" name="nomRue" id="nomRue" class="col-xs-10 col-sm-5 " value="${organisation.adresse.nomRue}"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>

			</td>
			<td>
									<div class="space-4"></div>
									

									<div class="form-group">
                	<label class="col-sm-3 control-label no-padding-right" >Ville:</label>
                	<div class="col-sm-9">
                    <input  type="text" name="ville" id="ville" value="${organisation.adresse.ville}" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
                           
                   </td>
                   <td>
                        
									<div class="space-4"></div>
								                                     
									
									<div class="form-group">
                	<label class="col-sm-3 control-label no-padding-right" >Code postal:</label>
                	<div class="col-sm-9">
                   <input  type="text" name="codePostal" id="codePostal" value="${organisation.adresse.codePostal}" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 </tr>
            </table>
            
										<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" type="submit" value="Submit" name="modifier">
												<i class="icon-ok bigger-110"></i>
												Modifier
												
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn btn-danger"" type="submit">
												<i class="icon-reply icon-only"></i>
												Annuler et retourner
											</button>
										</div>
									</div>
 	
									

										
                             
     </form:form></fieldset>
							
									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->				
				
		<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

		

		<!-- ace scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>	
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
