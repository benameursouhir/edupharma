<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>Employe - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" rel="stylesheet"  />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />

		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/core/demos.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.all.css">
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery-1.10.2.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.core.js"></script>
	
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.button.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.position.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.menu.js"></script>
		
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.tooltip.js"></script>


	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>
		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/produits"%>">Produit</a>
							</li>
							<li class="active">Liste</li>
						</ul><!-- .breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
								Produit Ajoutés
								<small>
									<i class="icon-double-angle-right"></i>
									Liste
								</small>
								 <small>
        <p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font></p> </small>
							</h1>
						</div><!-- /.page-header -->

						
											
											
			
<div class="row-fluid">
		<div class="col-xs-12">
    <c:if test="${!empty page}">
               
				<div class="table-header">
										Listes des produits
										</div>
										
			<table class="table table-condensed table-bordered">
				<thead>
													 <tr>
                        <th>Id</th>
                        <th>refProduit</th>
                        <th>Libelle</th>
                         <th>forme</th>
                          
                          <th>details</th>
                          <c:if test="${produit.confirme ==false}">
                          <th>Modifier</th>
                     
                         <th>Supprimer</th>
                      </c:if>
                    </tr>
				</thead>

												
											<tbody>
											<c:forEach items="${page.content}" var="produit" varStatus="loopStatus">
											<tr  class="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}">
											
														

														<td class=" ">
															${produit.prodId}
														</td>
														<td class=" ">${produit.refP}</td>
														<td class="hidden-480 ">${produit.libelle}</td>
														<td class="center  sorting_1">
															${produit.forme}
														</td>
														<td>
														<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="detailsProd/${produit.prodId}" method="get">
								
								<button class="btn btn-xs btn-success">
							    <i class="icon-ok bigger-120"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
						</form>
					</div>	
														</td>
														
														
						<c:if test="${produit.confirme ==false}">
														<td class=" ">
					<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="modifprod/${produit.prodId}" method="get">
								
								<button class="btn btn-xs btn-info">
							    <i class="icon-edit bigger-120"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
								                                          
								 
								 
								 
								 
                         
						</form>
					</div>
												
					</td>
							
					
					
					
					

					<td class="hidden-480 ">
														
						<form name="suppression" onSubmit="return Demande_verification()" action="deleteProdAjout/${produit.prodId}" method="post">
							<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                               
							         <button class="btn btn-xs btn-danger">
																	<i class="icon-trash bigger-120"></i>
								
																</button>
							</div>
							<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																	<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
																				<span class="red">
																					<i class="icon-trash bigger-120"></i>
																				</span>
																			</a>
																	</li>
																	</ul>
								</div>
                                </div>								
						</form>
					</td>

						</c:if>								
				</tr>
													
											</c:forEach>
					</tbody>						
             </table>
             		
             <div class="row-fluid">
			<util:pagination thispage="${page}"></util:pagination>
		</div>
    </c:if>
				
							

										
                     
                             
     
							
									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
						
		
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				
				
				
		
				
		

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
	
		<!-- basic scripts -->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

		

		<!-- ace scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

				
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
