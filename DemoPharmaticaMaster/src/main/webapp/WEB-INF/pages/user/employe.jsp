<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>Employe - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/datepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/colorpicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/core/demos.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.all.css">
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery-1.10.2.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.core.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.widget.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.button.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.position.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.menu.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.autocomplete.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.tooltip.js"></script>

		<!-- fonts -->

		<style>
	.custom-combobox {
		position: relative;
		display: inline-block;
	}
	.custom-combobox-toggle {
		position: absolute;
		top: 0;
		bottom: 0;
		margin-left: -1px;
		padding: 0;
		/* support: IE7 */
		*height: 1.7em;
		*top: 0.1em;
	}
	.custom-combobox-input {
		margin: 0;
		padding: 0.3em;
	}
	</style>

		<!-- fonts -->

		
	<script type="text/javascript">
	
		
			 function test(str) {
                 return /^ *[0-9]+ *$/.test(str);
             }
			 function cin(str) {
		            return /^ *[0-9]{8} *$/.test(str);
		        }
			 function tel(str) {
		            return /^(2|9|5) *[0-9]{7} *$/.test(str);
		        }
			 function VerifFloat(f) { // v�rif validit� email par REGEXP
			      var reg =/^[\+\-]?[0-9]+(([\.\,][0-9]{1})|([\.\,][0-9]{2}))?$/;
			      return (reg.exec(f)!=null);
			   }		
			function bonmail(mailteste)

			{
				var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');

				return(reg.test(mailteste));
			}
			 function Demandeverification(){
			        
			        msg = "Voulez-vous vraiment supprimer cet employee ?"
			        return confirm(msg);
			 }
						
function valider ( )
{
    if ( document.formulaire.nom.value == "" )
    {
        alert ( "Veuillez entrer votre nom !" );
        valid = false;
return valid;
    }
    if ( document.formulaire.prenom.value == "" )
    {
        alert ( "Veuillez entrer votre prenom !" );
        valid = false;
return valid;
    }
    if ( cin(document.formulaire.cin.value) == "" )
    {
        alert ( "Veuillez entrer votre cin !" );
        valid = false;
return valid;
    }

    if ( tel(document.formulaire.tel.value) == "" )
    {
        alert ( "Veuillez entrer votre tel !" );
        valid = false;
return valid;
    }
    if ( document.formulaire.grade.value == "" )
    {
        alert ( "Veuillez entrer votre grade!" );
        valid = false;
return valid;
    }

    if ( VerifFloat(document.formulaire.salaire.value) == "" )
    {
        alert ( "Veuillez entrer votre salaire!" );
        valid = false;
return valid;
    }
   
    if ( document.formulaire.datenaissance.value == "" )
    {
        alert ( "Veuillez entrer votre date de naissance!" );
        valid = false;
return valid;
    }
    if ( document.formulaire.datedebut.value == "" )
    {
        alert ( "Veuillez entrer votre date de debut!" );
        valid = false;
return valid;
    }

    if ( document.formulaire.departement.value == "" )
    {
        alert ( "Veuillez entrer votre departemet!" );
        valid = false;
return valid;
    }
    
    if(document.formulaire.adr.value == ""){
        if(document.formulaire.ville.value == ""){
          alert ( "Veuillez entrer votre ville!" );
                valid = false;
           return valid;
            }
        if(test(document.formulaire.codePostal.value )== ""){
         alert ( "Veuillez entrer votre code postal!" );
               valid = false;
          return valid;
           }
        if(document.formulaire.nomRue.value == ""){
         alert ( "Veuillez entrer votre nom de rue " );
               valid = false;
          return valid;
           }
               

       }
}
       
   

</script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>
<script>
	(function( $ ) {
		$.widget( "custom.combobox", {
			_create: function() {
				this.wrapper = $( "<span>" )
					.addClass( "custom-combobox" )
					.insertAfter( this.element );

				this.element.hide();
				this._createAutocomplete();
				this._createShowAllButton();
			},

			_createAutocomplete: function() {
				var selected = this.element.children( ":selected" ),
					value = selected.val() ? selected.text() : "";

				this.input = $( "<input>" )
					.appendTo( this.wrapper )
					.val( value )
					.attr( "title", "" )
					.addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
					.autocomplete({
						delay: 0,
						minLength: 0,
						source: $.proxy( this, "_source" )
					})
					.tooltip({
						tooltipClass: "ui-state-highlight"
					});

				this._on( this.input, {
					autocompleteselect: function( event, ui ) {
						ui.item.option.selected = true;
						this._trigger( "select", event, {
							item: ui.item.option
						});
					},

					autocompletechange: "_removeIfInvalid"
				});
			},

			_createShowAllButton: function() {
				var input = this.input,
					wasOpen = false;

				$( "<a>" )
					.attr( "tabIndex", -1 )
					.attr( "title", "Show All Items" )
					.tooltip()
					.appendTo( this.wrapper )
					.button({
						icons: {
							primary: "ui-icon-triangle-1-s"
						},
						text: false
					})
					.removeClass( "ui-corner-all" )
					.addClass( "custom-combobox-toggle ui-corner-right" )
					.mousedown(function() {
						wasOpen = input.autocomplete( "widget" ).is( ":visible" );
					})
					.click(function() {
						input.focus();

						// Close if already visible
						if ( wasOpen ) {
							return;
						}

						// Pass empty string as value to search for, displaying all results
						input.autocomplete( "search", "" );
					});
			},

			_source: function( request, response ) {
				var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
				response( this.element.children( "option" ).map(function() {
					var text = $( this ).text();
					if ( this.value && ( !request.term || matcher.test(text) ) )
						return {
							label: text,
							value: text,
							option: this
						};
				}) );
			},

			_removeIfInvalid: function( event, ui ) {

				// Selected an item, nothing to do
				if ( ui.item ) {
					return;
				}

				// Search for a match (case-insensitive)
				var value = this.input.val(),
					valueLowerCase = value.toLowerCase(),
					valid = false;
				this.element.children( "option" ).each(function() {
					if ( $( this ).text().toLowerCase() === valueLowerCase ) {
						this.selected = valid = true;
						return false;
					}
				});

				// Found a match, nothing to do
				if ( valid ) {
					return;
				}

				// Remove invalid value
				this.input
					.val( "" )
					.attr( "title", value + " didn't match any item" )
					.tooltip( "open" );
				this.element.val( "" );
				this._delay(function() {
					this.input.tooltip( "close" ).attr( "title", "" );
				}, 2500 );
				this.input.data( "ui-autocomplete" ).term = "";
			},

			_destroy: function() {
				this.wrapper.remove();
				this.element.show();
			}
		});
	})( jQuery );

	$(function() {
		$( "#dep" ).combobox();
		$( "#toggle" ).click(function() {
			$( "#dep" ).toggle();
		});
	});
	$(function() {
		$( "#adr" ).combobox();
		$( "#toggle" ).click(function() {
			$( "#adr" ).toggle();
		});
	});
	
	</script>
		
		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/employee"%>">Employ�</a>
							</li>
							<li class="active">Ajout/Liste</li>
						</ul><!-- .breadcrumb -->

						
						
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
							   Employ�
								<small>
									<i class="icon-double-angle-right"></i>
									Liste
								</small>
								<small>
								<p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font></p> </small>
								
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								<div class="col-xs-12 col-sm-6 widget-container-span" style="width: 100%">
										<div class="widget-box">
											<div class="widget-header widget-header-large">
												<h4>Ajouter nouveau employ�</h4>

												<div class="widget-toolbar">
													

													

													<a href="#" data-action="collapse">
														<i class="icon-chevron-up"></i>
													</a>

													
												</div>
											</div>

											<div class="widget-body" style="height:650px">
												<div class="widget-main" >
												
										<form:form method="post" name="formulaire" onsubmit="return valider()" action="addEmp" commandName="employe" class="form-horizontal">
									<div class="form-group">
									<table>
									<tr>
									<td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="nom">Nom:</form:label>
                	<div class="col-sm-9">
                    <form:input path="nom"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
           		 <div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="prenom">Prenom:</form:label>
                	<div class="col-sm-9">
                    <form:input path="prenom"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
           		  <div class="form-group">
           		  <table><tr><td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="cin">CIN:</form:label>
                	</td> <td>
                	<br>
                    <p><font color="red" size="2" face="Georgia, Arial" >
                    <c:out value="${message1}"/></font></p></td></tr></table>
                	<div class="col-sm-9">
                    <form:input path="cin"  class="col-xs-10 col-sm-5"/>
                    
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 </tr>
           		 <tr>
           		 <td>

							 <div class="form-group">
							  <table><tr><td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="tel">Num Telephone:</form:label>
                	</td> <td>
                	<br>
                    <p><font color="red" size="2" face="Georgia, Arial" >
                    <c:out value="${message2}"/></font></p></td></tr></table>
                	<div class="col-sm-9">
                    <form:input path="tel"  class="col-xs-10 col-sm-5"/>
                     
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
							 <div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="grade">Grade:</form:label>
                	<div class="col-sm-9">
                    <form:input path="grade"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
							 <div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="salaire">Salaire:</form:label>
                	<div class="col-sm-9">
                    <form:input path="salaire"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 </tr>
           		 <tr>
           		 <td>
							 <div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="datenaissance">Date de naissance:</form:label>
                	<div class="col-sm-9">
                    <form:input type="date" path="datenaissance"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
           		  <div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="datedebut">Date debut:</form:label>
                	<div class="col-sm-9">
                    <form:input type="date"  path="datedebut"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
									
				<div class="form-group">
                <form:label class="col-sm-3 control-label no-padding-right" path="departement" >Departement:</form:label>
                <div class="col-sm-9">
                     <form:select name="dep" id="dep" path="departement">
                    <option value="">-------------</option>
                    <c:forEach var="cl" items="${departements}">
                        <option value="${cl}" path="departement">${cl.nomDep}</option>
                    </c:forEach>
                </form:select>
                </div>
            </div>
             </td>
             </tr>
             <tr>
             <td>
             <td>
             <div class="form-group">
               <label class="col-sm-3 control-label no-padding-right" > Adresse existante:</label>
                <div class="col-sm-9">
                    <select name="adr" id="adr">
                    <option value="">-------------</option>
                    <c:forEach var="cl1" items="${adresses}">
                        <option value="${cl1.ville},${cl1.nomRue},${cl1.codePostal}" >${cl1.ville} ${cl1.nomRue} ${cl1.codePostal}</option>
                        
                    </c:forEach>
                </select>
                </div>
            </div>
</td>
</tr>
  <tr>
           <td colspan="3">
           <div class="space-4"></div>
           <div class="page-content">
						<div class="page-header">
						<h4>
           Nouvelle Adresse:</h4>
           </div></div>
           </td>
           </tr> 
<tr>
<td>
									<div class="space-4"></div>
							
<div class="form-group">
                	<label class="col-sm-3 control-label no-padding-right" >Nom de rue:</label>
                	<div class="col-sm-9">
                    <input  name="nomRue"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>

			</td>
			<td>
									<div class="space-4"></div>
									

									<div class="form-group">
                	<label class="col-sm-3 control-label no-padding-right" >Ville:</label>
                	<div class="col-sm-9">
                    <input name="ville" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
                           
                   </td>
                   <td>
                        
									<div class="space-4"></div>
								                                     
									
									<div class="form-group">
                	<label class="col-sm-3 control-label no-padding-right" >Code postal:</label>
                	<div class="col-sm-9">
                    <input name="codePostal"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 </tr>
</table>
								
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" type="submit" value="Submit">
												<i class="icon-ok bigger-110"></i>
												Ajouter
												
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												annuler
											</button>
										</div>
									</div>
									</form:form>
													
												</div>
											</div>
										</div>
									</div>
											
											
			
<div class="row">
		<div class="col-xs-12">
    <c:if test="${!empty employes}">
                <h3 class="header smaller lighter blue">employes</h3>
				<div class="table-header">
										Listes des employes
										</div>
										
			<table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
				<thead>
				<tr>
                    	<th></th>
                        <th>Name</th>
                        <th>Prenom</th>
                        <th>Departement</th>
                         <th>&nbsp;</th>
                           
                        <th>&nbsp;</th>
                         <th>&nbsp;</th>
                    </tr>
				</thead>

												
											<tbody role="alert" aria-live="polite" aria-relevant="all">
											<c:forEach items="${employes}" var="employe">
											<tr class="odd">
											
														<td class="center  sorting_1">
															<input type="hidden" value="{employe.empId}"/>
														</td>

														<td class=" ">
															${employe.nom}
														</td>
														<td class=" ">${employe.prenom}</td>
														<td class="hidden-480 ">${employe.departement}</td>
														<td><div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="detailsEmp/${employe.empId}" method="get">
								
								<button class="btn btn-xs btn-success">
							    <i class="icon-ok bigger-120"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
						</form>
					</div>	</td>
														<td class=" ">
														
														
					<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="updateEmp/${employe.empId}" method="get">
								
								<button class="btn btn-xs btn-info">
							    <i class="icon-edit bigger-120"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
								                                          
								 
								 
								 
								 
                         
						</form>
					</div>
																
					</td>

					<td class="hidden-480 ">
														
						<form  name="suppression" onSubmit="return Demandeverification()" action="deleteEmp/${employe.empId}" method="post">
							<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                               
							         <button class="btn btn-xs btn-danger">
																	<i class="icon-trash bigger-120"></i>
								
																</button>
							</div>
							<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																	<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
																				<span class="red">
																					<i class="icon-trash bigger-120"></i>
																				</span>
																			</a>
																	</li>
																	</ul>
								</div>
                                </div>								
						</form>
					</td>

														
				</tr>
													
											</c:forEach>
					</tbody>						
             </table>
    </c:if>		
									

										
                             
     
							
									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				
				
				
		
				
		

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
	
		<!-- basic scripts -->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/jquery.dataTables.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/jquery.dataTables.bootstrap.js"></script>

		<!-- ace scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
		</script>					
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
