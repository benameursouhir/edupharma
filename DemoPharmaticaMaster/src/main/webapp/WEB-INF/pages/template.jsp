<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from 198.74.61.72/themes/preview/ace/blank.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:12:32 GMT -->
<head>
		<meta charset="utf-8" />
		<title>Blank Page - Ace Admin</title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- basic styles -->

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!-- page specific plugin styles -->

		<!-- fonts -->

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		<!-- ace styles -->

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

		<!--[if lt IE 9]>
		<script src="assets/js/html5shiv.js"></script>
		<script src="assets/js/respond.min.js"></script>
		<![endif]-->
		<style type="text/css">

.main {
width:189px;
border:0px solid black;
}

.month {
background-color:#d15b47;
font:bold 12px verdana;
color:white;
}

.daysofweek {
background-color:#2e8965;
font:bold 12px verdana;
color:white;
}

.days {
font-size: 12px;
font-family:verdana;
color:black;
background-color: #f5f5f5;
padding: 2px;
}

.days #today{
font-weight: bold;
color: #892e65;
}

</style>


<script type="text/javascript">
/***********************************************

*/		 
function buildCal(m, y, cM, cH, cDW, cD, brdr){
var mn=['Janvier','F�vrier','Mars','Avril','Mai','Juin','Juillet','Aout','Septembre','Octobre','Novembre','D�cembre'];
var dim=[31,0,31,30,31,30,31,31,30,31,30,31];

var oD = new Date(y, m-1, 1); //DD replaced line to fix date bug when current day is 31st
oD.od=oD.getDay()+1; //DD replaced line to fix date bug when current day is 31st

var todaydate=new Date() //DD added
var scanfortoday=(y==todaydate.getFullYear() && m==todaydate.getMonth()+1)? todaydate.getDate() : 0 //DD added

dim[1]=(((oD.getFullYear()%100!=0)&&(oD.getFullYear()%4==0))||(oD.getFullYear()%400==0))?29:28;
var t='<div class="'+cM+'"><table class="'+cM+'" cols="7" cellpadding="0" border="'+brdr+'" cellspacing="0"><tr align="center">';
t+='<td colspan="7" align="center" class="'+cH+'">'+mn[m-1]+' - '+y+'</td></tr><tr align="center">';
for(s=0;s<7;s++)t+='<td class="'+cDW+'">'+"SMTWTFS".substr(s,1)+'</td>';
t+='</tr><tr align="center">';
for(i=1;i<=42;i++){
var x=((i-oD.od>=0)&&(i-oD.od<dim[m-1]))? i-oD.od+1 : '&nbsp;';
if (x==scanfortoday) //DD added
x='<span id="today">'+x+'</span>' //DD addedF
t+='<td class="'+cD+'">'+x+'</td>';
if(((i)%7==0)&&(i<36))t+='</tr><tr align="center">';
}
return t+='</tr></table></div>';
}
</script> 
	
	</head>
<c:if test="${username== null} ">
<%=request.getContextPath()+"/login"%>
</c:if>
	<body >
		<div class="navbar navbar-default" id="navbar">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<div class="navbar-header pull-left">
					<a href="#" class="navbar-brand">
						<small>
							<img src="${pageContext.request.contextPath}/resources/assets/images/logo.png"  height="26px"  width="200px"/>
						</small>
					</a><!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
					
					
					<c:if test="${username != 'admin'}">
					
						<li class="grey">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-tasks"></i>
								<span class="badge badge-grey">${prodPerim }</span>
							</a>

							<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-warning-sign"></i>
									${prodPerim} produits date perimation proche
								</li>
<c:if test="${!empty prodPerimL}">
<c:forEach items="${prodPerimL}" var="p">
								<li>
									
										
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">${p.produit.refP }</span>
												<span class="badge badge-important">${p.qteStock }</span>
											</span>

										
										</span>
									
								</li>
								</c:forEach>
								</c:if>
							<!-- <li>
									<a href="#">
										Voir d�tails
										<i class="icon-arrow-right"></i>
									</a>
								</li> -->
							</ul>
						</li>

						<li class="purple">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-bell-alt icon-animated-bell"></i>
								<span class="badge badge-important">${prodFini }</span>
							</a>

							<ul class="pull-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="icon-warning-sign"></i>
									${prodFini } produits ont atteint seuil
								</li>

								<c:if test="${!empty prodFiniL}">
<c:forEach items="${prodFiniL}" var="pf">
								<li>
									<a href="#">
										
										<span class="msg-body">
											<span class="msg-title">
												<span class="blue">${pf.produit.refP }</span>
												
								<span class="badge badge-important">${pf.qteStock }</span>
											</span>

										
										</span>
									</a>
								</li>
								</c:forEach>
								</c:if>

							<!--	<li>
									<a href="#">
										Voir details
										<i class="icon-arrow-right"></i>
									</a>
								</li>-->
							</ul>
						</li>

						<li class="green">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="icon-envelope icon-animated-vertical"></i>
								<span class="badge badge-success">${nbrMsg}</span>
							</a>

							<ul class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close" >
								<li class="dropdown-header">
									<i class="icon-envelope-alt"></i>
									Commandes
								</li>
<c:if test="${!empty nbrMsgL}">
<c:forEach items="${nbrMsgL}" var="m">
								<li>
									<a href="/DemoPharmaticaMaster/detailsCom/${m.comId}">
										
										<span class="msg-body">
											<span class="msg-title">
											<!-- <input type="hidden" name="comId" id="comId"  value="${m.comId}"/> -->
												<span class="blue">${m.organisationEnvoi.nom}</span>
												${m.totalTtc } D
											</span>

											<span class="msg-time">
												<i class="icon-time"></i>
												<span>${m.dateComm }</span>
											</span>
										</span>
									</a>
								</li>
</c:forEach>
</c:if>
								

<!--
								<li>
									<a href="<%=request.getContextPath()+"/commandeNouv"%>">
										Voir listes 
										<i class="icon-arrow-right"></i>
									</a>
								</li>-->
							</ul>
						</li>
						</c:if>
						
						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								
								<span class="user-info">
									<small>Bienvenue,</small>
									${username}
									
								</span>

								<i class="icon-caret-down"></i>
							</a>

							<ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#">
										<i class="icon-cog"></i>
										Parametre
									</a>
								</li>

								<li>
									<a href="<%=request.getContextPath()+"/detailsUser"%>">
										<i class="icon-user"></i>
										Profile
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="<c:url value="/j_spring_security_logout" />">
										<i class="icon-off"></i>
										Deconnexion
									</a>
								</li>
							</ul>
						</li>
					</ul><!-- /.ace-nav -->
				</div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>

		<div class="main-container" id="main-container">
			<script type="text/javascript">
				try{ace.settings.check('main-container' , 'fixed')}catch(e){}
			</script>

			<div class="main-container-inner">
				<a class="menu-toggler" id="menu-toggler" href="#">
					<span class="menu-text"></span>
				</a>

				<div class="sidebar" id="sidebar">
					<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'fixed')}catch(e){}
					</script>


					<ul class="nav nav-list">
					<c:if test="${username != 'admin'}">
						<li>
							<a href="#" class="dropdown-toggle">
								<img src="${pageContext.request.contextPath}/resources/assets/icons/first_aid.png"  />
								
								<span class="menu-text"> Commandes</span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
							<li>
									<a href="<%=request.getContextPath()+"/choixOrg"%>">
										<i class="icon-double-angle-right"></i>
										<i class="icon-plus"></i>
										Nouvelle Commandes
									</a>
								</li>
							
							<li>
									<a href="<%=request.getContextPath()+"/commandeNouv"%>">
										
										<img src="${pageContext.request.contextPath}/resources/assets/images/redEye.PNG" style="width: 20px" />
										Commandes re�ues non trait�es</label>
									</a>
								</li>
								
								
								
								<li>
									<a href="<%=request.getContextPath()+"/listeCommande"%>">
										<img src="${pageContext.request.contextPath}/resources/assets/images/yellowEye.PNG" style="width: 20px" />
										
										Liste des Commandes re�ues</label>
									</a>
								</li>
								
									<li>
									<a href="<%=request.getContextPath()+"/envoiListe"%>">
									
										<img src="${pageContext.request.contextPath}/resources/assets/images/blueaye.PNG" style="width: 20px" />
										Commandes envoy�es</label>
									</a>
								</li>
							</ul>
						</li>
						</c:if>
						
						<c:if test="${username == 'admin'}">
						<li>
							<a href="#" class="dropdown-toggle">
								<img src="${pageContext.request.contextPath}/resources/assets/icons/prescription_drugs.png"  />
								
								<span class="menu-text"> Produits</span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
							<li>
									<a href="<%=request.getContextPath()+"/nouvProd"%>">
										<i class="icon-double-angle-right"></i>
										<i class="icon-plus"></i>
										Nouveau produit
									</a>
								</li>
								<li>
									<a href="<%=request.getContextPath()+"/produits"%>">
										<i class="icon-double-angle-right"></i>
										<i class="icon-plus"></i>
										Liste des produits
									</a>
								</li>
								
								
								</ul>
							
							</li>
							</c:if>
					
						<c:if test="${username != 'admin'}">
						
						<li>
							<a href="#" class="dropdown-toggle">
								<img src="${pageContext.request.contextPath}/resources/assets/icons/prescription_drugs.png"  />
								
								<span class="menu-text"> Produits</span>

								<b class="arrow icon-angle-down"></b>
							</a>

							<ul class="submenu">
							<li>
									<a href="<%=request.getContextPath()+"/nouvProd"%>">
										<i class="icon-double-angle-right"></i>
										<i class="icon-plus"></i>
										Nouveau produit
									</a>
								</li>
								<li>
									<a href="<%=request.getContextPath()+"/stockProduits"%>">
										<i class="icon-double-angle-right"></i>
										<i class="icon-plus"></i>
										Liste des produits
									</a>
								</li>
								
								<li>
									<a href="<%=request.getContextPath()+"/listeProdAjout"%>">
										<i class="icon-double-angle-right"></i>
										<i class="icon-plus"></i>
										Produits ajout�s 
									</a>
								</li>
							
								</ul>
							
							</li>
						
						
						
						<li>
							<a href="<%=request.getContextPath()+"/employee"%>" class="dropdown-toggle">
								<img src="${pageContext.request.contextPath}/resources/assets/icons/doctor.png"  />
								<span class="menu-text">Employ�es</span>
							</a>

						</li>
						</c:if>	
						<li>
							<a href="<%=request.getContextPath()+"/organisationn"%>" class="dropdown-toggle">
								<img src="${pageContext.request.contextPath}/resources/assets/icons/family_doctor.png"  />
								<span class="menu-text">Organisations</span>
							</a>

							
						</li>
							<c:if test="${username != 'admin'}">
						<li>
							<a href="<%=request.getContextPath()+"/listDepartments"%>" class="dropdown-toggle">
							<img src="${pageContext.request.contextPath}/resources/assets/icons/stethoscope.png"  />
							
								<span class="menu-text">Departements</span>
							</a>

						</li>
						</c:if>
<c:if test="${username == 'admin'}">
						<li>
							<a href="<%=request.getContextPath()+"/ImportData"%>" class="dropdown-toggle">
							<img src="${pageContext.request.contextPath}/resources/assets/icons/medicine.png"  />
								<span class="menu-text">Importer </span>
										
							</a>
					
							
						
						</li>	
						<li>
							<a href="<%=request.getContextPath()+"/register"%>" class="dropdown-toggle">
							<img src="${pageContext.request.contextPath}/resources/assets/icons/pharmachology.png"  />
								<span class="menu-text">Inscrir organisation </span>
										
							</a>
					
							
						
						</li>	
</c:if>
						
					</ul>

	<br>				
<form>
<select onChange="updatecalendar(this.options)">
<script type="text/javascript">

var themonths=['Janvier','F�vrier','Mars','Avril','Mai','Juin',
'Juillet','Aout','Septembre','Octobre','Novembre','D�cembre']

var todaydate=new Date()
var curmonth=todaydate.getMonth()+1 //get current month (1-12)
var curyear=todaydate.getFullYear() //get current year

function updatecalendar(theselection){
var themonth=parseInt(theselection[theselection.selectedIndex].value)+1
var calendarstr=buildCal(themonth, curyear, "main", "month", "daysofweek", "days", 0)
if (document.getElementById)
document.getElementById("calendarspace").innerHTML=calendarstr
}

document.write('<option value="'+(curmonth-1)+'" selected="yes" >Mois en Cours</option>')
document.write('<br><br>')
for (i=0; i<12; i++) //display option for 12 months of the year
document.write('<option value="'+i+'">'+themonths[i]+' '+curyear+'</option>')


</script>
</select>

<div id="calendarspace">
<script>
//write out current month's calendar to start
document.write(buildCal(curmonth, curyear, "main", "month", "daysofweek", "days", 0))
</script>
</div>

</form>



	<script type="text/javascript">
						try{ace.settings.check('sidebar' , 'collapsed')}catch(e){}
					</script>
					
					
					
				</div>

			</div>

			
			</div><!-- /.main-container-inner -->

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<!-- basic scripts -->

		<!--[if !IE]> -->

		
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/blank.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:12:32 GMT -->
</html>
