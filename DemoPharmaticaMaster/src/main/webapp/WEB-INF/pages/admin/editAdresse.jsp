<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>Adresse - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/datepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/colorpicker.css" />

		<!-- fonts -->

		
		
	<script type="text/javascript">
   function test(str) {
             return /^ *[0-9]+ *$/.test(str);
         }
   function valider ( )
   {
       if ( document.formulaire.nomRue.value == "" )
       {
           alert ( "Veuillez entrer votre nom de rue !" );
           valid = false;
   return valid;
       }
       if ( test(document.formulaire.codePostal.value) == "" )
       {
           alert ( "Veuillez entrer votre code postale !" );
           valid = false;
   return valid;
       }

       if ( document.formulaire.ville.value == "" )
       {
           alert ( "Veuillez entrer votre ville !" );
           valid = false;
   return valid;
       }}
</script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>

		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/adresses"%>">Adresse</a>
							</li>
							<li class="active">Modifier</li>
						</ul><!-- .breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
							   Adresse
								<small>
									<i class="icon-double-angle-right"></i>
									Modifier
								</small>
								 <small>
        <p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font></p> </small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
<form:form  action="${pageContext.request.contextPath}/modif/${adresse.adrId}" name="formulaire" onsubmit="return valider()"  method="Post" commandName="adresse" class="form-horizontal">
						<div class="form-group" style="width: 400px;">
                	<table> <tr> <td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="nomRue">nom Rue:</form:label>
                	</td><td>
                	 <p><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message1}"/></font></p> </td></tr></table>
                	<div class="col-sm-9">
                    <form:input path="nomRue"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 <div class="form-group" style="width: 400px;">
                	<form:label class="col-sm-3 control-label no-padding-right" path="codePostal">code Postal:</form:label>
                	<div class="col-sm-9">
                    <form:input path="codePostal"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		  <div class="form-group" style="width: 400px;">
                	<form:label class="col-sm-3 control-label no-padding-right" path="ville">ville:</form:label>
                	<div class="col-sm-9">
                    <form:input path="ville"  class="col-xs-10 col-sm-5"/>
                   
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>

										

								
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" type="submit" value="Submit" name="modifier">
												<i class="icon-ok bigger-110"></i>
												Modifier
												
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn btn-danger"" type="submit">
												<i class="icon-reply icon-only"></i>
												Annuler et retourner
											</button>
										</div>
									</div>
									</form:form>
    			
                             
     
							
									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				
				
				
		
				
		

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
	
		<!-- basic scripts -->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

		

		<!-- ace scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

				
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
