<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>Adresse - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/datepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/colorpicker.css" />

		<!-- fonts -->

	<script type="text/javascript">
	function Demande_verification(){
	       
	       msg = "Voulez-vous vraiment supprimer cette adresse ?"
	       return confirm(msg);
	}

   function test(str) {
             return /^ *[0-9]+ *$/.test(str);
         }
   function valider ( )
   {
       if ( document.formulaire.nomRue.value == "" )
       {
           alert ( "Veuillez entrer votre nom de rue !" );
           valid = false;
   return valid;
       }
       if ( test(document.formulaire.codePostal.value) == "" )
       {
           alert ( "Veuillez entrer votre code postale !" );
           valid = false;
   return valid;
       }

       if ( document.formulaire.ville.value == "" )
       {
           alert ( "Veuillez entrer votre ville !" );
           valid = false;
   return valid;
       }}
</script>
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>

		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/adresses"%>">Adresse</a>
							</li>
							<li class="active">Ajout</li>
						</ul><!-- .breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
							   Adresse
								<small>
									<i class="icon-double-angle-right"></i>
									Ajouter
								</small>
								<small>
								<p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font></p> 
							</h1>
							</small>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								<form:form method="post" name="formulaire" onsubmit="return valider()" action="addAdr" commandName="adresse" class="form-horizontal">
						<div class="form-group" style="width: 400px;">
						<table> <tr> <td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="nomRue">nom Rue:</form:label>
                	</td><td>
                	 <p><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message1}"/></font></p> </td></tr></table>
                	<div class="col-sm-9">
                    <form:input path="nomRue"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 <div class="form-group" style="width: 400px;">
                	<form:label class="col-sm-3 control-label no-padding-right" path="codePostal">code Postal:</form:label>
                	<div class="col-sm-9">
                    <form:input path="codePostal"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		  <div class="form-group" style="width: 400px;">
                	<form:label class="col-sm-3 control-label no-padding-right" path="ville">ville:</form:label>
                	<div class="col-sm-9">
                    <form:input path="ville"  class="col-xs-10 col-sm-5"/>
                    <br>
                   
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>

										

								
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" type="submit" value="Submit">
												<i class="icon-ok bigger-110"></i>
												Ajouter
												
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												annuler
											</button>
										</div>
									</div>
									</form:form>
    <c:if test="${!empty adresses}">
                <h3 class="header smaller lighter blue">adresses</h3>
				<div class="table-header">
										Listes des adresses
										</div>
										
			<table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
				<thead>
													 <tr>
                         <th>Id</th>
                        <th>Nom de Rue</th>
                        <th>codePostal</th>
                        <th>ville</th>
                        
                        <th>&nbsp;</th>
                         <th>&nbsp;</th>
                    </tr>
				</thead>

												
											<tbody role="alert" aria-live="polite" aria-relevant="all">
											<c:forEach items="${adresses}" var="adresse">
											<tr class="odd">
											
														<td class="center  sorting_1">
															${adresse.adrId}
														</td>

														<td class=" ">
															${adresse.nomRue}
														</td>
														<td class=" ">${adresse.codePostal}</td>
														<td class="hidden-480 ">${adresse.ville}</td>
														
							<td class="hidden-480 ">
								<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="details/${adresse.adrId}" method="get">
								
								<button class="btn btn-xs btn-success">
							    <i class="icon-ok bigger-120"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
						</form>
					</div>						
						
					</td>							
				
				<td class=" ">
					<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="modif/${adresse.adrId}" method="get">
								
								<button class="btn btn-xs btn-info">
							    <i class="icon-edit bigger-120"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
								                                          
								 
								 
								 
								 
                         
						</form>
					</div>
																
					</td>

					

														<td>
															<form  name="suppression"  onSubmit="return Demande_verification()" action="delete/${adresse.adrId}" method="post">
							<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                               
							         <button class="btn btn-xs btn-danger" >
																	<i class="icon-trash bigger-120"></i>
								
																</button>
							</div>
							<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown" >
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close" >
																	<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
																				<span class="red">
																					<i class="icon-trash bigger-120"></i>
																				</span>
																			</a>
																	</li>
																	</ul>
								</div>
                                </div>								
						</form>
														</td>
				</tr>
													
											</c:forEach>
					</tbody>						
             </table>
    </c:if>		
									

										
                             
     
							
									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				
				
				
		
				
		

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
	
		<!-- basic scripts -->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/jquery.dataTables.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/jquery.dataTables.bootstrap.js"></script>

		<!-- ace scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

		<script type="text/javascript">
			jQuery(function($) {
				var oTable1 = $('#sample-table-2').dataTable( {
				"aoColumns": [
			      { "bSortable": false },
			      null, null,null, null, null,
				  { "bSortable": false }
				] } );
				
				
				$('table th input:checkbox').on('click' , function(){
					var that = this;
					$(this).closest('table').find('tr > td:first-child input:checkbox')
					.each(function(){
						this.checked = that.checked;
						$(this).closest('tr').toggleClass('selected');
					});
						
				});
			
			
				$('[data-rel="tooltip"]').tooltip({placement: tooltip_placement});
				function tooltip_placement(context, source) {
					var $source = $(source);
					var $parent = $source.closest('table')
					var off1 = $parent.offset();
					var w1 = $parent.width();
			
					var off2 = $source.offset();
					var w2 = $source.width();
			
					if( parseInt(off2.left) < parseInt(off1.left) + parseInt(w1 / 2) ) return 'right';
					return 'left';
				}
			})
		</script>					
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
