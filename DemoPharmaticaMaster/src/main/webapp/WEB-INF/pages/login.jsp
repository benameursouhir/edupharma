<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
	
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
		<meta charset="utf-8" />
		<title>Produit - pharma</title>

		<meta name="description" content="Common form elements and layouts" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<!-- basic styles -->

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<!--[if IE 7]>
		  <link rel="stylesheet" href="assets/css/font-awesome-ie7.min.css" />
		<![endif]-->

		<!-- page specific plugin styles -->

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/datepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/colorpicker.css" />

		<!-- fonts -->

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		<!-- ace styles -->

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="assets/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>

<style>
.errorblock {
	color: #ff0000;
padding-right:450px;

}
</style>
		
	</head>

	<body style="width:0%" onload='document.f.j_username.focus();'>
	<center>
	<div class="navbar navbar-default" id="navbar" style="width:900px; height:150px;top:50px;left:190px">
	
			<div class="navbar-container" id="navbar-container" style="width:700px">
			<img src="${pageContext.request.contextPath}/resources/assets/images/logo.png" style="width:700px;height: 150px"/>
				<div class="navbar-header pull-left"  >
				
					<!-- /.brand -->
				</div><!-- /.navbar-header -->

				<div class="navbar-header pull-right" role="navigation"></div><!-- /.navbar-header -->
			</div><!-- /.container -->
		</div>

		<div class="main-container" id="main-container"  style="width:900px; height:150px;top:170px;">
			<div class="main-container-inner" style="top:60px;left:190px;">
				
				<div class="main-content" style="margin-left:0px;">
					<div class="breadcrumbs" id="breadcrumbs">
						
						

						
					</div>

					<div class="page-content" style="background-color:#f5f5f5;">
						<div class="page-header">
							<h1>
								Bienvenue
								<small>
									<i class="icon-double-angle-right"></i>
									Page de connexion
								</small>
							
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								
					<form class="form-horizontal" name='f' action="<c:url value='j_spring_security_check' />" method='POST'>
								
								
	<c:if test="${not empty error}">
		<div class="errorblock">
			Verifier votre information
		</div>
	</c:if>
								
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-1"><b>Email</b>  </label>

										<div class="col-sm-9">
											<input type="text" placeholder="Email" class="col-xs-10 col-sm-5" name='j_username' value='' style="width:350px" />
										</div>
									</div>

									<div class="space-4"></div>

									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" for="form-field-2"  style="width:200px"><b>mot de passe</b>  </label>

										<div class="col-sm-9">
											<input type="password" name='j_password'   value="" placeholder="Mot de passe" class="col-xs-10 col-sm-5" style="width:350px" />
											<span class="help-inline col-xs-12 col-sm-7">
												
										</div>
									</div>

									

									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<input name="submit"  class="btn btn-primary" type="submit"
					value="Connecter" />

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												annuler
											</button>
										</div>
									</div>

									<div class="hr hr-24"></div>

									

										
							</form>
							
							</div>
										
										</div><!-- /span -->

										

									

									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->

		</center>

	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
