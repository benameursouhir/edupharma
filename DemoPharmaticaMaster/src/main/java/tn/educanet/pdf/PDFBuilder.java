package tn.educanet.pdf;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import tn.educanet.entities.CommandeProduit;



import java.util.List;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class PDFBuilder extends PdfCommande{

	@Override
	protected void buildPdfDocument(Map<String, Object> model,
			Document document, PdfWriter writer, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		
		List<CommandeProduit> cp=(List<CommandeProduit>) model.get("commandeProduits");
		 document.add(new Paragraph((String) cp.get(0).getCommande().getOrganisation().getNom()+"\n GROSSISTE-REPARTITEUR EN PHARMACIE \n"+cp.get(0).getCommande().getOrganisation().getAdresse().getVille()+"  "+  cp.get(0).getCommande().getOrganisation().getAdresse().getNomRue()+"  "+ cp.get(0).getCommande().getOrganisation().getAdresse().getCodePostal()+"\n TEL:"+cp.get(0).getCommande().getOrganisation().getTelfix()+" FAX: "+cp.get(0).getCommande().getOrganisation().getFax()+"\n CODE TVA:  816959J A M 000"));

		 PdfPTable table3 = new PdfPTable(3);
			
		  table3.setWidthPercentage(60.0f);
		  
	        table3.setWidths(new float[] {2.0f, 2.0f,2.0f});
	       table3.setSpacingBefore((float) 5.5);
	       
	       
	       
		 
		 
		 
		PdfPTable table = new PdfPTable(3);
		
		  table.setWidthPercentage(100.0f);
		  
	        table.setWidths(new float[] {3.0f, 3.0f,4.0f});
	       table.setSpacingBefore((float) 5.5);
	      //define font for table header row
	        Font font = FontFactory.getFont(FontFactory.HELVETICA);
	        font.setColor(BaseColor.WHITE);
	         
	        // define table header cell
	        PdfPCell cell1 = new PdfPCell();
	       // cell.setBackgroundColor(BaseColor.BLUE);
	       // cell1.setPadding(5);
	       
	        cell1.setBorder(0);
	        
	        
	       cell1.setPhrase(new Phrase());
		   table.addCell(cell1);
		   cell1.setPhrase(new Phrase(cp.get(0).getCommande().getDateComm()));
			table.addCell(cell1);
			cell1.setPhrase(new Phrase());
			table.addCell(cell1);
			
			
			cell1.setPhrase(new Phrase("BON DE LIVRAISON : "+cp.get(0).getCommande().getBonLivraison()));
			table.addCell(cell1);
			cell1.setPhrase(new Phrase("16177"));
			table.addCell(cell1);
			cell1.setPhrase(new Phrase(new Paragraph((String)"CODE CLIENT"+cp.get(0).getCommande().getOrganisationEnvoi().getOrgId()+"\n"+cp.get(0).getCommande().getOrganisationEnvoi().getAdresse().getVille()+" "+cp.get(0).getCommande().getOrganisationEnvoi().getAdresse().getNomRue()+" "+cp.get(0).getCommande().getOrganisationEnvoi().getAdresse().getCodePostal()+"\n"+cp.get(0).getCommande().getOrganisationEnvoi().getTelfix()+"\nID.FISCALE: 929465Q/C/B/000" )));
			table.addCell(cell1);
			
		 
		 table.setSpacingAfter(10);
		 PdfPTable table1 = new PdfPTable(9);
		  table1.setWidthPercentage(100.0f);

	        table1.setWidths(new float[] {1.0f, 2.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f,1.0f});

	     // define font for table header row
//	        Font font1 = FontFactory.getFont(FontFactory.HELVETICA);
//	        font.setColor(BaseColor.WHITE);
//	         
	        // define table header cell
        PdfPCell cell = new PdfPCell();
//	        cell.setBackgroundColor(BaseColor.WHITE);
	       
		 
		 
		 cell.setPhrase(new Phrase("PEREM"));
		 table1.addCell(cell);
		 cell.setPhrase(new Phrase("DESIGNATION"));
		 table1.addCell(cell);
		 cell.setPhrase(new Phrase("QTE"));
		 table1.addCell(cell);
		 cell.setPhrase(new Phrase("P.PUBLIC"));
		 table1.addCell(cell);
		 cell.setPhrase(new Phrase("T"));
		 table1.addCell(cell);
		 cell.setPhrase(new Phrase("P.PHARM."));
		 table1.addCell(cell);
		 cell.setPhrase(new Phrase("MT.H.TVA"));
		 table1.addCell(cell);
		 cell.setPhrase(new Phrase("TVA %"));
		 table1.addCell(cell);
		 cell.setPhrase(new Phrase("C"));
		 table1.addCell(cell);
		 boolean verif=false;
		 int i=0;
		for(CommandeProduit c: cp){
			
			table1.addCell(c.getProduit().getRefP());
			table1.addCell(c.getProduit().getLibelle());
			table1.addCell(Float.toString(c.getQteDonne()));
			table1.addCell(Float.toString(c.getProduit().getPrixVenteHt()));
			table1.addCell(c.getProduit().getTableau());
			table1.addCell(Float.toString(c.getProduit().getPrixAchatHt()));
			table1.addCell(Float.toString(c.getPrixTotalHTVA()));
			table1.addCell(Float.toString(c.getProduit().getTauxTva()));
			if (c.getProduit().getClasse().equals("medecament"))
			table1.addCell(new Phrase("M"));
			if (c.getProduit().getClasse().equals("accessoire"))
				table1.addCell(new Phrase("A"));
			
			if (c.getProduit().getClasse().equals("para"))
				table1.addCell(new Phrase("P"));
			
			
			if(c.getQteDonne() < c.getQte()){
				if(i==0){
					
					 cell.setPhrase(new Phrase("Reference produit"));
					 table3.addCell(cell);
					 cell.setPhrase(new Phrase("Quantit� demand�es"));
					 table3.addCell(cell);
					 cell.setPhrase(new Phrase("Quantit� Donn�es"));
					 table3.addCell(cell);
					 i++;
				}
				verif=true;
				table3.addCell(c.getProduit().getRefP());
				table3.addCell(Float.toString(c.getQte()));
				table3.addCell(Float.toString(c.getQteDonne()));
			}
			
			
		}
		
		 table1.setSpacingAfter(10);
		 
		 
		 PdfPTable tab = new PdfPTable(3);
		 tab.setWidthPercentage(100.0f);
		 tab.setWidths(new float[] {3.0f, 4.0f,3.0f});
		 
		 PdfPCell cel = new PdfPCell();
		 cel.setPhrase(new Phrase("MT BRUT      :"+cp.get(0).getCommande().getMtBrut() ));
		 tab.addCell(cel);
		 
		 cel.setPhrase(new Phrase("TX TVA       :"+cp.get(0).getCommande().getTxTva()+" %"));
		 tab.addCell(cel);
		 cel.setPhrase(new Phrase("NET H.T      :"+cp.get(0).getCommande().getTxTva()));
		 tab.addCell(cel);
		 cel.setPhrase(new Phrase("M.H.TVA      :"+cp.get(0).getCommande().getMtBrut()));
		 tab.addCell(cel);
		 cel.setPhrase(new Phrase("BASE         :"+cp.get(0).getCommande().getBase()));
		 tab.addCell(cel);
		 cel.setPhrase(new Phrase("MT.TVA       :"+cp.get(0).getCommande().getMtTva()));
		 tab.addCell(cel);
		 cel.setPhrase(new Phrase("EXONOR�    0.000"));
		 tab.addCell(cel);
		 cel.setPhrase(new Phrase("MONTANT      :"+cp.get(0).getCommande().getMontant()));
		 tab.addCell(cel);
		 cel.setPhrase(new Phrase("TOTAL TTC    :"+cp.get(0).getCommande().getTotalTtc()));
		 tab.addCell(cel);
		 
		
		 document.add(table);
		 document.add(table1);
		 document.add(tab);
		 document.add(new Phrase("NB : tout retour sera refus� apr�s 48 heures de la livraison \n"));
		 document.add(new Phrase("Signature du Tireur                       Signature du Controleur"));
		 if(verif){
			 cell.setPhrase(new Phrase("Reference produit"));
			 table3.addCell(cell);
			 cell.setPhrase(new Phrase("Quantit� demand�es"));
			 table3.addCell(cell);
			 cell.setPhrase(new Phrase("Quantit� Donn�es"));
			 table3.addCell(cell);
			 document.add(new Phrase("\nListe de changement sur la commande"));
			 document.add(table3);
			 
			 
		 }
	}

}
