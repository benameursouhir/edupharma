package tn.educanet.service;

import java.util.List;

import tn.educanet.entities.Roles;

public interface RoleService {
	
	public void create(Roles p);
	public void update(Roles p) ;
	public void delete(long id);
	public Roles find(long id);
	public List<Roles> getAllRoles();
	Roles findByNomRole(String nomRole);

}
