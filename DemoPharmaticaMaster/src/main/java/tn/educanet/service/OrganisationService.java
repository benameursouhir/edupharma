package tn.educanet.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import tn.educanet.entities.Adresse;
import tn.educanet.entities.Organisation;


public interface OrganisationService {

	public void save(Organisation c,String rue,String ville,Integer codePostal,String type);
	public void update(Long id,Organisation c,String rue,String ville,Integer codePostal,String type);
	public void delete(long id);
	public Organisation find(long id);
	public List<Organisation> getAllOrganisation();
	List<Organisation> findByNom(String nom);
	Organisation findByNomAndAdresse(String nom,Adresse  adr);
	Organisation findByEmail(String email);
	Organisation findBytelfix(Integer telfix);
	Organisation findByfax(Integer fax);
	Organisation findBytelmobile(Integer telmobile);
	Organisation findByAdresse(Adresse  adr);
	List<Organisation> findByType(String type);
	List<Organisation> findByTypeAndNom(String type,String nom);
	List<Organisation> findByTypeNotLike(String type);
	Page<Organisation> findByTypeNotLike(String type,int s,int i);
	Organisation findByAdresse_VilleAndAdresse_NomRueAndAdresse_CodePostal(String ville,String nomRue,Integer codePostal);
	
	
	Page<Organisation> findByTypeAndAdresse_VilleAndAdresse_NomRueAndNom(String type,String ville,String nomRue,String nom,int s,int i);
	Page<Organisation> findByTypeAndAdresse_VilleAndAdresse_NomRue(String type,String ville,String nomRue,int s,int i);
	Page<Organisation> findByNomAndAdresse_VilleAndAdresse_NomRue(String nom,String ville,String nomRue,int s,int i );
	Page<Organisation> findByTypeAndAdresse_Ville(String type,String ville,int s,int i);
	Page<Organisation> findByNomAndAdresse_Ville(String nom,String ville,int s,int i);
	Page<Organisation> findByType(String type,int s,int i);
	Page<Organisation> findByNom(String nom,int s,int i);
	Page<Organisation> findByAdresse_Ville(String ville,int s,int i);
	
	public void updateNumCommande(Organisation o,int numCommande);
}
