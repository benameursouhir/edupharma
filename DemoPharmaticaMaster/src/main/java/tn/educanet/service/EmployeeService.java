package tn.educanet.service;

import java.util.List;

import tn.educanet.entities.Employee;
import tn.educanet.entities.Organisation;

public interface EmployeeService {
	
	
	public void save(String email,Employee e,String ville,String nomRue,Integer codePostal);
	public void update(Long empId,String email,Employee e,String ville,String nomRue,Integer codePostal);
	public void delete(Long id);
	public Employee find(Long id);
	
	public List<Employee> getAllEmployee();

	List<Employee> findByGrade(String grade);
	List<Employee> findByNomAndPrenom(String nom,String prenom);
	Employee findByCin(int cin);
	Employee findByTel(Integer tel);
	
	List<Employee> findByOrganisation(Organisation o);
	
}
