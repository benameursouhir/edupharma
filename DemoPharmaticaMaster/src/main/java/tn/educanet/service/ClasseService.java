package tn.educanet.service;

import java.util.List;

import org.springframework.data.domain.Page;

import tn.educanet.entities.Classe;

public interface ClasseService {
	
	public Classe save(Classe a);
	public void update(Long classeId,String nomClasse);
	public	void delete(Long id);
	public	Classe find(Long id);
	public	Page<Classe> findPerPage(int s,int i);
	public List<Classe> findAllClasse();
	Classe findByNomClasse(String nomClasse);

}
