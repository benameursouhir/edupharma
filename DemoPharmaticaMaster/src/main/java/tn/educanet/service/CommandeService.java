package tn.educanet.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import tn.educanet.entities.Commande;
import tn.educanet.entities.Organisation;

public interface CommandeService {
 public void create(Commande c);//,String user,String destOrg,String emp,String adresse,String listeProd,String listeQte);
	public  void update(Commande c) ;
	public void delete(long id);
	public Commande find(long id);
	List<Commande> getAllCommande();

	
	List<Commande> findByOrganisationEnvoiOrderByEtatAsc(Organisation orgE);
	List<Commande> findByOrganisationOrderByEtatAsc(Organisation orgR);
	List<Commande> findByOrganisationAndExisteDTrueOrderByEtatAsc(Organisation orgR);
	List<Commande> findByOrganisationEnvoiAndExisteSTrueOrderByEtatAsc(Organisation orgE);
	
	List<Commande> findByOrganisationEnvoiAndEtatTrue(Organisation orgE);
	List<Commande> findByOrganisationAndEtatTrue(Organisation orgR);
	
	List<Commande> findByOrganisationEnvoiAndEtatFalse(Organisation orgE);
	List<Commande> findByOrganisationAndEtatFalse(Organisation orgR);
	
	Commande findByDateComm(String date);
	
	Commande findByBonLivraison(int bonLivraison);

	
	Page<Commande> findByOrganisationAndExisteDTrue(Organisation orgE,int s,int i);
	Page<Commande> findByOrganisationEnvoiAndExisteSTrue(Organisation orgE,int s,int i);
	Page<Commande> findByOrganisationAndEtatFalse(Organisation orgR,int s,int i);
	
	List<Commande> findByOrganisation_NomAndDateCommAndEtat(String nom,String dateComm,boolean etat);
	List<Commande> findByOrganisation_NomAndDateComm(String nom,String dateComm);
	List<Commande> findByNomOrgAndEtat(String nom,boolean etat);
	List<Commande> findByNomOrg(String nom);
	List<Commande> findByDateAndEtat(String date,boolean etat);
	List<Commande> findByDate(String date);
	
	
	List<Commande> findByOrganisationEnvoi_NomAndDateCommAndEtat(String nom,String dateComm,boolean etat);
	List<Commande> findByOrganisationEnvoi_NomAndDateComm(String nom,String dateComm);
	List<Commande> findByOrganisationEnvoi_NomAndEtat(String nom,boolean etat);
	List<Commande> findByOrganisationEnvoi_Nom(String nom);
	List<Commande> findEnvoiByDateAndEtat(String date,boolean etat);
	List<Commande> findEnvoiByDate(String date);

}
