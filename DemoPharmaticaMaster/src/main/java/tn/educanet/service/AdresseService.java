package tn.educanet.service;

import java.util.List;

import tn.educanet.entities.Adresse;

public interface AdresseService {

	
	public void save(Adresse a);
	public void update(Long id,Adresse d);
	public	void delete(Long id);
	public	Adresse find(Long id);
	public	List<Adresse> findAll();
	public Adresse findByVilleAndNomRueAndCodePostal(String ville,String nomRue,int codePostal);
	
}
