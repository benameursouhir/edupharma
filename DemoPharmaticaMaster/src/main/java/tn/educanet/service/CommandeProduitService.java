package tn.educanet.service;

import java.util.List;

import tn.educanet.entities.Commande;
import tn.educanet.entities.CommandeProduit;

public interface CommandeProduitService {
	
	
	
	public void create(CommandeProduit c);
	public void update(Long id,CommandeProduit c) ;
	public void delete(long id);
	public CommandeProduit find(long id);
	List<CommandeProduit> getAllCommandeProduit();
	
	List<CommandeProduit> findByCommande(Commande c);
}
