package tn.educanet.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import tn.educanet.entities.Classe;
import tn.educanet.entities.Produit;

public interface ProduitService {

	public void create(Produit p,String userType,String tab,String classe,String classe1);
	public void update(Long id,String refP, String libelle, String forme, String codeBarre,
			Integer qteCarton,  String datePer, String tableau,
			Long prixAchatHt, Long prixVenteHt, Long tauxTva,
			Long prixVenteTtc, Long prixPublic,String classe,String classe1) ;
	public void delete(long id);
	public void deleteDef(long id);
	public void confirm(long id);
	public Produit find(long id);
	public List<Produit> getAllProduits();
	public Produit findByRefP(String refP);
	public Produit findByLibelle(String libelle);
	Produit findByCodeBarre(String codeBarre);
	List<Produit> findByDatePerAfter(String date);
	
	Page<Produit> findPerPage(int s,int i);
	Page<Produit> findByValide(Boolean valide,int s,int i);
	Page<Produit> findByOrganisation_Nom(String nomOrg,int s,int i);
}
