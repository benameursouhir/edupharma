package tn.educanet.service;

import java.util.List;

import tn.educanet.entities.Stock;

public interface StockService {
	
	public void save(Stock s);
	public void update(Stock s);
	public	void delete(Long id);
	public	Stock find(Long id);
	public	List<Stock> findAll();
	
	Stock findByNomStock(String nomStock);

}
