package tn.educanet.service;

import java.util.List;

import tn.educanet.entities.Organisation;
import tn.educanet.entities.OrganisationCompte;

public interface OrganisationCompteService {

	
	public void save(OrganisationCompte a);
	public void update(OrganisationCompte d);
	public	void delete(Long id);
	public	OrganisationCompte find(Long id);
	public	List<OrganisationCompte> findAll();
	List<OrganisationCompte> findByCompte(long compte);
	List<OrganisationCompte> findByOrganisationS(Organisation organisationS);
	OrganisationCompte findByOrganisationDesireAndOrganisationS(Organisation organisationdesire,Organisation organisationS);
}
