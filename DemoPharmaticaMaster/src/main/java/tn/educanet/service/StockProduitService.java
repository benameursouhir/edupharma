package tn.educanet.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import tn.educanet.entities.Produit;
import tn.educanet.entities.Stock;
import tn.educanet.entities.StockProduit;

public interface StockProduitService {
	
	
	public void save(StockProduit sp);
	public void update(StockProduit sp);
	public	void delete(Long id);
	public	StockProduit find(Long id);
	public	List<StockProduit> findAll();
	
	Page<StockProduit> findByStock_NomStockAndProduit_ValideAndProduit_Confirme(int s,int i,String nomStock,Boolean valide,Boolean confirme);
	StockProduit findByStockAndProduit(Stock s,Produit p);
	
	List<StockProduit> findByStockAndQteStock(Stock s,Integer qteStock);
	List<StockProduit> findByStockAndQteStockLessThan(Stock s,Integer seuil);
	List<StockProduit> findByStockAndProduit_DatePerAfter(Stock s,String date);
	
	List<StockProduit> findByProduitAndQteStockGreaterThan(Produit p,Integer qteStock );
	List<StockProduit> findByProduit(Produit p);
	List<StockProduit> findByStock_NomStock(String nomStock);
	
	
	Page<StockProduit> findByStock_NomStockAndProduit_RefPContaining(String nomStock,String refP, int s1, int i);
	Page<StockProduit> findByStock_NomStockAndProduit_LibelleContaining(String nomStock,String libelle,int s1, int i);
	Page<StockProduit> findByStock_NomStockAndProduit_Classe_NomClasse(String nomStock,String nomClasse,int s1, int i);
	
	List<StockProduit> findByStock_NomStockAndProduit_RefPContaining(String nomStock,String refP);
	List<StockProduit> findByStock_NomStockAndProduit_LibelleContaining(String nomStock,String libelle);
	List<StockProduit> findByStock_NomStockAndProduit_Classe_NomClasse(String nomStock,String nomClasse);
	
	
}
