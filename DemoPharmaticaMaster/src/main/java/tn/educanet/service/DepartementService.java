package tn.educanet.service;

import java.util.List;

import tn.educanet.entities.Departement;
import tn.educanet.entities.Organisation;

public interface DepartementService {

	
	public void save(Departement d);
	public void update(Departement d);
	public void delete(Long id);
	public Departement find(Long id);
	
	public Departement findByNomDep(String nomDep);
	 
	 List<Departement> findAllDepartement();
	 
	 
	 List<Departement> findByOrganisation(Organisation o);
	 Departement findByOrganisation_NomAndNomDep(String nom,String nomDep);
}
