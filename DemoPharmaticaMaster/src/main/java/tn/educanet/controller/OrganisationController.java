package tn.educanet.controller;

import java.sql.SQLDataException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tn.educanet.entities.Adresse;
import tn.educanet.entities.Organisation;
import tn.educanet.entities.Produit;
import tn.educanet.entities.Roles;
import tn.educanet.entities.Stock;
import tn.educanet.entities.StockProduit;
import tn.educanet.service.AdresseService;
import tn.educanet.service.CommandeService;
import tn.educanet.service.OrganisationService;
import tn.educanet.service.ProduitService;
import tn.educanet.service.RoleService;
import tn.educanet.service.StockProduitService;
import tn.educanet.service.StockService;



@Controller
public class OrganisationController {

	
	
	
		@Autowired
		OrganisationService organisationService;
		
		@Autowired
		AdresseService adresseService;
		
		
		@Autowired
		RoleService roleService;
	
		@Autowired
		  StockService stockService;
		
		@Autowired
		ProduitService produitService;
		
		@Autowired
		StockProduitService stockProduitService;
		
		
		
		@Autowired
		CommandeService commandeService;

		 @RequestMapping(value = "/register", method = RequestMethod.GET)
		    public String register(ModelMap model) {
			 String name = MediaController.getUser();
			 Organisation o=organisationService.findByEmail(name);
		     if(name==null ) return "login"; 
		       // Organisation o=organisationService.findByEmail(name);
				String nomorg=o.getNom();
			 model.addAttribute("username", nomorg);
		        model.addAttribute("organisation", new Organisation());
		     
		        model.addAttribute("adresses", adresseService.findAll());
		        return "admin/registrationform";
		    }

		
		
		
		
		 @RequestMapping(value = "/organisationn", method = RequestMethod.GET)
		    public String listUsers(ModelMap model,HttpServletRequest request) {
		     
		        String name = MediaController.getUser();
		        Organisation o=organisationService.findByEmail(name);
		        if(name==null) return "login"; 
		       
		        int i,s1;
				if(	request.getParameter("page")==null)
					 i=0;
				else i=Integer.parseInt(request.getParameter("page"));
				
				if(	request.getParameter("size")==null)
					 s1=10;
				else s1=Integer.parseInt(request.getParameter("size"));
		        
		        
		        
		        
				String nomorg=o.getNom();
		        model.addAttribute("page", organisationService.findByTypeNotLike("admin", s1, i));
		        model.addAttribute("fournisseurs", organisationService.findByType("fournisseur"));
		        model.addAttribute("pharmaciens", organisationService.findByType("pharmacie"));
		        model.addAttribute("organisation",o);
				model.addAttribute("username", nomorg);
				Stock s=stockService.findByNomStock(name);
		         model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
				 model.addAttribute("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
				 model.addAttribute("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
				 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
				 model.addAttribute("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
				 model.addAttribute("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
		        return "organisation";
		    }
		 
		 @Test
		 @RequestMapping(value = "/addOrg", method = RequestMethod.POST)
		    public ModelAndView addUser(@ModelAttribute("organisation") Organisation organisation, BindingResult result,HttpServletRequest request,final RedirectAttributes redirectAttributes) {
			 	try{
			 		String name = MediaController.getUser();
					 Organisation o=organisationService.findByEmail(name);
			 		if(name==null) return new ModelAndView("login"); 
			
			 
			 
			 
			 String rue= request.getParameter("nomRue");
			 String ville= request.getParameter("ville");
			 String postal= request.getParameter("codePostal");
			 String type= request.getParameter("type");
			organisationService.save(organisation, rue, ville, Integer.parseInt(postal), type);
			 redirectAttributes.addFlashAttribute("message", "Ajout reussis");
		
			   return new ModelAndView("redirect:/organisationn");

			
			 	}
			 	catch (IllegalArgumentException e) {
					// TODO: handle exception
			 		 ModelAndView modelAndView = new ModelAndView("admin/registrationform");
			 		String name = MediaController.getUser();
					 Organisation o=organisationService.findByEmail(name);	
						
					 Stock s=stockService.findByNomStock(name);
					 modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
					 modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
					 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
					 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
					 modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
					 modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
		    		
		    		 redirectAttributes.addFlashAttribute("message", e.getMessage());
		    		return  modelAndView;
				}
		    }
		 
		 
		  @RequestMapping(value="/updateOrg/{orgId}", method=RequestMethod.GET)
		    public ModelAndView prepareUpdate(@PathVariable("orgId") Long orgId){  
			  
		    	 ModelAndView modelAndView = new ModelAndView("admin/editOrganisation");
		    	 String name = MediaController.getUser();
		    	 if(name==null) return new ModelAndView("login"); 
		    	 Organisation o=organisationService.findByEmail(name);
					String nomorg=o.getNom();
					modelAndView.addObject("username", nomorg);
					 Stock s=stockService.findByNomStock(name);
					  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
					  modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
					 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
					 
					 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
			    	  modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
			    	  modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
		    	 
		    	 Organisation org=organisationService.find(orgId);
		    	 modelAndView.addObject("adresses", adresseService.findAll());
		       modelAndView.addObject("organisation", org);
		        return modelAndView;
		    }

		  @Test
		  @RequestMapping(value="/updateOrg/{orgId}", method=RequestMethod.POST)
		    public ModelAndView update(@ModelAttribute Organisation organisation,@PathVariable("orgId") Long orgId,HttpServletRequest request,final RedirectAttributes redirectAttributes){
	try{
			
			
			 String name = MediaController.getUser();
			 if(name==null) return new ModelAndView("login"); 
			 if(request.getParameter("modifier") != null){
			 
			 
			 
			 String rue= request.getParameter("nomRue");
			 String ville= request.getParameter("ville");
			 String postal= request.getParameter("codePostal");
			 System.out.println("codePostal"+postal);
			 String type= request.getParameter("type");

		organisationService.update(orgId,organisation, rue, ville, Integer.parseInt(postal), type);
			 redirectAttributes.addFlashAttribute("message", "Modification effectuer avec succ�es");
			
			   return new ModelAndView("redirect:/organisationn");

			
			 
			}
			else return new ModelAndView("redirect:/organisationn");
			
			
	}
		  
		catch(IllegalArgumentException e){
			ModelAndView modelAndView = new ModelAndView("admin/editOrganisation");
			 String name = MediaController.getUser();
			 Organisation o=organisationService.findByEmail(name);
			 Stock s=stockService.findByNomStock(name);
			 modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
			 modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
			 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
			 modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
			 modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
			 modelAndView.addObject("message",e.getMessage());
			 return modelAndView;
			
		}  
	catch (Exception e) {
						// TODO: handle exception
			  		 String message="Probl�me survenu lors de modification essayer plus tard";
			  		ModelAndView modelAndView= new ModelAndView ("redirect:/organisationn");
			  		
			  		 redirectAttributes.addFlashAttribute("message", message);
			  		return  modelAndView;
					}
}
		    @RequestMapping("/deleteOrg/{orgId}")
		    public String delete(@PathVariable("orgId") Long orgId) {
		    	try{
		    	organisationService.delete(orgId);
		        return "redirect:/organisationn";
		    	}
		    	catch (Exception e) {
					// TODO: handle exception
		    		  return "login"; 
				}
		    }
		    
		    @Test
		    @RequestMapping(value ="/deleteOrg/{orgId}", method = RequestMethod.POST)
		    public ModelAndView deleteDepartement(@ModelAttribute("organisation") Organisation organisation,@PathVariable("orgId") Long orgId, final RedirectAttributes redirectAttributes) throws SQLDataException 
		    {
		    	try{
		    		String name = MediaController.getUser();
			    	 if(name==null) return new ModelAndView("login"); 
		    		organisationService.delete(orgId);
		return new ModelAndView("redirect:/organisationn");
		    }
		    	catch (IllegalArgumentException e) {
					
		    		 //String message="cette organisation ne peut pas �tre supprimer";
		    		ModelAndView modelAndView= new ModelAndView ("redirect:/organisationn");
		    		
		    		 redirectAttributes.addFlashAttribute("message", e.getMessage());
		    		return  modelAndView;
				}
		    
		    
		    
		    }
		    
		    

		    @RequestMapping(value="/detailsOrg/{orgId}", method=RequestMethod.GET)
		    public ModelAndView details(@PathVariable("orgId") Long orgId,final RedirectAttributes redirectAttributes){
		    	try{
		    	 ModelAndView modelAndView = new ModelAndView("user/detailOrganisation");
		    	 
		    	 
		    	 String name = MediaController.getUser();
		    	 if(name==null) return new ModelAndView("login"); 
		    	 Organisation o=organisationService.findByEmail(name);
					String nomorg=o.getNom();
					modelAndView.addObject("username", nomorg);
					Stock ss=stockService.findByNomStock(name);
					  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
					  modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(ss,8).size());
					 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(ss,MediaController.calculDatePerim()).size());
					 
					 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
			    	  modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(ss,8));
			    	  modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(ss,MediaController.calculDatePerim()));
		    	 
		    	 Organisation org=organisationService.find(orgId);
		    	
		       modelAndView.addObject("organisation", org);
		        return modelAndView;
		    }
		    	catch (IllegalArgumentException e) {
					
		    		 //String message="cette organisation ne peut pas �tre supprimer";
		    		ModelAndView modelAndView= new ModelAndView ("redirect:/organisationn");
		    		
		    		 redirectAttributes.addFlashAttribute("message", e.getMessage());
		    		return  modelAndView;
				}
		    }
		    @Test
		    @RequestMapping(value="/detailsOrg/{orgId}", method=RequestMethod.POST)
		    public ModelAndView details(@ModelAttribute Organisation organisation,@PathVariable("orgId") Long orgId,HttpServletRequest request , final RedirectAttributes redirectAttributes){
		    	
		    	try{
		    		String name = MediaController.getUser();
			    	 if(name==null) return new ModelAndView("login"); 
		    	if(request.getParameter("modifier") != null){
		    	ModelAndView modelAndView = new ModelAndView("admin/editOrganisation");
		    	
		    
		    	Organisation o=organisationService.findByEmail(name);
				String nomorg=o.getNom();
				modelAndView.addObject("username", nomorg);
				Stock ss=stockService.findByNomStock(name);
				  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
				  modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(ss,8).size());
				 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(ss,MediaController.calculDatePerim()).size());
				 
				 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		    	  modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(ss,8));
		    	  modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(ss,MediaController.calculDatePerim()));		    	
		    	 
		    	Organisation org=organisationService.find(orgId);
		    	 modelAndView.addObject("organisation", org);
		       return modelAndView;}
		       else{
		    	   
		    	   ModelAndView modelAndView1 = new ModelAndView("redirect:/organisationn");
			   return modelAndView1;}}
		    	catch (IllegalArgumentException e) {
					
		    		 ModelAndView modelAndView= new ModelAndView ("redirect:/organisationn");
		    		redirectAttributes.addFlashAttribute("message", e.getMessage());
		    		return  modelAndView;
				}
		    	
		    	catch(Exception e) {
					// TODO: handle exception
		    		 String message="verifier vos d�tail";
		    		ModelAndView modelAndView= new ModelAndView ("redirect:/organisationn");
		    		
		    		 redirectAttributes.addFlashAttribute("message", message);
		    		return  modelAndView;
				}
		    
	}
		    
		  
		    @RequestMapping(value = "/searchOrg", method = RequestMethod.GET)
		    public String searchOrg(ModelMap model,HttpServletRequest request) {
		     
		        String name = MediaController.getUser();
		        Organisation o=organisationService.findByEmail(name);
		        if(name==null) return "login"; 
		       
		        int i,s1;
				if(	request.getParameter("page")==null)
					 i=0;
				else i=Integer.parseInt(request.getParameter("page"));
				
				if(	request.getParameter("size")==null)
					 s1=10;
				else s1=Integer.parseInt(request.getParameter("size"));
				String nomorg=o.getNom();
		        
		        String type=request.getParameter("typeS");
		        String nom=request.getParameter("nomS");
		        String rue=request.getParameter("nomrueS");
		        String ville=request.getParameter("villeS");
		        
				model.addAttribute("organisation",o);
				model.addAttribute("username", nomorg);
				Stock s=stockService.findByNomStock(name);
		         model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
				 model.addAttribute("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
				 model.addAttribute("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
				 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
				 model.addAttribute("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
				 model.addAttribute("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
				 if(!type.equals("") && !nom.equals("")  && !rue.equals("")  && !ville.equals("") ){
			        	model.addAttribute("page",organisationService.findByTypeAndAdresse_VilleAndAdresse_NomRueAndNom(type, ville, rue, nom, s1, i));
			        	
				 }
			     if(!type.equals("") && nom.equals("")  && !rue.equals("")  && !ville.equals("")){
			        	model.addAttribute("page",organisationService.findByTypeAndAdresse_VilleAndAdresse_NomRue(type, ville, rue, s1, i));
			        	
			     }
			     if(type.equals("") && !nom.equals("")  && !rue.equals("")  && !ville.equals("")){
			        	model.addAttribute("page",organisationService.findByNomAndAdresse_VilleAndAdresse_NomRue(nom, ville, rue, s1, i));
			        	}
			     if(!type.equals("") && nom.equals("")  && rue.equals("")  && !ville.equals("")){
			       	        	model.addAttribute("page",organisationService.findByTypeAndAdresse_Ville(type, ville, s1, i));
			       	        	}
			     if(type.equals("") && !nom.equals("")  && rue.equals("")  && !ville.equals("")){
			        	model.addAttribute("page",organisationService.findByNomAndAdresse_Ville(nom, ville, s1, i));
			        	}
			     if(!type.equals("") && nom.equals("")  && rue.equals("")  && ville.equals("")){
			        	model.addAttribute("page",organisationService.findByType(type,s1, i));
			        	}
			     if(type.equals("") && !nom.equals("")  && rue.equals("")  && ville.equals(""))
			        	model.addAttribute("page",organisationService.findByNom( nom, s1, i));
			        	
			     
			     if(type.equals("") && nom.equals("")  && rue.equals("")  && !ville.equals(""))
			        	model.addAttribute("page",organisationService.findByAdresse_Ville( ville, s1, i));
			      
			   
			     
		        return "organisation";
		    }
		    

		    
}
