package tn.educanet.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.SystemException;
import javax.transaction.Transaction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tn.educanet.entities.Adresse;
import tn.educanet.entities.Commande;
import tn.educanet.entities.CommandeProduit;
import tn.educanet.entities.Employee;
import tn.educanet.entities.Organisation;
import tn.educanet.entities.OrganisationCompte;
import tn.educanet.entities.Produit;
import tn.educanet.entities.Stock;
import tn.educanet.entities.StockProduit;
import tn.educanet.service.AdresseService;
import tn.educanet.service.CommandeProduitService;
import tn.educanet.service.CommandeService;
import tn.educanet.service.EmployeeService;
import tn.educanet.service.OrganisationCompteService;
import tn.educanet.service.OrganisationService;
import tn.educanet.service.ProduitService;
import tn.educanet.service.StockProduitService;
import tn.educanet.service.StockService;


@Controller
public class CommandeController {
	
	@Autowired
	CommandeService commandeService;
	
	@Autowired
	EmployeeService employeService;
	
	@Autowired
	OrganisationService organisationService;
	
	@Autowired
	ProduitService produitService;
	
	@Autowired
	CommandeProduitService commandeproduitService;
	
	@Autowired
	AdresseService adresseService;
	
	@Autowired
	StockService stockService;
	
	@Autowired
	StockProduitService stockProdService;
	@Autowired
	OrganisationCompteService organisationCompteService;
	

	

	
	 @RequestMapping(value = "/choixOrg", method = RequestMethod.GET)
		    public String choixOrg(ModelMap model) throws IllegalStateException, SystemException {

		 try{

		        model.addAttribute("commande", new Commande());
		        
		        String name = MediaController.getUser();
		        if(name==null) return "login"; 
		        
		        
				Organisation nomorg=organisationService.findByEmail(name);
				
				model.addAttribute("username", nomorg.getNom());
		       
		        model.addAttribute("organisations", organisationService.getAllOrganisation());
		       
		        Stock s=stockService.findByNomStock(name);
		        
		        model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(nomorg).size());
				 model.addAttribute("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
				 model.addAttribute("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
				 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(nomorg));
				 model.addAttribute("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
				 model.addAttribute("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));

		        return "user/choixOrg";
		 }
		 catch (Exception e) {

			 
			 return "user/commande";
		}
		    }

	
	
	
 @RequestMapping(value = "/ajoutCommandes", method = RequestMethod.GET)
	    public String listUsers(ModelMap model,HttpServletRequest request) throws IllegalStateException, SystemException {

	 try{

	        model.addAttribute("commande", new Commande());
	        
	        String name = MediaController.getUser();
	        if(name==null) return "login"; 
	        
	        String org=request.getParameter("org");
	        String str1[]=org.split(",");
	        
	        System.out.println("organisation"+org);
			Organisation nomorg=organisationService.findByEmail(name);
			
			model.addAttribute("username", nomorg.getNom());
	        model.addAttribute("employees", employeService.findByOrganisation(nomorg));
	        model.addAttribute("organisations", organisationService.findByAdresse_VilleAndAdresse_NomRueAndAdresse_CodePostal(str1[1], str1[2], Integer.parseInt(str1[3])));
	        model.addAttribute("produits", stockProdService.findByStock_NomStock(name));
	        Stock s=stockService.findByNomStock(name);
	        
	        model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(nomorg).size());
			 model.addAttribute("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 model.addAttribute("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(nomorg));
			 model.addAttribute("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
			 model.addAttribute("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));

	        return "user/commande";
	 }
	 catch (Exception e) {

		 
		 return "user/commande";
	}
	    }

	    @RequestMapping(value = "/addComm", method = RequestMethod.POST)
	    public String addComm(ModelMap model,@ModelAttribute("commande") Commande commande, BindingResult result,HttpServletRequest request,final RedirectAttributes redirectAttributes) {
	    	
	    try{
	    	//recherche de l'oraginsation qui a passe la commande(loggined)
	    	String name = MediaController.getUser();
			Organisation nomorg=organisationService.findByEmail(name);
			if(name==null) return "login"; 
	    	
			//recherche l'organisation dont laquelle envoyer la commande
	    	String organ= request.getParameter("org");
			System.out.println("adresse"+organ);
	    	String str[]=organ.split(",");
	    	Adresse adr=adresseService.findByVilleAndNomRueAndCodePostal(str[1], str[2],Integer.parseInt(str[3]) );
	    	Organisation org=organisationService.findByAdresse_VilleAndAdresse_NomRueAndAdresse_CodePostal(str[1], str[2],Integer.parseInt(str[3]));
	    	
	    	//recherche l'employee qui a passer la commande
	    	String empl= request.getParameter("emp");
	    	
	    	String str1[]=empl.split(",");
	    	Employee emp=employeService.findByCin(Integer.parseInt(str1[2]));
			
	       	
	    	
	       	//remplir 
	       	Date dateNow = new Date();
	       	
	    	SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
	    	String date_to_string = dateformatddMMyyyy.format(dateNow);
	    	commande.setDateComm(date_to_string);
	       	commande.setEtat(false);
	       	commande.setEmployee(emp);
	       	commande.setOrganisation(org);
	       	commande.setOrganisationEnvoi(nomorg);
	       	int n=nomorg.getNumComm()+1;
	       	commande.setBonLivraison(n);
	    	commandeService.create(commande);
	    	//modifier le numero de commande envoyee a chaque ajout d'une commande
	    	
	    	organisationService.updateNumCommande(nomorg,n);
	    	//remplir commandeProduit
	    	String prods=request.getParameter("listeProd");
			System.out.println("produits"+prods);
	    	String qtes=request.getParameter("listeQte");
			System.out.println("qtes"+qtes);
	    	String pr[]=prods.split(",");
	    	String qt[]=qtes.split(",");
	    	Commande c=commandeService.findByBonLivraison(n);
	    	for(int i=1;i<pr.length;i++){
	    		Produit p=produitService.findByRefP(pr[i]);
	    		commandeproduitService.create(new CommandeProduit(Integer.parseInt(qt[i]), p, c));
	    	}
	    	
	    	//creation du compte cas ou premiere commande
	    	if(organisationCompteService.findByOrganisationDesireAndOrganisationS(org, nomorg)==null)
	    		organisationCompteService.save(new OrganisationCompte(0, nomorg, org));
	    	redirectAttributes.addFlashAttribute("message","Commandes envoyés avec succée");
	  if(nomorg.getType().equals("pharmacie"))
	         return "redirect:/envoiListe";
	  else
		  return "redirect:/listeCommande";
	    }
	    catch (Exception e) {
			redirectAttributes.addFlashAttribute("message","Commandes non ajoutées verifier votre information");
	    	return  "user/commande";
		}
	    }
	    
	    
	    @RequestMapping(value = "/listeCommande", method = RequestMethod.GET)
	    public String listeCommandeReçu(ModelMap model,HttpServletRequest request) {
	      	String name = MediaController.getUser();
	      	if(name==null) return "login"; 
	      	Organisation nomorg=organisationService.findByEmail(name);
			 String nom=nomorg.getNom();
			
			  int i,s1;
				if(	request.getParameter("page")==null)
					 i=0;
				else i=Integer.parseInt(request.getParameter("page"));
				
				if(	request.getParameter("size")==null)
					 s1=10;
				else s1=Integer.parseInt(request.getParameter("size"));
			 
			 
			 Stock s=stockService.findByNomStock(name);
			 model.addAttribute("username", nom);
			 			
			 model.addAttribute("page",commandeService.findByOrganisationAndExisteDTrue(nomorg,s1,i));
			
			 
			 model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(nomorg).size());
			 model.addAttribute("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 model.addAttribute("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(nomorg));
			 model.addAttribute("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
			 model.addAttribute("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	

	        return "user/commandeRecu";
	    }
	    
	    
	    @RequestMapping(value = "/commandeNouv", method = RequestMethod.GET)
	    public String nouvCommandeReçu(ModelMap model,HttpServletRequest request) {
	      	String name = MediaController.getUser();
	      	if(name==null) return "login"; 
	      	Organisation nomorg=organisationService.findByEmail(name);
			 String nom=nomorg.getNom();
			  int i,s1;
				if(	request.getParameter("page")==null)
					 i=0;
				else i=Integer.parseInt(request.getParameter("page"));
				
				if(	request.getParameter("size")==null)
					 s1=10;
				else s1=Integer.parseInt(request.getParameter("size"));
			 Stock s=stockService.findByNomStock(name);
			 model.addAttribute("username", nom);
			 			
			 model.addAttribute("page",commandeService.findByOrganisationAndEtatFalse(nomorg,s1,i));
			 
			 model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(nomorg).size());
			 model.addAttribute("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 model.addAttribute("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(nomorg));
			 model.addAttribute("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
			 model.addAttribute("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	

	        return "user/nouvCommande";
	    }
	     
	  

	    
	    
	    @RequestMapping(value="/detailsCom/{comId}", method=RequestMethod.GET)
	    public ModelAndView details(@PathVariable("comId") Long comId){  
	    	String name = MediaController.getUser();
	    	if(name==null) return new ModelAndView("login"); 
	      	Organisation nomorg=organisationService.findByEmail(name);
	    	 ModelAndView modelAndView = new ModelAndView("user/detailCommande");
	    	 Commande c=commandeService.find(comId);
	    	 Stock s=stockService.findByNomStock(name);
	    	 List<CommandeProduit> cp=commandeproduitService.findByCommande(c);
	    	  modelAndView.addObject("username", nomorg.getNom());
	    	  modelAndView.addObject("commande", c);
	    	  modelAndView.addObject("commandeProduits", cp);
	    	  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(nomorg).size());
			  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 
			 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(nomorg));
	    	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
	    	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	        return modelAndView;
	    }
	    
	    @RequestMapping(value="/detailsCom/{comId}", method=RequestMethod.POST)
	    public ModelAndView detailsPost(@ModelAttribute Commande commande,@PathVariable("comId") Long comId,HttpServletRequest request , final RedirectAttributes redirectAttributes){
	    	String name = MediaController.getUser();
	    	if(name==null) return new ModelAndView("login"); 
	    	if(request.getParameter("modifier") != null){
		    	ModelAndView modelAndView = new ModelAndView("redirect:/imprime/{comId}");
		    	return modelAndView;
		    	}
	    	
	    	else {
	    		
	    		ModelAndView modelAndView1 = new ModelAndView("redirect:/listeCommande");
				   return modelAndView1;
	    	}
	    }
	    
	    
	    @RequestMapping(value="/imprime/{comId}", method=RequestMethod.GET)
	    public ModelAndView details1(@PathVariable("comId") Long comId){  
	    	
	    	String name = MediaController.getUser();
	    	if(name==null) return new ModelAndView("login"); 
	    	 Commande c=commandeService.find(comId);
	    	 Long tauxtva=(long) 0;
		    	Long base=(long) 0;
		    	//Long mthtva=(long) 0;
		    	long prix=0;
		    	int j=0;
		    	
		      	List<CommandeProduit> commandeProduits=commandeproduitService.findByCommande(c);
		      	
		      	if(c.getEtat()==false){
		      		if(!(name.equals(c.getOrganisationEnvoi().getEmail()))){
		      		c.setEtat(true);
		      		}
		      		commandeService.update(c);
		      		
		      	for(int i=0;i<commandeProduits.size();i++){
		      		Integer q=0;
		      		StockProduit sp=stockProdService.findByStockAndProduit(stockService.findByNomStock(name),commandeProduits.get(i).getProduit());
		      		if(sp.getQteStock() != 0){
		      		if(commandeProduits.get(i).getQte() < sp.getQteStock()){
		      			Integer newqtestock=(int) (sp.getQteStock() -commandeProduits.get(i).getQte());
		      			sp.setQteStock(newqtestock );
		      			 q=(int) commandeProduits.get(i).getQte();
		      			commandeProduits.get(i).setQteDonne(q);
		      			
		      			
		      		}
		      		else{
		      			sp.setQteStock(0 );
		      			q=(int) commandeProduits.get(i).getQte();
		      			commandeProduits.get(i).setQteDonne(q);
		      		}
		      		
		      		stockProdService.update(sp); 		
		      				      		
		      		}
		      		else{
		      			commandeProduits.get(i).setQteDonne(0);
		      			q=0;
		      		}
		      		
		      		Produit p=commandeProduits.get(i).getProduit();
		      		if(p.getTauxTva()>0){
		    			j++;
		    		tauxtva= tauxtva+p.getTauxTva();
		    		base = base+q*p.getPrixVenteHt();
		    		
		    		}
		      		
		      		commandeProduits.get(i).setPrixTotalHTVA(q*p.getPrixVenteHt());
		      		prix= prix+(q*p.getPrixVenteHt());
		      		
		      		commandeproduitService.update(commandeProduits.get(i).getComProdId(),commandeProduits.get(i));
		      		
		      		
		      	}
		      	Long txtva;
		      	if(j>0)
		      	txtva=tauxtva/j;
		      	else txtva=(long) 0;
		    	System.out.println("taux tva somme"+txtva);
		    	Long stauxtva=(txtva*base)/100;
		    	c.setTxTva(txtva);
		    	c.setNetHT(prix);
		    	c.setMtTva(stauxtva);
		    	c.setMtBrut(prix);
		    	c.setBase(base);
		    	c.setMontant(stauxtva);
		    	c.setTotalTtc(prix+stauxtva);
		    	
		    	commandeService.update(c);
		      	
		      	
		      }
		      	return   new ModelAndView("pdfViews","commandeProduits",commandeProduits);
	    	 
	       
	    }
	    
    
	    
	    @RequestMapping("/deleteComm/{comId}")
	    public String delete(@PathVariable("comId") Long comId,final RedirectAttributes redirectAttributes) {
	    	String name=MediaController.getUser();
	    	if(name==null) return "login";
	    	Commande c=commandeService.find(comId);
	    
	    	if(c.getOrganisationEnvoi().getEmail().equals(name) && c.getEtat()==false ){
	    		
	    		List<CommandeProduit> cp=commandeproduitService.findByCommande(c);
				for(int i=0;i<cp.size();i++)
				commandeproduitService.delete(cp.get(i).getComProdId());

				commandeService.delete(comId);
		    	redirectAttributes.addFlashAttribute("message","suuppression effectuer avec succées");
				return "redirect:/listeCommande";
	    	}
	    	
	    	if(c.getOrganisation().getEmail().equals(name)){
	    		
	    		c.setExisteD(false);
	    		commandeService.update(c);
	    						}
	    		
	    	else{ 
	    		c.setExisteS(false);
	    		
	    	commandeService.update(c);
	    	}
	    	
	    	
	    	
	    	if((c.isExisteD()==false && c.isExisteS()==false)||(organisationService.findByEmail(name).getType().equals("admin"))){
	    	List<CommandeProduit> cp=commandeproduitService.findByCommande(c);
	    	for(int i=0;i<cp.size();i++)
	    		commandeproduitService.delete(cp.get(i).getComProdId());
	    	
	    	commandeService.delete(comId);
	    	}
	    	redirectAttributes.addFlashAttribute("message","suuppression effectuer avec succées");

	        return "redirect:/listeCommande";
	    }
	    

	
	    @RequestMapping(value = "/envoiListe", method = RequestMethod.GET)
	    public String listeCommandeEnvoi(ModelMap model,HttpServletRequest request) {
	    	
	      	String name = MediaController.getUser();
	      	if(name==null) return "login"; 
	      	Organisation nomorg=organisationService.findByEmail(name);
			 String nom=nomorg.getNom();
			  int i,s1;
				if(	request.getParameter("page")==null)
					 i=0;
				else i=Integer.parseInt(request.getParameter("page"));
				
				if(	request.getParameter("size")==null)
					 s1=10;
				else s1=Integer.parseInt(request.getParameter("size"));
			 
			 
			 Stock s=stockService.findByNomStock(name);
			 model.addAttribute("username", nom);
			 			
			 model.addAttribute("page",commandeService.findByOrganisationEnvoiAndExisteSTrue(nomorg,s1,i));
			
			 
			 model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(nomorg).size());
			 model.addAttribute("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 model.addAttribute("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(nomorg));
			 model.addAttribute("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
			 model.addAttribute("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	

	        return "user/commandeEnvoi";
	    }
	    
	    
	    @RequestMapping(value = "/editComm/{comId}", method = RequestMethod.GET)
	    public ModelAndView prepareUpdate(@PathVariable("comId") Long comId){  
	   	 ModelAndView modelAndView = new ModelAndView("user/editCommande");
	   	String name = MediaController.getUser();
	   	if(name==null) return new ModelAndView("login"); 
	   	Organisation o=organisationService.findByEmail(name);
		String nomorg=o.getNom();
		modelAndView.addObject("username", nomorg);
	     Commande c=commandeService.find(comId);
	      modelAndView.addObject("commande", c);
	      List<CommandeProduit> cp=commandeproduitService.findByCommande(c);
	      modelAndView.addObject("cp", cp);
	     
	      modelAndView.addObject("nbrCP", cp.size());
	      modelAndView.addObject("produits", stockProdService.findAll());
	      modelAndView.addObject("employees", employeService.findByOrganisation(o));
	      modelAndView.addObject("organisations", organisationService.getAllOrganisation());
	      Stock s=stockService.findByNomStock(name);
		  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
		  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
		 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
		 
		 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
		  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	       return modelAndView;
	   }
	    
	    @RequestMapping(value="/editComm/{comId}", method=RequestMethod.POST)
	    public ModelAndView update(@ModelAttribute("commande") Commande commande,@PathVariable("comId") Long comId,HttpServletRequest request, final RedirectAttributes redirectAttributes){
	    	try{
	    		String name = MediaController.getUser();
				Organisation nomorg=organisationService.findByEmail(name);
				if(name==null) return new ModelAndView("login"); 
	    		ModelAndView modelAndView1 = new ModelAndView("redirect:/envoiListe");
	    		if(request.getParameter("modifier") != null){
	    			
	    			Commande c=commandeService.find(comId);
	    			//supprimer liste de commande produit pour construire la nouvelle
	    			List<CommandeProduit> cp=commandeproduitService.findByCommande(c);
	    			for(int i=0;i<cp.size();i++)
	    	    		commandeproduitService.delete(cp.get(i).getComProdId());
	    			
	    			//recherche l'organisation dont laquelle envoyer la commande
	    	    	String organ= request.getParameter("org");
	    			System.out.println("adresse"+organ);
	    	    	String str[]=organ.split(",");
	    	    	Adresse adr=adresseService.findByVilleAndNomRueAndCodePostal(str[1], str[2],Integer.parseInt(str[3]) );
	    		
	    	    	Organisation org=organisationService.findByAdresse(adr);
	    	    	
	    	    	//recherche l'employee qui a passer la commande
	    	    	String empl= request.getParameter("emp");
	    	    	
	    	    	String str1[]=empl.split(",");
	    	    	Employee emp=employeService.findByCin(Integer.parseInt(str1[2]));
	    			
	    	       	
	    	    	
	    	       	//remplir 
	    	       	Date dateNow = new Date();
	    	       	
	    	    	SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("yyyy-MM-dd-HH:mm:ss");
	    	    	String date_to_string = dateformatddMMyyyy.format(dateNow);
	    	    	c.setDateComm(date_to_string);
	    	       	c.setEtat(false);
	    	       	c.setEmployee(emp);
	    	       	c.setOrganisation(org);
	    	       	c.setOrganisationEnvoi(nomorg);
	    			commandeService.update(c);
	    			
	    			
	    			
	    			//remplir commandeProduit
	    	    	String prods=request.getParameter("listeProd");
	    			System.out.println("produits"+prods);
	    	    	String qtes=request.getParameter("listeQte");
	    			System.out.println("qtes"+qtes);
	    	    	String pr[]=prods.split(",");
	    	    	String qt[]=qtes.split(",");
	    	    	
	    	    	for(int i=1;i<pr.length;i++){
	    	    		Produit p=produitService.findByRefP(pr[i]);
	    	    		commandeproduitService.create(new CommandeProduit(Integer.parseInt(qt[i]), p, c));
	    		}
	    	    	redirectAttributes.addFlashAttribute("message","modification effectuer avec succées");
	    		return modelAndView1;
    			
	    		}
	    		else{
	    			redirectAttributes.addFlashAttribute("message","modification non réaliser verifier votre données");
	    			return new ModelAndView("redirect:/envoiListe");
	    		}
	    	}
	    catch(Exception e){
	    	redirectAttributes.addFlashAttribute("message","modification non réaliser verifier votre données");
	    	return new ModelAndView("redirect:/envoiListe");
	    	
	    }
	    }
	    
	    
	    @RequestMapping(value = "/searchCommRecu", method = RequestMethod.GET)
	    public String rechercheCommandeReçu(ModelMap model,HttpServletRequest request) {
	      	String name = MediaController.getUser();
	      	if(name==null) return "login"; 
	      	Organisation nomorg=organisationService.findByEmail(name);
			 String nom=nomorg.getNom();
			
//			  int i,s1;
//				if(	request.getParameter("page")==null)
//					 i=0;
//				else i=Integer.parseInt(request.getParameter("page"));
//				
//				if(	request.getParameter("size")==null)
//					 s1=10;
//				else s1=Integer.parseInt(request.getParameter("size"));
			 
			 
			 Stock s=stockService.findByNomStock(name);
			 model.addAttribute("username", nom);
			 model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(nomorg).size());
			 model.addAttribute("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 model.addAttribute("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(nomorg));
			 model.addAttribute("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
			 model.addAttribute("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
			 String dateComm=request.getParameter("dateComm");
			 String nomOrg=request.getParameter("nomOrg");
			 String etat1=request.getParameter("etatComm");
			 model.addAttribute("nomOrg", nomOrg);
			Boolean etat=Boolean.valueOf(etat1);
			 
			 if(!dateComm.equals("") && !nomOrg.equals("")&& !etat1.equals("") )
				 model.addAttribute("page",commandeService.findByOrganisation_NomAndDateCommAndEtat(nomOrg, dateComm, etat));
			 if(!dateComm.equals("") && !nomOrg.equals("")&& etat1.equals("") ) model.addAttribute("page", commandeService.findByOrganisation_NomAndDateComm(nomOrg, dateComm));
			 
			 if(dateComm.equals("") && !nomOrg.equals("")&& !etat1.equals("") )
			 model.addAttribute("page",commandeService.findByNomOrgAndEtat(nomOrg,etat));
			 
			 if(dateComm.equals("") && !nomOrg.equals("")&& etat1.equals("") )
				 model.addAttribute("page",commandeService.findByNomOrg(nomOrg));
			 
			 if(!dateComm.equals("") && nomOrg.equals("")&& !etat1.equals("") )
				 model.addAttribute("page",commandeService.findByDateAndEtat(dateComm,etat));
			 if(!dateComm.equals("") && nomOrg.equals("")&& etat1.equals("") )
				 model.addAttribute("page",commandeService.findByDate(dateComm));

	        return "user/searchComm";
	    }
	    
	    
	    @RequestMapping(value = "/searchCommRecuNnTraite", method = RequestMethod.GET)
	    public String rechercheCommandeReçuNnTraitee(ModelMap model,HttpServletRequest request) {
	      	String name = MediaController.getUser();
	      	if(name==null) return "login"; 
	      	Organisation nomorg=organisationService.findByEmail(name);
			 String nom=nomorg.getNom();
			
//			  int i,s1;
//				if(	request.getParameter("page")==null)
//					 i=0;
//				else i=Integer.parseInt(request.getParameter("page"));
//				
//				if(	request.getParameter("size")==null)
//					 s1=10;
//				else s1=Integer.parseInt(request.getParameter("size"));
			 
			 
			 Stock s=stockService.findByNomStock(name);
			 model.addAttribute("username", nom);
			 model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(nomorg).size());
			 model.addAttribute("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 model.addAttribute("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(nomorg));
			 model.addAttribute("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
			 model.addAttribute("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
			 String dateComm=request.getParameter("dateComm");
			 String nomOrg=request.getParameter("nomOrg");
		
			 model.addAttribute("nomOrg", nomOrg);
		
			 
			  if(!dateComm.equals("") && !nomOrg.equals("") ) model.addAttribute("page", commandeService.findByOrganisation_NomAndDateCommAndEtat(nomOrg, dateComm, false));
			 
			 if(dateComm.equals("") && !nomOrg.equals(""))
				 model.addAttribute("page",commandeService.findByNomOrgAndEtat(nomOrg, false));
			 if(!dateComm.equals("") && nomOrg.equals("") )
				 model.addAttribute("page",commandeService.findByDateAndEtat(dateComm, false));

	        return "user/searchComm";
	    }
	    
	    
	    @RequestMapping(value = "/searchCommEnvoi", method = RequestMethod.GET)
	    public String rechercheCommandeEnvoi(ModelMap model,HttpServletRequest request) {
	      	String name = MediaController.getUser();
	      	if(name==null) return "login"; 
	      	Organisation nomorg=organisationService.findByEmail(name);
			 String nom=nomorg.getNom();
			
//			  int i,s1;
//				if(	request.getParameter("page")==null)
//					 i=0;
//				else i=Integer.parseInt(request.getParameter("page"));
//				
//				if(	request.getParameter("size")==null)
//					 s1=10;
//				else s1=Integer.parseInt(request.getParameter("size"));
			 
			 
			 Stock s=stockService.findByNomStock(name);
			 model.addAttribute("username", nom);
			 model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(nomorg).size());
			 model.addAttribute("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 model.addAttribute("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(nomorg));
			 model.addAttribute("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
			 model.addAttribute("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
			 String dateComm=request.getParameter("dateComm");
			 String nomOrg=request.getParameter("nomOrg");
			 String etat1=request.getParameter("etatComm");
			 model.addAttribute("nomOrg", nomOrg);
			Boolean etat=Boolean.valueOf(etat1);
			 
			 if(!dateComm.equals("") && !nomOrg.equals("")&& !etat1.equals("") )
				 model.addAttribute("page",commandeService.findByOrganisationEnvoi_NomAndDateCommAndEtat(nomOrg, dateComm, etat));
			 
			 if(!dateComm.equals("") && !nomOrg.equals("")&& etat1.equals("") ) model.addAttribute("page", commandeService.findByOrganisationEnvoi_NomAndDateComm(nomOrg, dateComm));
			 
			 if(dateComm.equals("") && !nomOrg.equals("")&& !etat1.equals("") )
			 model.addAttribute("page",commandeService.findByOrganisationEnvoi_NomAndEtat(nomOrg,etat));
			 
			 if(dateComm.equals("") && !nomOrg.equals("")&& etat1.equals("") )
				 model.addAttribute("page",commandeService.findByOrganisationEnvoi_Nom(nomOrg));
			  
			 if(!dateComm.equals("") && nomOrg.equals("")&& !etat1.equals("") )
				 model.addAttribute("page",commandeService.findEnvoiByDateAndEtat(dateComm,etat));
			 
			 if(!dateComm.equals("") && nomOrg.equals("")&& etat1.equals("") )
				 model.addAttribute("page",commandeService.findEnvoiByDate(dateComm));

	        return "user/searchComm";
	    }
	    
	    
	    
	    }


