package tn.educanet.controller;

import java.sql.SQLDataException;

import javax.servlet.http.HttpServletRequest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tn.educanet.entities.Departement;
import tn.educanet.entities.Organisation;
import tn.educanet.entities.Stock;
import tn.educanet.service.CommandeService;
import tn.educanet.service.DepartementService;
import tn.educanet.service.OrganisationService;
import tn.educanet.service.ProduitService;
import tn.educanet.service.StockProduitService;
import tn.educanet.service.StockService;



@Controller
public class DepartementController {
	
	@Autowired
	DepartementService departementService;
	
	@Autowired
	OrganisationService orgService;
	@Autowired
	ProduitService produitService;
	@Autowired
	StockProduitService stockProdService;
	@Autowired
	CommandeService commandeService;
	@Autowired
	StockService stockService;

	@RequestMapping(value = "/listDepartments", method = RequestMethod.GET)
    public String listDepartements(ModelMap model) {
		
		String name = MediaController.getUser();
		 if(name==null) return "login"; 
		Organisation o=orgService.findByEmail(name);
		String nomorg=o.getNom();
		model.addAttribute("username", nomorg);
        model.addAttribute("departement", new Departement());
        model.addAttribute("departements", departementService.findByOrganisation(o));
        Stock s=stockService.findByNomStock(name);
        model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
		 model.addAttribute("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
		 model.addAttribute("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
		 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		 model.addAttribute("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
		 model.addAttribute("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
        
        
        return "user/departement";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public ModelAndView addDepartement(@ModelAttribute("departement") Departement departement, BindingResult result,HttpServletRequest request,final RedirectAttributes redirectAttributes) {
    	

    	try{
    		String name = MediaController.getUser();
    		 if(name==null) return new ModelAndView("login"); 
    		Organisation o=orgService.findByEmail(name);
    		Departement dep=departementService.findByOrganisation_NomAndNomDep(o.getNom(), departement.getNomDep());
    		
    		
      		if(dep != null){
      			
      			ModelAndView modelAndView = new ModelAndView("user/departement");
        		modelAndView.addObject ("departements", departementService.findAllDepartement());
        		modelAndView.addObject("username", o.getNom());
      			modelAndView.addObject("message1", "departement existant");
      		  Stock s=stockService.findByNomStock(name);
      		modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
      		modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
      		modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
      		modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
      		modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
      		modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
              
        			return modelAndView;
        		}
        		
    		
    	
      		else{
      			
      			departement.setOrganisation(o);
      			 departementService.save(departement);
      			 
      			
      			 String message="departement ajout� avec succ�es";
          		
      			ModelAndView modelAndView1 = new ModelAndView("redirect:/listDepartments");
          		 redirectAttributes.addFlashAttribute("message", message);
      	      return modelAndView1;}}
    	

    	catch (Exception e) {
      			// TODO: handle exception
         		 String message="v�rifier votre ajout";
         		ModelAndView modelAndView= new ModelAndView ("redirect:/listDepartments");
         		
         		 redirectAttributes.addFlashAttribute("message", message);
         		return  modelAndView;
     		}
         
      	    }

    @RequestMapping("/deleteDep/{depId}")
    public String deleteDepartement(@PathVariable("depId") Long depId) {
        departementService.delete(depId);
        return "redirect:/listDepartments";
    }
    
    @RequestMapping(value = "/deleteDep/{depId}", method = RequestMethod.POST)
    public ModelAndView deleteDepartement(@ModelAttribute("departement") Departement  departement,@PathVariable("depId") Long depId, final RedirectAttributes redirectAttributes) throws SQLDataException 
    {
    	try{
    		String message="le departement est supprim� avec succ�s";
   departementService.delete(depId);
   ModelAndView modelAndView= new ModelAndView ("redirect:/listDepartments");
	
	 redirectAttributes.addFlashAttribute("message", message);
	return  modelAndView;
    }
    	
    	
    	catch (IllegalArgumentException e) {
			
    		ModelAndView modelAndView= new ModelAndView ("redirect:/listDepartments");
    		
    		 redirectAttributes.addFlashAttribute("message", e.getMessage());
    		return  modelAndView;
		}
    	catch (Exception e) {
			// TODO: handle exception
    		 String message="le departement ne peut pas etre supprimer";
    		ModelAndView modelAndView= new ModelAndView ("redirect:/listDepartments");
    		
    		 redirectAttributes.addFlashAttribute("message", message);
    		return  modelAndView;
		}
    	
    
    
    
    }
    

    
    @RequestMapping(value = "/modiff/{depId}", method = RequestMethod.GET)
    public ModelAndView prepareUpdate(@PathVariable("depId") Long depId,final RedirectAttributes redirectAttributes){  
    	try{
   	 ModelAndView modelAndView = new ModelAndView("user/editDepartement");
   	String name = MediaController.getUser();
    if(name==null) return new ModelAndView("login"); 
   	Organisation o=orgService.findByEmail(name);
	String nomorg=o.getNom();
	modelAndView.addObject("username", nomorg);
     Departement dep=departementService.find(depId);
      modelAndView.addObject("departement", dep);
      
      Stock s=stockService.findByNomStock(name);
	  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
	  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
	 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
	 
	 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
       return modelAndView;
   }
catch (IllegalArgumentException e) {
			
    		ModelAndView modelAndView= new ModelAndView ("redirect:/listDepartments");
    		
    		 redirectAttributes.addFlashAttribute("message", e.getMessage());
    		return  modelAndView;
		}
    }
    
    
     
    @RequestMapping(value="/modiff/{depId}", method=RequestMethod.POST)
    public ModelAndView update(@ModelAttribute Departement departement,@PathVariable("depId") Long depId,HttpServletRequest request, final RedirectAttributes redirectAttributes){
    	
    	try{
    		ModelAndView modelAndView1 = new ModelAndView("redirect:/listDepartments");
    		String name = MediaController.getUser();
    		 if(name==null) return new ModelAndView("login"); 
    		Organisation o=orgService.findByEmail(name);
    		modelAndView1.addObject("username", o.getNom());
    	if(request.getParameter("modifier") != null){
    		
    		
    		 ModelAndView modelAndView = new ModelAndView("user/editDepartement");
    		 Departement dep=departementService.findByNomDep(departement.getNomDep());
    			modelAndView.addObject("username", o.getNom());
      			modelAndView.addObject("message1", "departement existant");
      		  Stock s=stockService.findByNomStock(name);
      		modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
      		modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
      		modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
      		modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
      		modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
      		modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
              
    		if(dep != null){
      			
        		
      			modelAndView.addObject("message1", "departement existant");
        		 

        		  
        		return modelAndView;
        		}
        		
        	
    	else{
        	
    		String message = "le departement est modifier avec succ�e";
    		departement.setOrganisation(o);
    		
            departementService.update(departement);
            redirectAttributes.addFlashAttribute("message", message);
    		
          return modelAndView1;}
    		
    	}	
    		

    	
    	else { 
    		
    		return  modelAndView1;
        }}
    	
	catch (IllegalArgumentException e) {
  			
     		ModelAndView modelAndView= new ModelAndView ("user/editDepartement");
     		String name = MediaController.getUser();
   		   		Organisation o=orgService.findByEmail(name);
   		String nomorg=o.getNom();
   		modelAndView.addObject("username", nomorg);
   		modelAndView.addObject("departement", new Departement());
   		modelAndView.addObject("departements", departementService.findByOrganisation(o));
           Stock s=stockService.findByNomStock(name);
           modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
           modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
           modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
           modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
           modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
           modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
           modelAndView.addObject("message",e.getMessage());
           
           
     		return  modelAndView;
 		}
    	
    	catch (Exception e) {
			// TODO: handle exception
   		 String message="verifier la modification";
   		ModelAndView modelAndView= new ModelAndView ("redirect:/listDepartments");
   		
   		 redirectAttributes.addFlashAttribute("message", message);
   		return  modelAndView;
		}

    }

    
    @RequestMapping(value = "/modiffdetails/{depId}", method = RequestMethod.GET)
    public ModelAndView prepareDetails(@PathVariable("depId") Long depId,final RedirectAttributes redirectAttributes){  
    	try{
   	 ModelAndView modelAndView = new ModelAndView("user/detailsDep");
   	String name = MediaController.getUser();
    if(name==null) return new ModelAndView("login"); 
   	Organisation o=orgService.findByEmail(name);
	String nomorg=o.getNom();
	modelAndView.addObject("username", nomorg);
     Departement dep=departementService.find(depId);
      modelAndView.addObject("departement", dep);
      
      Stock s=stockService.findByNomStock(name);
	  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
	  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
	 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
	 
	 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
       return modelAndView;
   }
	catch (IllegalArgumentException e) {
  			
     		ModelAndView modelAndView= new ModelAndView ("redirect:/listDepartments");
   
          redirectAttributes.addFlashAttribute("message", e.getMessage());
           
           
     		return  modelAndView;
 		}
    }
    
    @RequestMapping(value = "/modiffdetails/{depId}", method = RequestMethod.POST)
    public ModelAndView prepareDetails(@ModelAttribute Departement departement,@PathVariable("depId") Long depId,HttpServletRequest request,final RedirectAttributes redirectAttributes){  
    	
    	try{
    	if(request.getParameter("modifier") != null){
    		 ModelAndView modelAndView = new ModelAndView("user/editDepartement");
    		 String name = MediaController.getUser();
    		 if(name==null) return new ModelAndView("login"); 
    		 Organisation o=orgService.findByEmail(name);
    			String nomorg=o.getNom();
    			modelAndView.addObject("username", nomorg);
    			 Stock s=stockService.findByNomStock(name);
    			  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
    			  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
    			 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
    			 
    			 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
    			  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
    			  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
    	     Departement dep=departementService.find(depId);
    	      modelAndView.addObject("departement", dep);
    	       return modelAndView;
    		  
    		  

    	   }
    	  
    	   else   { ModelAndView modelAndView1 = new ModelAndView("redirect:/listDepartments");
    		   return modelAndView1;}

    	    }  catch (Exception e) {
    			// TODO: handle exception
       		 String message="v�rifier vos details";
       		ModelAndView modelAndView= new ModelAndView ("redirect:/listDepartments");
       		
       		 redirectAttributes.addFlashAttribute("message", message);
       		return  modelAndView;
   		} }
    	    
}

    


