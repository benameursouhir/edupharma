package tn.educanet.controller;

import java.sql.SQLDataException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tn.educanet.entities.Classe;
import tn.educanet.entities.Organisation;
import tn.educanet.entities.Produit;
import tn.educanet.entities.Stock;
import tn.educanet.entities.StockProduit;
import tn.educanet.service.ClasseService;
import tn.educanet.service.CommandeService;
import tn.educanet.service.OrganisationService;
import tn.educanet.service.ProduitService;
import tn.educanet.service.StockProduitService;
import tn.educanet.service.StockService;




@Controller
public class ProduitController {
	
	
	//@Resource	(name = "produitService")
	@Autowired
	private ProduitService produitService;
	
	@Autowired
	OrganisationService orgService;
	
	@Autowired
	StockService stockService;
	
	@Autowired
	StockProduitService stockProduitService;
	@Autowired
	ClasseService classeService;
	

	
	@Autowired
	CommandeService commandeService;
	
	@ModelAttribute("produit")
	public Produit getProduit(){
		return new Produit();
	}
	

	
	@RequestMapping(value = "/produits", method = RequestMethod.GET)
    public String listProduits(ModelMap model, Pageable p,HttpServletRequest request) {
		String name = MediaController.getUser();
		if(name==null) return "login"; 
		Organisation o=orgService.findByEmail(name);
		String nomorg=o.getNom();
	
		int i,s1;
			if(	request.getParameter("page")==null)
				 i=0;
			else i=Integer.parseInt(request.getParameter("page"));
			
			if(	request.getParameter("size")==null)
				 s1=10;
			else s1=Integer.parseInt(request.getParameter("size"));
			
		
		model.addAttribute("username", nomorg);
        model.addAttribute("page", this.produitService.findByValide(true, s1, i));
  
        Stock s=stockService.findByNomStock(name);
        model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
		 model.addAttribute("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
		 model.addAttribute("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
		 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		 model.addAttribute("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
		 model.addAttribute("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
        return "admin/produit";
    }
	

	
	

	
	
	@RequestMapping(value = "/nouvProd", method = RequestMethod.GET)
    public String nouvProd(ModelMap model) {
		String name = MediaController.getUser();
		if(name==null) return "login"; 
		Organisation o=orgService.findByEmail(name);
		String nomorg=o.getNom();
		model.addAttribute("username", nomorg);
		 model.addAttribute("produit", new Produit());
		 model.addAttribute("classe", classeService.findAllClasse());
		 
		 Stock s=stockService.findByNomStock(name);
	        model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
			 model.addAttribute("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
			 model.addAttribute("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
			 model.addAttribute("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
			 model.addAttribute("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
        return "user/ajoutProd";
    }
	
	
	
	
	
	
	
	 @RequestMapping(value = "/addProd", method = RequestMethod.POST)
	
	    public ModelAndView addProduit(@ModelAttribute("produit") Produit produit, BindingResult result,HttpServletRequest request, final RedirectAttributes redirectAttributes) {
		 try{
		
		 String name = MediaController.getUser();
		 Organisation o=orgService.findByEmail(name);
 		if(name==null) return new ModelAndView("login"); 
 		String message="Vous devez changer les donn�es suivantes:";
 	 String classe= request.getParameter("cl");
	 String classe1= request.getParameter("classe");
	 Produit prod =produitService.findByRefP(produit.getRefP());
 	 Produit prod1 =produitService.findByLibelle(produit.getLibelle());
 	 if(prod!= null) message+="reference produit";
 	 if(prod1!= null) message+="libelleproduit";
 	 if(message.equals("Vous devez changer les donn�es suivantes:")){
 		 produitService.create(produit,name,request.getParameter("tab"), classe, classe1);
 		
 		redirectAttributes.addFlashAttribute("message", "Produits ajout�es avec succ�es");
 		if(o.getType().equals("admin"))
 		return new ModelAndView("redirect:/produits");
 		else return new ModelAndView("redirect:/stockProduits");
 	 									}
 	 else{
 		redirectAttributes.addFlashAttribute("message", message);
 		
 		return new ModelAndView("redirect:/nouvProd");
 	 }
 	 
		 }
catch(IllegalArgumentException e){
	String name = MediaController.getUser();
	 Organisation o=orgService.findByEmail(name);
	 ModelAndView modelAndView=new ModelAndView("user/ajoutProd");
	 modelAndView.addObject("username", o.getNom());

	 modelAndView.addObject("classe", classeService.findAllClasse());
		 
		 Stock s=stockService.findByNomStock(name);
		 modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
		 modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
		 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
		 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		 modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
		 modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
		 modelAndView.addObject("message",e.getMessage());
	return modelAndView;
}
	    }


//    @RequestMapping("/deleteProd/{prodId}")
//    public String deleteDepartement(@PathVariable("prodId") Long prodId) {
// 
//    	produitService.delete(prodId);
//
//        return "redirect:/produits";
//    }
//    
    
    @RequestMapping(value ="/deleteProd/{prodId}", method = RequestMethod.POST)
    public ModelAndView deleteDepartement(@ModelAttribute("produit") Produit produit,@PathVariable("prodId") Long prodId, final RedirectAttributes redirectAttributes) throws SQLDataException 
    {
    	try{
    		
    		String name = MediaController.getUser();
      		if(name==null) return new ModelAndView("login"); 
    		
    		
    			produitService.delete(prodId);
    			 String message="ce produit est supprimer  avec succ�e";
    			 ModelAndView modelAndView= new ModelAndView ("redirect:/produits");
    	    	 redirectAttributes.addFlashAttribute("message", message);
    	    	 return  modelAndView;
    }
    	
    	catch (IllegalArgumentException e) {
			   ModelAndView modelAndView= new ModelAndView ("redirect:/produits");
    		
    		redirectAttributes.addFlashAttribute("message", e.getMessage());
    		return  modelAndView;
		}
    	catch (Exception e) {
			
    		 String message="ce produit ne peut pas �tre supprimer ";
    		ModelAndView modelAndView= new ModelAndView ("redirect:/produits");
    		
    		redirectAttributes.addFlashAttribute("message", message);
    		return  modelAndView;
		}
    }
    
    
    
    @RequestMapping(value = "/modifprod/{prodId}", method = RequestMethod.GET)
    public ModelAndView prepareUpdate(@PathVariable("prodId") Long prodId,final RedirectAttributes redirectAttributes){  
    	try{
   
   	String name = MediaController.getUser();
   	if(name==null) return new ModelAndView("name"); 
   ;
   	Organisation o=orgService.findByEmail(name);
   
   		ModelAndView modelAndView = new ModelAndView("editProduit");
   
	String nomorg=o.getNom();
	modelAndView.addObject("username", nomorg);
	modelAndView.addObject("classe", classeService.findAllClasse());
     Produit prod=produitService.find(prodId);
      modelAndView.addObject("produit", prod);
      Stock s=stockService.findByNomStock(name);
	  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
	  modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
	 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
	 
	 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	  modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
	  modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
       return modelAndView;
   }
    	catch (IllegalArgumentException e) {
			// TODO: handle exception
    		String name = MediaController.getUser();
    	   	if(name==null) return new ModelAndView("name"); 
    	    ModelAndView modelAndView;
    	   	Organisation o=orgService.findByEmail(name);
    	   	if(o.getType().equals("admin"))
    		modelAndView= new ModelAndView ("redirect:/produits");
    	   	else modelAndView= new ModelAndView ("redirect:/listeProdAjout");
    		redirectAttributes.addFlashAttribute("message", e.getMessage());
    		return  modelAndView;
		}
    }
    
    @RequestMapping(value="/modifprod/{prodId}", method=RequestMethod.POST)
    public ModelAndView update(@ModelAttribute("produit") Produit produit,@PathVariable("prodId") Long prodId,HttpServletRequest request, final RedirectAttributes redirectAttributes){
    	try{
    	
    		String name = MediaController.getUser();
    		Organisation o=orgService.findByEmail(name);
    		if(name==null) return new ModelAndView("login"); 
    	if(request.getParameter("modifier") != null){
    	String message="produit existant verifier les informations suivantes:";
    
    	String tab= request.getParameter("tab");
    	System.out.println("tab"+tab);
    	String date= request.getParameter("datePer");
    	String classe= request.getParameter("cl");
   	    String classe1= request.getParameter("classe");
    	
    		
    		
	       produitService.update(prodId, produit.getRefP(), produit.getLibelle(), produit.getForme(), produit.getCodeBarre(), produit.getQteCarton(), date, tab, produit.getPrixAchatHt(), produit.getPrixVenteHt(), produit.getTauxTva(), produit.getPrixVenteTtc(), produit.getPrixPublic(), classe, classe1);
	       redirectAttributes.addFlashAttribute("message", "produit modifier avec succ�es");
	       if(o.getType().equals("admin"))
	    		return new ModelAndView("redirect:/produits");	
	    	
	    	else return new ModelAndView("redirect:/stockProduits");
    		}
    		
	
    	else{ if(o.getType().equals("admin"))
    		return new ModelAndView("redirect:/produits");	
    	
    	else return new ModelAndView("redirect:/stockProduits");	 
    	
    	}
    	}
    	
    	catch(IllegalArgumentException e){
    		String name = MediaController.getUser();
    		 Organisation o=orgService.findByEmail(name);
    		 ModelAndView modelAndView=new ModelAndView("editProduit");
    		 modelAndView.addObject("username", o.getNom());

    		 modelAndView.addObject("classe", classeService.findAllClasse());
    			 
    			 Stock s=stockService.findByNomStock(name);
    			 modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
    			 modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
    			 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
    			 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
    			 modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
    			 modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
    			 modelAndView.addObject("message",e.getMessage());
    		return modelAndView;
    	}
	    	
    	catch (Exception e) {
			// TODO: handle exception
    		 String message="v�rifier votre modification";
    		ModelAndView modelAndView= new ModelAndView ("redirect:/produits");
    		
    		 redirectAttributes.addFlashAttribute("message", message);
    		return  modelAndView;
		}
        }
        
    
    @RequestMapping(value = "/detailsProd/{prodId}", method = RequestMethod.GET)
    public ModelAndView details(@PathVariable("prodId") Long prodId,final RedirectAttributes redirectAttributes){  
    	try{
    		
    		
    		
 
   	String name = MediaController.getUser();
   	System.out.println("prod Id"+prodId);
   	if(name==null) return new ModelAndView("login"); 
   	Organisation o=orgService.findByEmail(name);
   	
   	ModelAndView modelAndView = new ModelAndView("detailsProduit");
  
	String nomorg=o.getNom();
	modelAndView.addObject("username", nomorg);
     Produit prod=produitService.find(prodId);
      modelAndView.addObject("produit", prod);
      Stock s=stockService.findByNomStock(name);
	  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
	  modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
	 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
	 
	 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	  modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
	  modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
      
       return modelAndView;
    	}
    	catch (IllegalArgumentException e) {
			// TODO: handle exception
    		
    		String name = MediaController.getUser();
    	   	if(name==null) return new ModelAndView("name"); 
    	    ModelAndView modelAndView;
    	   	Organisation o=orgService.findByEmail(name);
    	   	if(o.getType().equals("admin"))
    		modelAndView= new ModelAndView ("redirect:/produits");
    	   	else modelAndView= new ModelAndView ("redirect:/listeProdAjout");
    		
    		redirectAttributes.addFlashAttribute("message", e.getMessage());
    		return  modelAndView;
		}
   }
    @RequestMapping(value = "/detailsProd/{prodId}", method = RequestMethod.POST)
    public ModelAndView details(@ModelAttribute("produit") Produit produit,@PathVariable("prodId") Long prodId,HttpServletRequest request, final RedirectAttributes redirectAttributes){  
    	try{ 
    		String name = MediaController.getUser();
    		if(name==null) return new ModelAndView("login");
    		Organisation o=orgService.findByEmail(name);
    	if(request.getParameter("modifier") != null){
    		 ModelAndView modelAndView = new ModelAndView("editProduit");
    		
    		   	
    		   
    			String nomorg=o.getNom();
    			modelAndView.addObject("username", nomorg);
    			Stock s=stockService.findByNomStock(name);
    			  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
    			  modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
    			 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
    			 
    			 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
    			  modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
    			  modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
    	     Produit prod=produitService.find(prodId);
    	      modelAndView.addObject("produit", prod);
    	       return modelAndView;
    		
	   
	   		
	
    	 }
    	else {
    		    	if(o.getType().equals("admin"))
    		return new ModelAndView("redirect:/produits");	
    		    	
    		    	else return new ModelAndView("redirect:/stockProduits");	 
    	}
    	
    	}
//    	 else   { ModelAndView modelAndView1 = new ModelAndView("redirect:/produits");
//  	   return modelAndView1;}}
    	catch (Exception e) {
			// TODO: handle exception
  		 String message="v�rifier vos d�tails";
  		ModelAndView modelAndView= new ModelAndView ("redirect:/produits");
  		
  		 redirectAttributes.addFlashAttribute("message", message);
  		return  modelAndView;
		}
  

      }
    
    
    
    @RequestMapping(value ="/confProd/{prodId}", method = RequestMethod.POST)
    public ModelAndView confirmProduit(@ModelAttribute("produit") Produit produit,@PathVariable("prodId") Long prodId, final RedirectAttributes redirectAttributes) throws SQLDataException 
    {
    
    try{
    	String name = MediaController.getUser();
	
		if(name==null) return new ModelAndView("login"); 
    	produitService.confirm(prodId);
    	redirectAttributes.addFlashAttribute("message", "Confirmation effectu�e avec succ�e");
    	return new ModelAndView("redirect:/produits");
    	
    }
    catch(Exception e){
    	redirectAttributes.addFlashAttribute("message", "Probleme survenus lors de confimartion");
    	return new ModelAndView("redirect:/produits");
    	
    }
    
    
    
    }
    @RequestMapping(value = "/listeProdAjout", method = RequestMethod.GET)
    public String listProduitsAjoutUser(ModelMap model, Pageable p,HttpServletRequest request) {
		String name = MediaController.getUser();
		if(name==null) return "login"; 
		Organisation o=orgService.findByEmail(name);
		String nomorg=o.getNom();
	
		int i,s1;
			if(	request.getParameter("page")==null)
				 i=0;
			else i=Integer.parseInt(request.getParameter("page"));
			
			if(	request.getParameter("size")==null)
				 s1=10;
			else s1=Integer.parseInt(request.getParameter("size"));
			
		
		model.addAttribute("username", nomorg);
        model.addAttribute("page", this.produitService.findByOrganisation_Nom(o.getNom(), s1, i));
  
        Stock s=stockService.findByNomStock(name);
        model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
		 model.addAttribute("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
		 model.addAttribute("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
		 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		 model.addAttribute("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
		 model.addAttribute("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
        return "user/listeProdAjou";
    }
    
    
    @RequestMapping(value ="/deleteProdAjout/{prodId}", method = RequestMethod.POST)
    public ModelAndView deleteProduitDef(@ModelAttribute("produit") Produit produit,@PathVariable("prodId") Long prodId, final RedirectAttributes redirectAttributes) throws SQLDataException 
    {
    	try{
    		
    		String name = MediaController.getUser();
      		if(name==null) return new ModelAndView("login"); 
    		
    		
    			produitService.deleteDef(prodId);
    			 String message="L'ajout de produit est annul�e  avec succ�e";
    			 ModelAndView modelAndView= new ModelAndView ("redirect:/listeProdAjout");
    	    	 redirectAttributes.addFlashAttribute("message", message);
    	    	 return  modelAndView;
    }
    	
    	catch (IllegalArgumentException e) {
			   ModelAndView modelAndView= new ModelAndView ("redirect:/listeProdAjout");
    		
    		redirectAttributes.addFlashAttribute("message", e.getMessage());
    		return  modelAndView;
		}
    	catch (Exception e) {
			
    		 String message="ce produit ne peut pas �tre supprimer ";
    		ModelAndView modelAndView= new ModelAndView ("redirect:/listeProdAjout");
    		
    		redirectAttributes.addFlashAttribute("message", message);
    		return  modelAndView;
		}
    }
    
    
    
    
    
    @RequestMapping(value = "/ImportData", method = RequestMethod.GET)
    public String importProduits(ModelMap model) {
    	return "admin/importData";
    }

//    @RequestMapping(value = "/save", method = RequestMethod.POST)
//    public String save(
//    		@ModelAttribute(value="FORM") UploadForm form,
//    		   BindingResult result) {
//    	if(!result.hasErrors()){
//    		FileOutputStream outputStream = null;
//    		String filename = form.getFile().getOriginalFilename();
//    		String filePath = "C:\\work\\webapplication\\src\\main\\webapp\\WEB-INF\\Upload\\"
//    		+filename;
//    		try {
//    		outputStream = new FileOutputStream(new File(filePath));
//    		outputStream.write(form.getFile().getFileItem().get());
//    		outputStream.close();
//    		importservice.readFromFile(filePath);
//    		} catch (Exception e) {
//    		System.out.println("Error while saving file");
//    		try {response.sendRedirect("FileUploadForm.html");
//    		} catch (IOException e1) {e1.printStackTrace();  } }
//    		try {  response.sendRedirect("ConsultClient.html");
//    		} catch (IOException e)
//    		{e.printStackTrace();}
//    		}else{
//    		try {  response.sendRedirect("FileUploadForm.html");
//    		} catch (IOException e) {e.printStackTrace();
//    		}
//    		}
//    		}



}
