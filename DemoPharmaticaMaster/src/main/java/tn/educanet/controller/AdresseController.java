package tn.educanet.controller;

import java.sql.SQLDataException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tn.educanet.entities.Adresse;
import tn.educanet.entities.Organisation;
import tn.educanet.entities.Stock;
import tn.educanet.service.AdresseService;
import tn.educanet.service.CommandeService;
import tn.educanet.service.OrganisationService;
import tn.educanet.service.ProduitService;
import tn.educanet.service.StockProduitService;
import tn.educanet.service.StockService;


@Controller
public class AdresseController {
	
	@Autowired
	AdresseService adresseService;
	
	@Autowired
	OrganisationService orgService;
	
	@Autowired
	ProduitService produitService;
	@Autowired
	StockProduitService stockProdService;
	@Autowired
	CommandeService commandeService;
	@Autowired
	StockService stockService;
	
	@RequestMapping(value = "/adresses", method = RequestMethod.GET)
    public String listAdresses(ModelMap model) {
		
		String name = MediaController.getUser();
		 if(name==null) return "login"; 
		Organisation o=orgService.findByEmail(name);
		String nomorg=(orgService.findByEmail(name)).getNom();
		model.addAttribute("username", nomorg);
        model.addAttribute("adresse", new Adresse());
        model.addAttribute("adresses", adresseService.findAll());
        Stock s=stockService.findByNomStock(name);
        model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
		 model.addAttribute("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
		 model.addAttribute("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
		 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		 model.addAttribute("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
		 model.addAttribute("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
        return "adresse";
    }


   
    
    
    @RequestMapping(value = "/addAdr", method = RequestMethod.POST)
    public ModelAndView addDepartement(@ModelAttribute("adresse") Adresse adresse, BindingResult result, final RedirectAttributes redirectAttributes) {
    	try{
    	Adresse adr=adresseService.findByVilleAndNomRueAndCodePostal(adresse.getVille(), adresse.getNomRue(), adresse.getCodePostal());
    	if(adr!=null){
    		
    		

  			ModelAndView modelAndView = new ModelAndView("adresse");
    		modelAndView.addObject ("adresses", adresseService.findAll());
  			modelAndView.addObject("message1", "adresse existe");
    		 

    		  
    		return modelAndView;
    		}
    	else{
    		
    		
    		String message="L'adresse est ajout� avec succ�es";
      		
  			
    	adresseService.save(adresse);
    	ModelAndView modelAndView1 = new ModelAndView("redirect:/adresses");
    	
 		 redirectAttributes.addFlashAttribute("message", message);
		
        return modelAndView1;
    }}catch (Exception e) {
		// TODO: handle exception
		 String message="v�rifier votre ajout";
		ModelAndView modelAndView= new ModelAndView ("redirect:/adresses");
		
		 redirectAttributes.addFlashAttribute("message", message);
		return  modelAndView;
	}
    }
    
    
    
    

    @RequestMapping("/delete/{adrId}")
    public String deleteDepartement(@PathVariable("adrId") Long adrId) {
        adresseService.delete(adrId);
        return "redirect:/adresses";
    }
    
    @RequestMapping(value = "/delete/{adrId}", method = RequestMethod.POST)
    public ModelAndView deleteDepartement(@ModelAttribute("adresse") Adresse  adresse,@PathVariable("adrId") Long adrId, final RedirectAttributes redirectAttributes) throws SQLDataException 
    {
    	try{
    		String message="L'adresse est supprim� avec succ�es";
      		
  			
    		adresseService.delete(adrId);
        	ModelAndView modelAndView = new ModelAndView("redirect:/adresses");
        	
     		 redirectAttributes.addFlashAttribute("message", message);
    		
return  modelAndView ;
    }
    	catch (Exception e) {
			// TODO: handle exception
    		 String message="cette adresse ne peut pas �tre supprimer";
    		ModelAndView modelAndView= new ModelAndView ("redirect:/adresses");
    		
    		 redirectAttributes.addFlashAttribute("message", message);
    		return  modelAndView;
		}
    
    
    
    }
    
    @RequestMapping(value = "/modif/{adrId}", method = RequestMethod.GET)
    public ModelAndView prepareUpdate(@PathVariable("adrId") Long adrId){  
   	 ModelAndView modelAndView = new ModelAndView("editAdresse");
   	 
   	String name = MediaController.getUser();
    if(name==null) return new ModelAndView("login"); 
	Organisation o=orgService.findByEmail(name);
     Adresse dep=adresseService.find(adrId);
     
     String nomorg=(orgService.findByEmail(name)).getNom();
		modelAndView.addObject("username", nomorg);
      modelAndView.addObject("adresse", dep);
      Stock s=stockService.findByNomStock(name);
	  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
	  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
	 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
	 
	 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
       return modelAndView;
   }
    
    @RequestMapping(value="/modif/{adrId}", method=RequestMethod.POST)
    public ModelAndView update(@ModelAttribute Adresse adresse,@PathVariable("adrId") Long adrId,HttpServletRequest request, final RedirectAttributes redirectAttributes){
    	try{
    	ModelAndView modelAndView1 = new ModelAndView("redirect:/adresses");
    	if(request.getParameter("modifier") != null){
    		ModelAndView modelAndView = new ModelAndView("editAdresse");
    		Adresse adr=adresseService.findByVilleAndNomRueAndCodePostal(adresse.getVille(), adresse.getNomRue(), adresse.getCodePostal());
    	
    		if(adr!=null){
      			modelAndView.addObject("message1", "adresse existe");
        		return modelAndView;
        		}
    		else{
    			 String message="cette adresse est modifi� avec succ�e";
    	    		 
    		adresseService.update(adrId,adresse);
    		redirectAttributes.addFlashAttribute("message", message);
         return  modelAndView1;}}
    	else   {return  modelAndView1;}}catch (Exception e) {
			// TODO: handle exception
   		 String message="v�rifier votre modification";
   		ModelAndView modelAndView= new ModelAndView ("redirect:/adresses");
   		
   		 redirectAttributes.addFlashAttribute("message", message);
   		return  modelAndView;
		}
        }
    
    
    
    @RequestMapping(value = "/details/{adrId}", method = RequestMethod.GET)
    public ModelAndView details(@PathVariable("adrId") Long adrId){ 
    	
   	 ModelAndView modelAndView = new ModelAndView("detailsAdr");
   	String name = MediaController.getUser();
    if(name==null) return new ModelAndView("login"); 
   	Organisation o=orgService.findByEmail(name);
	String nomorg=(orgService.findByEmail(name)).getNom();
	modelAndView.addObject("username", nomorg);
	
     Adresse dep=adresseService.find(adrId);
      modelAndView.addObject("adresse", dep);
      
      Stock s=stockService.findByNomStock(name);
	  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
	  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
	 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
	 
	 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
      
       return modelAndView;
   }
    @RequestMapping(value = "/details/{adrId}", method = RequestMethod.POST)
    
   public ModelAndView details(@ModelAttribute Adresse adresse,@PathVariable("adrId") Long adrId,HttpServletRequest request, final RedirectAttributes redirectAttributes){  
   try{
    	if(request.getParameter("modifier") != null){
	   ModelAndView modelAndView = new ModelAndView("editAdresse");
	   Adresse dep=adresseService.find(adrId);
	      modelAndView.addObject("adresse", dep);
	  
	  

    return  modelAndView;}
  
   else   { ModelAndView modelAndView1 = new ModelAndView("redirect:/adresses");
	   return modelAndView1;}

      }catch (Exception e) {
		// TODO: handle exception
  		 String message="v�rifier vos d�tails";
  		ModelAndView modelAndView= new ModelAndView ("redirect:/adresses");
  		
  		 redirectAttributes.addFlashAttribute("message", message);
  		return  modelAndView;
		}
}}
