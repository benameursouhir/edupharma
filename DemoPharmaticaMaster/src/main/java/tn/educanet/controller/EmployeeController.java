package tn.educanet.controller;




import static org.junit.Assert.*;

import java.sql.SQLDataException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tn.educanet.entities.Adresse;
import tn.educanet.entities.Employee;
import tn.educanet.entities.Organisation;
import tn.educanet.entities.Stock;
import tn.educanet.service.AdresseService;
import tn.educanet.service.CommandeService;
import tn.educanet.service.DepartementService;
import tn.educanet.service.EmployeeService;
import tn.educanet.service.OrganisationService;
import tn.educanet.service.ProduitService;
import tn.educanet.service.StockProduitService;
import tn.educanet.service.StockService;



@Controller
public class EmployeeController {
	
	/**
	 * Injection Services
	 */
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private DepartementService departementServiceImpl;
	@Autowired 
	private AdresseService adresseService;
	@Autowired
	OrganisationService orgService;
	
	
	@Autowired
	ProduitService produitService;
	@Autowired
	StockProduitService stockProdService;
	@Autowired
	CommandeService commandeService;
	@Autowired
	StockService stockService;

	  @RequestMapping(value = "/employee", method = RequestMethod.GET)
	    public String listUsers(ModelMap model) {
	        model.addAttribute("employe", new Employee());
	        String name = MediaController.getUser();
	        if(name==null) return "login"; 
	       Organisation o=orgService.findByEmail(name);
			String nomorg=o.getNom();
			model.addAttribute("username", nomorg);
		
	        model.addAttribute("employes", employeeService.findByOrganisation(o));
	        model.addAttribute("departements", departementServiceImpl.findByOrganisation(o));
	        model.addAttribute("adresses", adresseService.findAll());
	        
	        Stock s=stockService.findByNomStock(name);
	         model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
			 model.addAttribute("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 model.addAttribute("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
			 model.addAttribute("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
			 model.addAttribute("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	        return "user/employe";
	    }
	  @Test
    @RequestMapping(value = "/addEmp", method = RequestMethod.POST)
	    public ModelAndView addUser( @ModelAttribute("employe") Employee employe, BindingResult result,HttpServletRequest request,final RedirectAttributes redirectAttributes) {
	    	
	    	try{
	    		String name = MediaController.getUser();
		    	 if(name==null) return new ModelAndView("login"); 
		    	 System.out.println("ok");
		    	 String adresse= request.getParameter("adr");
		    	 System.out.println("adresse"+adresse);
				 String rue= request.getParameter("nomRue");
				 System.out.println("adresse"+adresse);
				 Integer postal=0;
				 String ville= request.getParameter("ville");
				 System.out.println("adresse"+request.getParameter("codePostal"));
				 if(request.getParameter("codePostal")!= "")	postal=Integer.parseInt(request.getParameter("codePostal"));
				 System.out.println("adresse");
			System.out.println("adresse"+adresse);
				 if(adresse != "" || adresse != null){
					 String str[]=adresse.split(",");
				    	rue=str[1];
				    	ville=str[0];
				    	postal=Integer.parseInt(str[2]);
				 }
			

				employeeService.save(name, employe, ville, rue, postal);
				ModelAndView m=new ModelAndView("redirect:/employee"); 
				redirectAttributes.addAttribute("message","Ajout employ�e effectuer avec succ�es");
				
				return m;
		
		    	 
	    		
	    	}
	    
	    	catch(IllegalArgumentException e) {
			
	    		String name = MediaController.getUser();
	    		ModelAndView modelAndView=new ModelAndView("user/employe");
	    		
				 Stock s=stockService.findByNomStock(name);
				 Organisation o=orgService.findByEmail(name);
			 		String nomorg=(orgService.findByEmail(name)).getNom();
			 		modelAndView.addObject("departements", departementServiceImpl.findByOrganisation(o));
			 		modelAndView.addObject("adresses", adresseService.findAll());
			 		modelAndView.addObject("username", nomorg);
			 		//modelAndView.addObject("employes", employeeService.findByOrganisation(o));
				  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
				  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
				 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
				 
				 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		    	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
		    	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	    		
		    	  modelAndView.addObject("message", e.getMessage());
	    		return  modelAndView;
			}
	    	catch(Exception e){
	    	
	    		ModelAndView modelAndView=new ModelAndView("redirect:/employee");
				
	    		 redirectAttributes.addFlashAttribute("message", "Probleme survenus lors de l'enregistrement r�ssayer plus tard");
	    		return  modelAndView;
	    		
	    	}
          }
            
	         
         
	    @RequestMapping(value="/updateEmp/{empId}", method=RequestMethod.GET)
	    public ModelAndView prepareUpdate(@PathVariable("empId") Long empId, final RedirectAttributes redirectAttributes){  
	    	try{
	    	 ModelAndView modelAndView = new ModelAndView("user/editEmploye");
	    	 String name = MediaController.getUser();
	    	 if(name==null) return new ModelAndView("login"); 
	    	 Organisation o=orgService.findByEmail(name);
	 		String nomorg=(orgService.findByEmail(name)).getNom();
	 		modelAndView.addObject("username", nomorg);
	    	 modelAndView.addObject("adresses", adresseService.findAll());
	    	 modelAndView.addObject("departements", departementServiceImpl.findAllDepartement());
	    	 Employee emp=employeeService.find(empId);
	         modelAndView.addObject("employee", emp);
	         
	         Stock s=stockService.findByNomStock(name);
			  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
			  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 
			 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	    	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
	    	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	       return modelAndView;
	    }
catch (IllegalArgumentException e) {
			
	    		
	    		ModelAndView modelAndView= new ModelAndView ("redirect:/employee");
	    		
	    		 redirectAttributes.addFlashAttribute("message", e.getMessage());
	    		return  modelAndView;
			}
	    	
catch (Exception e) {
			
	    		
	    		ModelAndView modelAndView= new ModelAndView ("redirect:/employee");
	    		
	    		 redirectAttributes.addFlashAttribute("message", "Probleme survenus lors de l'enregistrement r�ssayer plus tard");
	    		return  modelAndView;
			}	
	    }

	    @Test
	    @RequestMapping(value="/updateEmp/{empId}", method=RequestMethod.POST)
	    public ModelAndView update(@ModelAttribute Employee employee,@PathVariable("empId") Long empId,HttpServletRequest request, final RedirectAttributes redirectAttributes){
	    	try{
	    		 String name = MediaController.getUser();
		    	 if(name==null) return new ModelAndView("login"); 
		    	 if(request.getParameter("modifier") != null){
		    		 
		    		 String date= request.getParameter("datenaissance");
		 	    	String date1= request.getParameter("datedebut");
		 	    	 String adresse= request.getParameter("adr");
					 String rue= request.getParameter("nomRue");
					 String ville= request.getParameter("ville");
					 Integer postal=0;
					 if(request.getParameter("codePostal")!= "")	postal=Integer.parseInt(request.getParameter("codePostal"));
					 
				
					 if(adresse != "" || adresse != null){
						 String str[]=adresse.split(",");
					    	rue=str[1];
					    	ville=str[0];
					    	postal=Integer.parseInt(str[2]);
					 }
					 employeeService.update(empId, name, employee, ville, rue, postal);
					 redirectAttributes.addFlashAttribute("message","Modification effectuer avec succ�es");
		 	    	return new ModelAndView("redirect:/employee");
		 	    	
		 	    	
		    	 }
		    	 else return new ModelAndView("redirect:/employee");
	    	
	    	}
				  
	    	catch(IllegalArgumentException e) {
				
	    		String name = MediaController.getUser();
	    		ModelAndView modelAndView=new ModelAndView("user/editEmploye");
				 Stock s=stockService.findByNomStock(name);
				 Organisation o=orgService.findByEmail(name);
			 		String nomorg=(orgService.findByEmail(name)).getNom();
			 		modelAndView.addObject("departements", departementServiceImpl.findByOrganisation(o));
			 		modelAndView.addObject("adresses", adresseService.findAll());
			 		modelAndView.addObject("username", nomorg);
				  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
				  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
				 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
				 
				 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		    	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
		    	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	    		
	    		 redirectAttributes.addFlashAttribute("message", e.getMessage());
	    		return  modelAndView;
			}	
		        catch(Exception e) {
						ModelAndView modelAndView= new ModelAndView ("redirect:/employee");
		    			 redirectAttributes.addFlashAttribute("message", "Probleme survenus lors de l'enregistrement r�ssayer plus tard");
		    		return  modelAndView;
				}
	    	
	    }

	    @RequestMapping("/deleteEmp/{empId}")
	    public String deleteUser(@PathVariable("empId") Long empId) {

	    	employeeService.delete(empId);

	        return "redirect:/employee";
	    }
	    @RequestMapping(value = "/deleteEmp/{empId}", method = RequestMethod.POST)
	    public ModelAndView deleteDepartement(@ModelAttribute("employee") Employee  Employee,@PathVariable("empId") Long empId, final RedirectAttributes redirectAttributes) throws SQLDataException 
	    {
	    	try{
	    		String message="L'employ�e est supprim� avec succ�es";
	    		employeeService.delete(empId);
	    		ModelAndView modelAndView= new ModelAndView ("redirect:/employee");
      		redirectAttributes.addFlashAttribute("message", message);
    
      
     
	    	
	return  modelAndView;
	    }
	    	catch (IllegalArgumentException e) {
			
	    		
	    		ModelAndView modelAndView= new ModelAndView ("redirect:/employee");
	    		
	    		 redirectAttributes.addFlashAttribute("message", e.getMessage());
	    		return  modelAndView;
			}
	    	
catch (Exception e) {
			
	    		
	    		ModelAndView modelAndView= new ModelAndView ("redirect:/employee");
	    		
	    		 redirectAttributes.addFlashAttribute("message", "Probleme survenus lors de l'enregistrement r�ssayer plus tard");
	    		return  modelAndView;
			}
	    
	    
	    
	    }
	    
	    
	    @RequestMapping(value="/detailsEmp/{empId}", method=RequestMethod.GET)
	    public ModelAndView details(@PathVariable("empId") Long empId, final RedirectAttributes redirectAttributes){  
	    	try{
	    	 ModelAndView modelAndView = new ModelAndView("user/detailEmploye");
	    	 String name = MediaController.getUser();
	    	 if(name==null) return new ModelAndView("login"); 
	 		String nomorg=(orgService.findByEmail(name)).getNom();
	 		Organisation o=orgService.findByEmail(name);
	 		modelAndView.addObject("username", nomorg);
	    	 modelAndView.addObject("adresses", adresseService.findAll());
	    	 modelAndView.addObject("departements", departementServiceImpl.findAllDepartement());
	    	 Employee emp=employeeService.find(empId);
	         modelAndView.addObject("employee", emp);
	         
	         Stock s=stockService.findByNomStock(name);
			  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
			  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 
			 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	    	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
	    	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	       return modelAndView;
	    }
catch (IllegalArgumentException e) {
			
	    		
	    		ModelAndView modelAndView= new ModelAndView ("redirect:/employee");
	    		
	    		 redirectAttributes.addFlashAttribute("message", e.getMessage());
	    		return  modelAndView;
			}
	    	
catch (Exception e) {
			
	    		
	    		ModelAndView modelAndView= new ModelAndView ("redirect:/employee");
	    		
	    		 redirectAttributes.addFlashAttribute("message", "Probleme survenus lors de l'enregistrement r�ssayer plus tard");
	    		return  modelAndView;
			}
	    }
	    
	    @RequestMapping(value="/detailsEmp/{empId}", method=RequestMethod.POST)
	    public ModelAndView details(@ModelAttribute Employee employee,@PathVariable("empId") Long empId,HttpServletRequest request , final RedirectAttributes redirectAttributes){
	    	
	    	try{
	    	
	    	if(request.getParameter("modifier") != null){
	    	ModelAndView modelAndView = new ModelAndView("user/editEmploye");
	    	String name = MediaController.getUser();
	 		String nomorg=(orgService.findByEmail(name)).getNom();
	 		Organisation o=orgService.findByEmail(name);
	 		modelAndView.addObject("username", nomorg);
	 		 Stock s=stockService.findByNomStock(name);
			  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
			  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
			 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
			 
			 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	    	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
	    	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	    	
	    	 modelAndView.addObject("adresses", adresseService.findAll());
	    	 modelAndView.addObject("departements", departementServiceImpl.findAllDepartement());
	    	 Employee emp=employeeService.find(empId);
	         modelAndView.addObject("employee", emp);
	       return modelAndView;}
	       else{ModelAndView modelAndView1 = new ModelAndView("redirect:/employee");
		   return modelAndView1;}}
	    	
catch (IllegalArgumentException e) {
			
	    		
	    		ModelAndView modelAndView= new ModelAndView ("redirect:/employee");
	    		
	    		 redirectAttributes.addFlashAttribute("message", e.getMessage());
	    		return  modelAndView;
			}
	    	
catch (Exception e) {
			
	    		
	    		ModelAndView modelAndView= new ModelAndView ("redirect:/employee");
	    		
	    		 redirectAttributes.addFlashAttribute("message", "Probleme survenus lors de l'enregistrement r�ssayer plus tard");
	    		return  modelAndView;
			}
	    
}
}
