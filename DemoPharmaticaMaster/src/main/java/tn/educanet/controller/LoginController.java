package tn.educanet.controller;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tn.educanet.entities.Adresse;
import tn.educanet.entities.Organisation;
import tn.educanet.entities.Stock;
import tn.educanet.service.AdresseService;
import tn.educanet.service.CommandeService;
import tn.educanet.service.OrganisationService;
import tn.educanet.service.ProduitService;
import tn.educanet.service.RoleService;
import tn.educanet.service.StockProduitService;
import tn.educanet.service.StockService;


@Controller
public class LoginController {
 
	
	@Autowired
	OrganisationService orgService;
	@Autowired
	ProduitService produitService;
	@Autowired
	StockProduitService stockProdService;
	@Autowired
	CommandeService commandeService;
	@Autowired
	StockService stockService;
	@Autowired
	RoleService roleService;
	
	@Autowired
	AdresseService adresseService;
	
	

    @RequestMapping("/default")
    public String defaultAfterLogin(HttpServletRequest request) {
    	String name = MediaController.getUser();
    	
	Organisation nomorg=(orgService.findByEmail(name));
	String nom= nomorg.getType();
	System.out.println("je suis laa");
        if (nom.compareTo("fournisseur")==0) {
        	System.out.println("je suis laa1");
            return "redirect:listeCommande";
        }
        if (nom.compareTo("pharmacie")==0) {
        	System.out.println("je suis laa2");
            return "redirect:envoiListe";
        }
        else{
        	System.out.println("je suis laa3");
        return "redirect:organisationn";}
    }
	
	
	
 
	
	
	
	
	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String login(ModelMap model) {
			
		return "login";
 
	}
	
	@RequestMapping(value="/loginfailed", method = RequestMethod.GET)
	public String loginerror(ModelMap model) {
 
		model.addAttribute("error", "true");
		return "login";
 
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logout(ModelMap model) {
 
		return "login";
 
	}
	
	
	@RequestMapping(value="/detailsUser", method=RequestMethod.GET)
    public ModelAndView details(ModelMap model){  
      ModelAndView modelAndView = new ModelAndView("user/detailUser");
      
      
      String name = MediaController.getUser();
      if(name==null) return new ModelAndView("login"); 
      Organisation o=orgService.findByEmail(name);
   String nomorg=o.getNom();
   modelAndView.addObject("username", nomorg);
   Stock ss=stockService.findByNomStock(name);
     modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
     modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(ss,8).size());
    modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(ss,MediaController.calculDatePerim()).size());
    modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
    modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(ss,8));
    modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(ss,MediaController.calculDatePerim()));
  
 
 
   modelAndView.addObject("organisation", o);
    return modelAndView;

	}
	 @RequestMapping(value="/detailsUser/{orgId}", method=RequestMethod.POST)
	    public ModelAndView details(@ModelAttribute Organisation organisation,@PathVariable("orgId") Long orgId,HttpServletRequest request , final RedirectAttributes redirectAttributes){
	    	
	    	try{
	    	
	    	if(request.getParameter("modifier") != null){
	    	ModelAndView modelAndView = new ModelAndView("user/editUser");
	    	
	    	String name = MediaController.getUser();
	    	 if(name==null) return new ModelAndView("login"); 
	    	Organisation o=orgService.findByEmail(name);
			String nomorg=o.getNom();
			modelAndView.addObject("username", nomorg);
			Stock ss=stockService.findByNomStock(name);
			  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
			  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(ss,8).size());
			 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(ss,MediaController.calculDatePerim()).size());
			 
			 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	    	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(ss,8));
	    	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(ss,MediaController.calculDatePerim()));		    	
	    	 
	    	//Organisation org=orgService.find(orgId);
	    	 modelAndView.addObject("organisation", o);
	       return modelAndView;
	       
	    	}
	       else{
	    	   
	    	   ModelAndView modelAndView1 = new ModelAndView("redirect:/listeCommande");
		   return modelAndView1;}
	    }
	    	
	    	
	 	catch(Exception e) {
				// TODO: handle exception
	    		 String message="verifier vos d�tail";
	    		ModelAndView modelAndView= new ModelAndView ("redirect:/listeCommande");
	    		
	    		 redirectAttributes.addFlashAttribute("message", message);
	    		return  modelAndView;
			}
	 }
	 
	  @RequestMapping(value="/updateUser/{orgId}", method=RequestMethod.GET)
	    public ModelAndView prepareUpdate(ModelMap model){  
		  
	    	 ModelAndView modelAndView = new ModelAndView("user/editUser");
	    	 String name = MediaController.getUser();
	    	 if(name==null) return new ModelAndView("login"); 
	    	 Organisation o=orgService.findByEmail(name);
				String nomorg=o.getNom();
				modelAndView.addObject("username", nomorg);
				 Stock s=stockService.findByNomStock(name);
				  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
				  modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
				 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
				 
				 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		    	  modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
		    	  modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	    	 
	    	 //Organisation org=orgService.find(orgId);
	    	
	       modelAndView.addObject("organisation", o);
	        return modelAndView;
	    }
	  
	  @Test
	  @RequestMapping(value="/updateUser/{orgId}", method=RequestMethod.POST)
	    public ModelAndView update(@ModelAttribute Organisation organisation,@PathVariable("orgId") Long orgId,HttpServletRequest request,final RedirectAttributes redirectAttributes){
	try{
		
		System.out.println("here");
		
		 String name = MediaController.getUser();
		 if(name==null) return new ModelAndView("login"); 
		 if(request.getParameter("modifier") != null){
			 System.out.println("here"+name);
		 Organisation o=orgService.findByEmail(name);
		
		 
		 String rue= request.getParameter("nomRue");
		 String ville= request.getParameter("ville");
		 String postal= request.getParameter("codePostal");
		 String type= request.getParameter("type");
		
		 orgService.update(orgId,organisation, rue, ville, Integer.parseInt(postal), type);
		 
		 if(!o.getEmail().equals(organisation.getEmail())) return new ModelAndView("redirect:/logout");
		 
		 
		
		 redirectAttributes.addFlashAttribute("message", "Modification effectuer avec succ�e");
		
			 if(o.getType().equals("pharmacie"))
		   return new ModelAndView("redirect:/commandeNouv");
			 else  return new ModelAndView("redirect:/listeCommande");
		 
		
		 }
		 else return new ModelAndView("redirect:/organisationn");
	
	}
	  
	  
catch (IllegalArgumentException e) {
					
		  		 
	ModelAndView modelAndView = new ModelAndView("user/editUser");
	String name = MediaController.getUser();
	Organisation o=orgService.findByEmail(name);
	Stock s=stockService.findByNomStock(name);
	modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
	 modelAndView.addObject("prodFini",stockProdService.findByStockAndQteStockLessThan(s,8).size());
	 modelAndView.addObject("prodPerim",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
	 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	 modelAndView.addObject("prodFiniL",stockProdService.findByStockAndQteStockLessThan(s,8));
	 modelAndView.addObject("prodPerimL",stockProdService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
	 modelAndView.addObject("username",o.getNom());
	 modelAndView.addObject("message",e.getMessage());
	
		  		return  modelAndView;
				}
	catch(Exception e){
		
		ModelAndView modelAndView= new ModelAndView ("user/commandeRecu");
  		
 		 redirectAttributes.addFlashAttribute("message", "Probleme survenus lors de modification essayer plus tard");
 		return  modelAndView;
	}
}

	
}