package tn.educanet.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import tn.educanet.entities.Organisation;
import tn.educanet.entities.Roles;
import tn.educanet.service.OrganisationService;
import tn.educanet.service.RoleService;



@Controller
public class MediaController {
	
	

	@Autowired
	RoleService roleService;

	@Autowired
	OrganisationService orgService;
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String PageHome(ModelMap model) {
    	List<Roles> l=roleService.getAllRoles();
		System.out.println("liste user Roles");
		if(l == null || l.isEmpty()){
			
			System.out.println("login ok");
			Roles r=new Roles("ROLE_USER");
			Roles r1=new Roles("ROLE_ADMIN");
			//Roles r2=new Roles("pharmacie");
		roleService.create(r);
		roleService.create(r1);
		//roleService.create(r2);
		
		Organisation admin=new Organisation("admin", null, null, null, "admin", "admin@gmail.com", "admin");
		admin.setRoles(r1);
		orgService.save(admin, null, null, null, "admin");
		//UserRole ur=new UserRole(r1, admin);
		//userRoleService.create(ur);
			
		}
	
	
        return "login";
    }

    
//
//	@Autowired
//	static
//	CommandeService commandeService;
//	@Autowired
//	static
//	ProduitService produitService;
//	@Autowired
//	static
//	StockProduitService stockProdService;
//	@Autowired
//	static
//	OrganisationService organisationService;
//	@RequestMapping
//    public static void  info(ModelMap model  ) {
//    	  User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//  		String name = user.getUsername();
//    	  System.out.println("nom"+name);
//			Organisation nomorg=organisationService.findByEmail(name);
//			String nom=nomorg.getNom();
//			 System.out.println("nom"+nom);
//			 model.addAttribute("username", nom);
//    	 model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(nomorg).size());
//		 model.addAttribute("prodFini",stockProdService.findByQteStockLessThan(8).size());
//		 model.addAttribute("prodPerim",produitService.findByDatePerAfter(MediaController.calculDatePerim()).size());
//		 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(nomorg));
//		 model.addAttribute("prodFiniL",stockProdService.findByQteStockLessThan(8));
//		 model.addAttribute("prodPerimL",produitService.findByDatePerAfter(MediaController.calculDatePerim()));		
//	}
    


    
   public static String getUser(){
    	
    	try{
    	User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String name = user.getUsername();

		
		return name;
    	}
    	catch(Exception e){
    		
    		return null;
    	}
		
    }
 
 
   
   public static String calculDatePerim(){
   	Date dateNow = new Date();
      	
   	SimpleDateFormat dateformatddMMyyyy = new SimpleDateFormat("yyyy-MM-dd");
   	String date_to_string = dateformatddMMyyyy.format(dateNow);
   	
   	String str[]=date_to_string.split("-");
   	
   	Integer day=Integer.parseInt(str[2])-30;
   	String d=day.toString();
   	str[2]=d;
   	date_to_string=str[0]+"-"+str[1]+"-"+str[2];
   	return date_to_string;
   }
}
