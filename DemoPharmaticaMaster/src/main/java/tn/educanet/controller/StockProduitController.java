package tn.educanet.controller;

import java.sql.SQLDataException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import tn.educanet.entities.Organisation;
import tn.educanet.entities.Produit;
import tn.educanet.entities.Stock;
import tn.educanet.entities.StockProduit;
import tn.educanet.service.ClasseService;
import tn.educanet.service.CommandeService;
import tn.educanet.service.OrganisationService;
import tn.educanet.service.ProduitService;
import tn.educanet.service.StockProduitService;
import tn.educanet.service.StockService;
@Controller
public class StockProduitController {
	@Autowired
	private ProduitService produitService;
	
	@Autowired
	OrganisationService orgService;
	
	@Autowired
	StockService stockService;
	
	@Autowired
	StockProduitService stockProduitService;
	

	
	@Autowired
	CommandeService commandeService;
	@Autowired
	ClasseService classeService;
	
	
	@RequestMapping(value = "/stockProduits", method = RequestMethod.GET)
    public String listProduits(ModelMap model,HttpServletRequest request) {
		
		String name = MediaController.getUser();
		if(name==null) return "login"; 
		int i,s1;
		if(	request.getParameter("page")==null)
			 i=0;
		else i=Integer.parseInt(request.getParameter("page"));
		
		if(	request.getParameter("size")==null)
			 s1=10;
		else s1=Integer.parseInt(request.getParameter("size"));
		Organisation o=orgService.findByEmail(name);
		String nomorg=o.getNom();
		model.addAttribute("username", nomorg);
        model.addAttribute("page",stockProduitService.findByStock_NomStockAndProduit_ValideAndProduit_Confirme(s1, i, name, true,true));
        model.addAttribute("classe",classeService.findAllClasse());
        Stock s=stockService.findByNomStock(name);
        model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
		 model.addAttribute("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
		 model.addAttribute("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
		 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		 model.addAttribute("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
		 model.addAttribute("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
        return "user/produits";
    }
	
	
	
    @RequestMapping(value ="/modifQte", method = RequestMethod.POST)
    public ModelAndView modifProd( final RedirectAttributes redirectAttributes,HttpServletRequest request) throws SQLDataException 
    {
    	try{
    		String name = MediaController.getUser();
    		if(name==null) return new ModelAndView("login"); 
    		String organ= request.getParameter("prod");
	    	String str[]=organ.split(",");
	    	StockProduit sp=stockProduitService.findByStockAndProduit(stockService.findByNomStock(name), produitService.findByRefP(str[0]));

	    	
	    		sp.setQteStock(sp.getQteStock()+Integer.parseInt(request.getParameter("qte")));
	    		
	    		stockProduitService.update(sp);
	    		redirectAttributes.addFlashAttribute("message", "ajout quantit� r�ussite");

	    		
return new ModelAndView("redirect:/stockProduits");
    }
    	catch (Exception e) {
			// TODO: handle exception
    		 String message="On ne peut modifier la quantit� du stock du produits en question";
    		ModelAndView modelAndView= new ModelAndView ("redirect:/stockProduits");
    		
    		 redirectAttributes.addFlashAttribute("message", message);
    		return  modelAndView;
		}
    
    
    
    }
    
    
    
    
    @RequestMapping(value ="/deleteStockProd/{stockProdId}", method = RequestMethod.POST)
    public ModelAndView deleteDepartement(@ModelAttribute("produit") StockProduit produit,@PathVariable("stockProdId") Long stockProdId, final RedirectAttributes redirectAttributes) throws SQLDataException 
    {
    	try{
    		
    		stockProduitService.delete(stockProdId);
return new ModelAndView("redirect:/stockProduits");
    }
    	catch (Exception e) {
			// TODO: handle exception
    		 String message="ce produit ne peut pas �tre supprimer";
    		ModelAndView modelAndView= new ModelAndView ("redirect:/stockProduits");
    		
    		 redirectAttributes.addFlashAttribute("message", message);
    		return  modelAndView;
		}
    }
    
    @RequestMapping(value = "/modifprodUser/{stockProdId}", method = RequestMethod.GET)
    public ModelAndView prepareUpdate(@PathVariable("stockProdId") Long stockProdId){  
   	 ModelAndView modelAndView = new ModelAndView("user/editProduit");
   	String name = MediaController.getUser();
   	if(name==null) return new ModelAndView("login"); 
   	Organisation o=orgService.findByEmail(name);
	String nomorg=o.getNom();
	modelAndView.addObject("username", nomorg);
     StockProduit prod=stockProduitService.find(stockProdId);
     
      modelAndView.addObject("produit", prod);
      Stock s=stockService.findByNomStock(name);
	  modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
	  modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
	 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
	 
	 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	  modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
	  modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
       return modelAndView;
   }

    
    @RequestMapping(value="/modifprodUser/{stockProdId}", method=RequestMethod.POST)
    public ModelAndView update(@ModelAttribute("produit") StockProduit produit,@PathVariable("stockProdId") Long stockProdId,HttpServletRequest request, final RedirectAttributes redirectAttributes){
    	try{
    		
    		if(request.getParameter("modifier") != null){
    			StockProduit prod=stockProduitService.find(stockProdId);
    		prod.setQteStock(produit.getQteStock());
    	stockProduitService.update(prod);
    	redirectAttributes.addFlashAttribute("message", "modification quantit� r�ussite");

    		}
    		
    		else return  new ModelAndView ("redirect:/stockProduits");
    		return new ModelAndView("redirect:/stockProduits");
    	}
    	catch (Exception e) {
			// TODO: handle exception
    		 String message="v�rifier votre modification";
    		ModelAndView modelAndView= new ModelAndView ("redirect:/stockProduits");
    		
    		
    		 redirectAttributes.addFlashAttribute("message", message);
    		return  modelAndView;
		}
    	}
    
    @RequestMapping(value = "/detailsStockProd/{stockProdId}", method = RequestMethod.GET)
    public ModelAndView details(@PathVariable("stockProdId") Long stockProdId){  
   	 ModelAndView modelAndView = new ModelAndView("user/detailsProduit");
   
   	String name = MediaController.getUser();
   	if(name==null) return new ModelAndView("login"); 
   	Organisation o=orgService.findByEmail(name);
	String nomorg=o.getNom();
	modelAndView.addObject("username", nomorg);
     StockProduit prod=stockProduitService.find(stockProdId);
      modelAndView.addObject("produit", prod);
      
      Stock s=stockService.findByNomStock(name);
   modelAndView.addObject("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
	  modelAndView.addObject("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
	 modelAndView.addObject("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
	 
	 modelAndView.addObject("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
	  modelAndView.addObject("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
	  modelAndView.addObject("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
       return modelAndView;
   }
    
    @RequestMapping(value = "/detailsStockProd/{stockProdId}", method = RequestMethod.POST)
    public ModelAndView details(@ModelAttribute("produit") StockProduit produit,@PathVariable("stockProdId") Long stockProdId,HttpServletRequest request, final RedirectAttributes redirectAttributes){  
    	try{ 
    
    	if(request.getParameter("modifier") != null){
	return new ModelAndView("redirect:/modifprodUser/{stockProdId}");
	
    	 }
    	else {
    		
    	   		return new ModelAndView("redirect:/stockProduits");
    	
    		
    	}
    	}

    	catch (Exception e) {
			// TODO: handle exception
  		 String message="v�rifier vos d�tails";
  		ModelAndView modelAndView= new ModelAndView ("redirect:/stockProduits");
  		
  		 redirectAttributes.addFlashAttribute("message", message);
  		return  modelAndView;
		}
  

      }
    
    @RequestMapping(value ="/searchItems", method = RequestMethod.GET)
    public String searchProd(ModelMap model, final RedirectAttributes redirectAttributes,HttpServletRequest request) throws SQLDataException 
    {
    	String name = MediaController.getUser();
		if(name==null) return "login"; 
//		int i,s1;
//		if(	request.getParameter("page")==null)
//			 i=0;
//		else i=Integer.parseInt(request.getParameter("page"));
//		
//		if(	request.getParameter("size")==null)
//			 s1=1;
//		else s1=Integer.parseInt(request.getParameter("size"));
		Organisation o=orgService.findByEmail(name);
		String nomorg=o.getNom();
		String option=request.getParameter("option");
		
		String terme=request.getParameter("terme");
		if(!option.equals("refProd") && !option.equals("libelle"))
		 model.addAttribute("page",stockProduitService.findByStock_NomStockAndProduit_Classe_NomClasse(name, option));
		else 
			if(option.equals("refProd")) model.addAttribute("page",stockProduitService.findByStock_NomStockAndProduit_RefPContaining(name, terme));
			else model.addAttribute("page",stockProduitService.findByStock_NomStockAndProduit_LibelleContaining(name, terme));
		model.addAttribute("username", nomorg);
        model.addAttribute("page",stockProduitService.findByStock_NomStockAndProduit_Classe_NomClasse(name, option));
        model.addAttribute("classe",classeService.findAllClasse());
        Stock s=stockService.findByNomStock(name);
        model.addAttribute("nbrMsg",commandeService.findByOrganisationAndEtatFalse(o).size());
		 model.addAttribute("prodFini",stockProduitService.findByStockAndQteStockLessThan(s,8).size());
		 model.addAttribute("prodPerim",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()).size());
		 model.addAttribute("nbrMsgL",commandeService.findByOrganisationAndEtatFalse(o));
		 model.addAttribute("prodFiniL",stockProduitService.findByStockAndQteStockLessThan(s,8));
		 model.addAttribute("prodPerimL",stockProduitService.findByStockAndProduit_DatePerAfter(s,MediaController.calculDatePerim()));
        return "user/search";
    }
    
    
    
    
}
