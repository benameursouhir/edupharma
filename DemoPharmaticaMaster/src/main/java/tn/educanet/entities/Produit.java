package tn.educanet.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;


@Entity(name="PRODUIT")
public class Produit  implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@SequenceGenerator(name="seq", initialValue=1)
	@Id
	@Column(name = "PROD_ID", nullable = false)
	@GeneratedValue( generator="seq")
	private long prodId;
	
	@Column(name = "REF_PROD", unique=true)
	@Size(max=255)
	private String refP;
	
	@Column(name="LIBELLE", unique=true)
	@Size(max=255)
	private String libelle;
	
	@Column(name="FORME")
	@Size(max=255)
	private String forme;
	
	@Column(name="CODE_BARRE", unique=true)
	@Size(max=255)
	private String codeBarre;
	
	@Column(name="QTE_CARTON")
	private Integer qteCarton;
	
	@Column(name="DATE_PERIMATION")
	@Size(max=255)
	private String datePer;
	
	@Column(name="TABLEAU")
	@Size(max=255)
	private String tableau;
	
	@Column(name="PRIX_ACHAT_THT")
	 private Long prixAchatHt;
	
	@Column(name="PRIX_VENTE_HT")
	 private Long prixVenteHt;
	
	@Column(name="TAUX_TVA")
	 private Long tauxTva;
	
	@Column(name="PRIX_VENTE_TTC")
	 private Long prixVenteTtc;
	
	@Column(name="PRIX_PUBLIC")
	 private Long prixPublic;
	
	@Column(name="VALIDE")
	private Boolean valide;
	@Column(name="CONFIRME")
	private Boolean confirme;
	
	@ManyToOne(optional=false)
    @JoinColumn(name="CLASSE_ID",referencedColumnName="CLASSE_ID")
	private Classe classe;
	
	@ManyToOne(optional=false)
    @JoinColumn(name="ORG_ID",referencedColumnName="ORG_ID")
	private Organisation organisation;
	


	public Produit() {
		// TODO Auto-generated constructor stub
	}
	


	public Produit(String refP, String libelle, String forme, String codeBarre,
			Integer qteCarton,  String datePer, String tableau,
			Long prixAchatHt, Long prixVenteHt, Long tauxTva,
			Long prixVenteTtc, Long prixPublic,Classe classe) {
		super();
		this.refP = refP;
		this.libelle = libelle;
		this.forme = forme;
		this.codeBarre = codeBarre;
		this.qteCarton = qteCarton;
		this.classe=classe;
		this.datePer = datePer;
		this.tableau = tableau;
		this.prixAchatHt = prixAchatHt;
		this.prixVenteHt = prixVenteHt;
		this.tauxTva = tauxTva;
		this.prixVenteTtc = prixVenteTtc;
		this.prixPublic = prixPublic;
		
	}



	public Boolean getValide() {
		return valide;
	}



	public void setValide(Boolean valide) {
		this.valide = valide;
	}



	public long getProdId() {
		return prodId;
	}



	public void setProdId(long prodId) {
		this.prodId = prodId;
	}



	public String getRefP() {
		return refP;
	}



	public void setRefP(String refP) {
		this.refP = refP;
	}



	public String getLibelle() {
		return libelle;
	}



	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}



	public String getForme() {
		return forme;
	}



	public void setForme(String forme) {
		this.forme = forme;
	}



	public String getCodeBarre() {
		return codeBarre;
	}



	public void setCodeBarre(String codeBarre) {
		this.codeBarre = codeBarre;
	}



	public Integer getQteCarton() {
		return qteCarton;
	}



	public void setQteCarton(Integer qteCarton) {
		this.qteCarton = qteCarton;
	}






	public String getDatePer() {
		return datePer;
	}



	public void setDatePer(String datePer) {
		this.datePer = datePer;
	}



	public String getTableau() {
		return tableau;
	}



	public void setTableau(String tableau) {
		this.tableau = tableau;
	}



	public Long getPrixAchatHt() {
		return prixAchatHt;
	}



	public void setPrixAchatHt(Long prixAchatHt) {
		this.prixAchatHt = prixAchatHt;
	}



	public Long getPrixVenteHt() {
		return prixVenteHt;
	}



	public void setPrixVenteHt(Long prixVenteHt) {
		this.prixVenteHt = prixVenteHt;
	}



	public Long getTauxTva() {
		return tauxTva;
	}



	public void setTauxTva(Long tauxTva) {
		this.tauxTva = tauxTva;
	}



	public Long getPrixVenteTtc() {
		return prixVenteTtc;
	}



	public void setPrixVenteTtc(Long prixVenteTtc) {
		this.prixVenteTtc = prixVenteTtc;
	}



	public Long getPrixPublic() {
		return prixPublic;
	}



	public void setPrixPublic(Long prixPublic) {
		this.prixPublic = prixPublic;
	}




	
	public Boolean getConfirme() {
		return confirme;
	}



	public void setConfirme(Boolean confirme) {
		this.confirme = confirme;
	}



	public Classe getClasse() {
		return classe;
	}



	public void setClasse(Classe classe) {
		this.classe = classe;
	}



	public Organisation getOrganisation() {
		return organisation;
	}



	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}





	
	
	
	
}
