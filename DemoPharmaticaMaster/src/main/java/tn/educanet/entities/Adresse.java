package tn.educanet.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;


@Entity(name="ADRESSE")
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"NOM_RUE","CODE_POSTAL","VILLE"}))
public class Adresse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name="seq", initialValue=1)
	@Id
	@Column(name = "ADR_ID", nullable = false)
	@GeneratedValue( generator="seq")
	private long adrId;
	
	@Column(name = "NOM_RUE")
	@Size(max=255)
	private String nomRue;
	
	@Column(name = "CODE_POSTAL")
	private Integer codePostal;
	
	@Column(name = "VILLE")
	@Size(max=255)
	private String ville;
	
	public Adresse() {
		// TODO Auto-generated constructor stub
	}

	public Adresse(String nomRue, Integer codePostal, String ville) {
		super();
		this.nomRue = nomRue;
		this.codePostal = codePostal;
		this.ville = ville;
	}

	public long getAdrId() {
		return adrId;
	}

	public void setAdrId(long adrId) {
		this.adrId = adrId;
	}

	public String getNomRue() {
		return nomRue;
	}

	public void setNomRue(String nomRue) {
		this.nomRue = nomRue;
	}

	public Integer getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(Integer codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}
	
	
}
