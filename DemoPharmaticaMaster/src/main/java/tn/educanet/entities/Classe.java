package tn.educanet.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;


@Entity(name="CLASSE")
public class Classe implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@SequenceGenerator(name="seq", initialValue=1)
	@Id
	@Column(name = "CLASSE_ID", nullable = false)
	@GeneratedValue( generator="seq")
	private long classeId;
	
	@Column(name = "NOM_CLASSE", unique=true)
	@Size(max=255)
	private String nomClasse;

	public Classe() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Classe(String nomClasse) {
		super();
		this.nomClasse = nomClasse;
	}

	public long getClasseId() {
		return classeId;
	}

	public void setClasseId(long classeId) {
		this.classeId = classeId;
	}

	public String getNomClasse() {
		return nomClasse;
	}

	public void setNomClasse(String nomClasse) {
		this.nomClasse = nomClasse;
	}
	
	

}
