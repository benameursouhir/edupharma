package tn.educanet.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Version;
import javax.validation.constraints.Size;

@Entity(name="EMPLOYEE")
public class Employee implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name="seq", initialValue=1)
	@Id
	@Column(name = "EMP_ID", nullable = false)
	@GeneratedValue( generator="seq")
	private long empId;
	
	@Column(name="CIN",unique=true)
	private Integer cin;
	
	@Column(name="NOM")
	@Size(max=255)
	private String nom;
	
	@Column(name="PRENOM")
	@Size(max=255)
	private String prenom;

	@Column(name="DATE_NAISSANCE")
	@Size(max=255)
	private String datenaissance;
	
	@Column(name="GRADE")
	private String grade;
	
	@Column(name="SALAIRE")
	private Long salaire;

	@Column(name="DATE_DEBUT")
	@Size(max=255)
	private String datedebut;
	
	@Column(name="TEL",unique=true)
	private Integer tel;

	
	@ManyToOne(optional=true)
    @JoinColumn(name="ORG_ID",referencedColumnName="ORG_ID")
	private Organisation organisation;
	
	
	@ManyToOne(optional=true)
    @JoinColumn(name="DEP_ID",referencedColumnName="DEP_ID")
	private Departement departement;
	
	
	@ManyToOne(optional=true)
    @JoinColumn(name="ADR_ID",referencedColumnName="ADR_ID")
	private Adresse adresse;
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}
	
	
	public Employee(Integer cin,String nom,String prenom,String datenaissance,String grade,Long salaire,String datedebut,Integer tel){
		this.cin=cin;
		this.nom=nom;
		this.prenom=prenom;
		this.datenaissance=datenaissance;
		this.grade=grade;
		this.salaire=salaire;
		this.datedebut=datedebut;
		this.tel=tel;
		
	}


	public long getEmpId() {
		return empId;
	}


	public void setEmpId(long empId) {
		this.empId = empId;
	}


	public Integer getCin() {
		return cin;
	}


	public void setCin(Integer cin) {
		this.cin = cin;
	}


	public String getNom() {
		return nom;
	}


	public void setNom(String nom) {
		this.nom = nom;
	}


	public String getPrenom() {
		return prenom;
	}


	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}


	public String getDatenaissance() {
		return datenaissance;
	}


	public void setDatenaissance(String datenaissance) {
		this.datenaissance = datenaissance;
	}


	public String getGrade() {
		return grade;
	}


	public void setGrade(String grade) {
		this.grade = grade;
	}


	public Long getSalaire() {
		return salaire;
	}


	public void setSalaire(Long salaire) {
		this.salaire = salaire;
	}


	public String getDatedebut() {
		return datedebut;
	}


	public void setDatedebut(String datedebut) {
		this.datedebut = datedebut;
	}


	public Integer getTel() {
		return tel;
	}


	public void setTel(Integer tel) {
		this.tel = tel;
	}


	public Departement getDepartement() {
		return departement;
	}


	public void setDepartement(Departement departement) {
		this.departement = departement;
	}


	public Adresse getAdresse() {
		return adresse;
	}


	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}


	public Organisation getOrganisation() {
		return organisation;
	}


	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}
	

	
	
	


	

	
	
	
//	
//	public Organisation getOrganisation() {
//		return organisation;
//	}
//	public void setOrganisation(Organisation organisation) {
//		this.organisation=organisation;
//	}


	
	
	
	}
