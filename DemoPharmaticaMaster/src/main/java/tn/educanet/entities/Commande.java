package tn.educanet.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
@Entity(name="COMMANDE")
public class Commande implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name="seq", initialValue=1)
	@Id
	@Column(name = "COM_ID", nullable = false)
	@GeneratedValue( generator="seq")
	private long comId;
	
	@Column(name = "Bon_Livraison",unique=true)
	private int bonLivraison;
	
	
	@Column(name = "DATE_COMM")
	private String  dateComm;
	
	
	
	@Column(name = "ETAT")
	private Boolean etat;
	
	@Column(name = "MT_BRUT")
	private long mtBrut;
	
	@Column(name = "NET_HT")
	private long netHT;
	
	@Column(name = "TX_TVA")
	private long txTva;
	
	@Column(name = "MT_TVA")
	private long mtTva;
	
	@Column(name = "BASE")
	private long base;
	
	@Column(name = "MONTANT")
    private long montant;
	
	@Column(name = "TOTAL_TTC")
	private long totalTtc;
	
	@Column(name = "EXISTE_S")
	private boolean existeS;
	
	@Column(name = "EXISTE_D")
	private boolean existeD;
	
	@ManyToOne(optional=true)
    @JoinColumn(name="ORG_IDR",referencedColumnName="ORG_ID")
	private Organisation organisation;
	
	@ManyToOne(optional=true)
    @JoinColumn(name="ORG_IDE",referencedColumnName="ORG_ID")
	private Organisation organisationEnvoi;
	   
	
	@ManyToOne(optional=true)
    @JoinColumn(name="EMP_ID",referencedColumnName="EMP_ID")	
	private Employee employee;
	
	
	public Commande() {
		// TODO Auto-generated constructor stub
		this.existeD=true;
		this.existeS=true;
	}
	

	public Commande(int bonLivraison, String dateComm, Boolean etat, long mtBrut,
			long netHT, long txTva, long mtTva, long base, long montant,
			long totalTtc) {
		
		
		this.bonLivraison = bonLivraison;
		this.dateComm = dateComm;
		this.etat = etat;
		this.mtBrut = mtBrut;
		this.netHT = netHT;
		this.txTva = txTva;
		this.mtTva = mtTva;
		this.base = base;
		this.montant = montant;
		this.totalTtc = totalTtc;
		this.existeD=true;
		this.existeS=true;
		
	}


	public long getComId() {
		return comId;
	}


	public void setComId(long comId) {
		this.comId = comId;
	}


	public int getBonLivraison() {
		return bonLivraison;
	}


	public void setBonLivraison(int bonLivraison) {
		this.bonLivraison = bonLivraison;
	}


	public String getDateComm() {
		return dateComm;
	}


	public void setDateComm(String dateComm) {
		this.dateComm = dateComm;
	}


	public Boolean getEtat() {
		return etat;
	}


	public void setEtat(Boolean etat) {
		this.etat = etat;
	}


	public long getMtBrut() {
		return mtBrut;
	}


	public void setMtBrut(long mtBrut) {
		this.mtBrut = mtBrut;
	}


	public long getNetHT() {
		return netHT;
	}


	public void setNetHT(long netHT) {
		this.netHT = netHT;
	}


	public long getTxTva() {
		return txTva;
	}


	public void setTxTva(long txTva) {
		this.txTva = txTva;
	}


	public long getMtTva() {
		return mtTva;
	}


	public void setMtTva(long mtTva) {
		this.mtTva = mtTva;
	}


	public long getBase() {
		return base;
	}


	public void setBase(long base) {
		this.base = base;
	}


	public long getMontant() {
		return montant;
	}


	public void setMontant(long montant) {
		this.montant = montant;
	}


	public long getTotalTtc() {
		return totalTtc;
	}


	public void setTotalTtc(long totalTtc) {
		this.totalTtc = totalTtc;
	}


	


	public Organisation getOrganisation() {
		return organisation;
	}


	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}


	public Employee getEmployee() {
		return employee;
	}


	public Organisation getOrganisationEnvoi() {
		return organisationEnvoi;
	}


	public void setOrganisationEnvoi(Organisation organisationEnvoi) {
		this.organisationEnvoi = organisationEnvoi;
	}


	public void setEmployee(Employee employee) {
		this.employee = employee;
	}


	public boolean isExisteS() {
		return existeS;
	}


	public void setExisteS(boolean existeS) {
		this.existeS = existeS;
	}


	public boolean isExisteD() {
		return existeD;
	}


	public void setExisteD(boolean existeD) {
		this.existeD = existeD;
	}


	
	
	
	

	
	
}
