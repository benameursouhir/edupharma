package tn.educanet.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity(name="COMMANDE_PRODUIT")
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"PROD_ID","COM_ID"}))
public class CommandeProduit implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name="seq", initialValue=1)
	@Id
	@GeneratedValue( generator="seq")
	@Column(name = "COMM_PROD_ID", nullable = false)
	private long comProdId;
	
	@Column(name="QTE")
	private Integer qte;
	
	@Column(name="QTE_DONNES")
	private Integer qteDonne;
	
	@Column(name="PRIX_TOTAL_HTVA")
	private long prixTotalHTVA;
	
	
	
	@ManyToOne(optional=false)
    @JoinColumn(name="PROD_ID",referencedColumnName="PROD_ID")
	private Produit produit;
	
	
	@ManyToOne(optional=false)
    @JoinColumn(name="COM_ID",referencedColumnName="COM_ID")
    private Commande commande;
	
	
	
	public CommandeProduit() {
		// TODO Auto-generated constructor stub
	}
	public CommandeProduit(Integer qte,long prixTotalHTVA) {
	
		this.prixTotalHTVA=prixTotalHTVA;
		this.qte=qte;
		
		
		
	}
	
	
	
	public CommandeProduit(Integer qte, Produit produit, Commande commande) {
		super();
		this.qte = qte;
		this.produit = produit;
		this.commande = commande;
		this.qteDonne=0;
	}
	public long getComProdId() {
		return comProdId;
	}
	public void setComProdId(long comProdId) {
		this.comProdId = comProdId;
	}
	public long getQte() {
		return qte;
	}
	public void setQte(Integer qte) {
		this.qte = qte;
	}
	public Integer getQteDonne() {
		return qteDonne;
	}
	public void setQteDonne(Integer qteDonne) {
		this.qteDonne = qteDonne;
	}
	public long getPrixTotalHTVA() {
		return prixTotalHTVA;
	}
	public void setPrixTotalHTVA(long prixTotalHTVA) {
		this.prixTotalHTVA = prixTotalHTVA;
	}
	public Produit getProduit() {
		return produit;
	}
	public void setProduit(Produit produit) {
		this.produit = produit;
	}
	public Commande getCommande() {
		return commande;
	}
	public void setCommande(Commande commande) {
		this.commande = commande;
	}
	
	

	
	
	
}
