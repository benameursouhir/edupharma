package tn.educanet.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity(name="ORGANISATION_COMPTE")
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"ORG_ID_S","ORG_ID_D","COMPTE"}))
public class OrganisationCompte implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@SequenceGenerator(name="seq", initialValue=1)
	@Id
	@Column(name = "ORG_COMPTE_ID", nullable = false)
	@GeneratedValue( generator="seq")
	private long orgCompteId;
	
	
	@Column(name="COMPTE")
	 private long compte;
	
	@ManyToOne(optional=true)
    @JoinColumn(name="ORG_ID_S",referencedColumnName="ORG_ID")
	private Organisation organisationS;
	
	@ManyToOne(optional=true)
    @JoinColumn(name="ORG_ID_D",referencedColumnName="ORG_ID")
	private Organisation organisationDesire;

	public OrganisationCompte() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OrganisationCompte(long compte) {
		super();
		this.compte = compte;
	}

	public OrganisationCompte(long compte, Organisation organisationS,
			Organisation organisationDesire) {
		super();
		this.compte = compte;
		this.organisationS = organisationS;
		this.organisationDesire = organisationDesire;
	}

	public long getOrgCompteId() {
		return orgCompteId;
	}

	public void setOrgCompteId(long orgCompteId) {
		this.orgCompteId = orgCompteId;
	}

	public long getCompte() {
		return compte;
	}

	public void setCompte(long compte) {
		this.compte = compte;
	}

	public Organisation getOrganisationS() {
		return organisationS;
	}

	public void setOrganisationS(Organisation organisationS) {
		this.organisationS = organisationS;
	}

	public Organisation getOrganisationDesire() {
		return organisationDesire;
	}

	public void setOrganisationDesire(Organisation organisationDesire) {
		this.organisationDesire = organisationDesire;
	}



	
	
}
