package tn.educanet.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;
@Entity(name="ORGANISATION")
public class Organisation implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name="seq", initialValue=1)
	
	@Id
	@GeneratedValue( generator="seq")
	@Column(name = "ORG_ID")
	private long orgId;
	
	
	@Column(name = "NOM")
	@Size(max=255)
	private String nom;
	
	@Column(name = "TEL_MOBILE",unique=true)
	private Integer telmobile;
	
	@Column(name = "FAX",unique=true)
	private Integer fax;
	
	@Column(name = "TEL_FIX",unique=true)
	private Integer telfix;
	
	
	@Column(name = "TYPE")
	@Size(max=255)
	private String type;
	
	@Column(name = "EMAIL",unique=true)
	@Size(max=255)
	private String email;
	
	
	@Column(name = "PASSWORD")
	@Size(max=255)
	private String password;
	

	@Column(name = "num_Com")
	private int numComm;
	
	@ManyToOne(optional=true)
    @JoinColumn(name="ADR_ID",referencedColumnName="ADR_ID")
	private Adresse adresse;
	
	@ManyToOne(optional=true)
    @JoinColumn(name="STOCK_ID",referencedColumnName="STOCK_ID")
	private Stock stock;
	
	@ManyToOne(optional=true)
    @JoinColumn(name="ROLE_ID",referencedColumnName="ROLE_ID")
	private Roles roles;
	
//	@OneToOne(optional=true)
//    @JoinColumn(name="COMPTE_ID",referencedColumnName="COMPTE_ID",nullable=true)
//	private Compte compte;
	
	
	public Organisation(){
		
		this.numComm=0;
	}
	
	public Organisation( String nom, Integer telmobile, Integer fax,
			Integer telfix, String type,String email,
	String password) {
		super();
		
		this.nom = nom;
		this.telmobile = telmobile;
		this.fax = fax;
		this.telfix = telfix;
		this.type=type;
		this.email=email;
		this.password=password;
	
	}

	public long getOrgId() {
		return orgId;
	}

	public void setOrgId(long orgId) {
		this.orgId = orgId;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Integer getTelmobile() {
		return telmobile;
	}

	public void setTelmobile(Integer telmobile) {
		this.telmobile = telmobile;
	}

	public Integer getFax() {
		return fax;
	}

	public void setFax(Integer fax) {
		this.fax = fax;
	}

	public Integer getTelfix() {
		return telfix;
	}

	public void setTelfix(Integer telfix) {
		this.telfix = telfix;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getNumComm() {
		return numComm;
	}

	public void setNumComm(int numComm) {
		this.numComm = numComm;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public Roles getRoles() {
		return roles;
	}

	public void setRoles(Roles roles) {
		this.roles = roles;
	}

//	public Compte getCompte() {
//		return compte;
//	}
//
//	public void setCompte(Compte compte) {
//		this.compte = compte;
//	}
//	
	



}
