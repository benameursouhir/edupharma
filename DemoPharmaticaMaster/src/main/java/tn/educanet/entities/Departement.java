package tn.educanet.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Cascade;


import com.sun.org.apache.bcel.internal.generic.Type;

@Entity(name="DEPARTEMENT")
@Table(uniqueConstraints=@UniqueConstraint(columnNames={"ORG_ID","NOM_DEP"}))
public class Departement implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name="seq", initialValue=1)
	@Id
	@Column(name = "DEP_ID", nullable = false)
	@GeneratedValue( generator="seq")
	private long depId;
	
	@Column(name="NOM_DEP", nullable = false)
	@Size(max=255)
	private String nomDep;
	
	@ManyToOne(optional=true)
    @JoinColumn(name="ORG_ID",referencedColumnName="ORG_ID")
	private Organisation organisation;
	
	public Departement() {
		// TODO Auto-generated constructor stub
	}
	
	public Departement(String nomDep) {
		super();
		this.nomDep = nomDep;
	}

	public long getDepId() {
		return depId;
	}

	public void setDepId(long depId) {
		this.depId = depId;
	}

	public String getNomDep() {
		return nomDep;
	}

	public void setNomDep(String nomDep) {
		this.nomDep = nomDep;
	}
	
	
  public Organisation getOrganisation() {
		return organisation;
	}

	public void setOrganisation(Organisation organisation) {
		this.organisation = organisation;
	}

@Override
	public String toString()
  {
	  return nomDep;
  }

}
