package tn.educanet.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;


@Entity(name="ROLES")
public class Roles implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	@SequenceGenerator(name="seq", initialValue=1)
	@Id
	@Column(name = "ROLE_ID", nullable = false)
	@GeneratedValue( generator="seq")
	private long roleId;
	
	@Column(name="NOM_ROLE", unique=true)
	@Size(max=255)
	private String nomRole;

	public Roles() {
		// TODO Auto-generated constructor stub
	}

	public Roles(String nomRole) {
		super();
		this.nomRole = nomRole;
	}

	public long getRoleId() {
		return roleId;
	}

	public void setRoleId(long roleId) {
		this.roleId = roleId;
	}

	public String getNomRole() {
		return nomRole;
	}

	public void setNomRole(String nomRole) {
		this.nomRole = nomRole;
	}

}
