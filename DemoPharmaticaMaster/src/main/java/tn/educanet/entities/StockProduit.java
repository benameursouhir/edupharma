package tn.educanet.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;

@Entity(name="STOCK_PRODUIT")
public class StockProduit implements Serializable {
	
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name="seq", initialValue=1)
	@Id
	@Column(name = "STOCK_PROD_ID", nullable = false)
	@GeneratedValue( generator="seq")
	private long stockProdId;
	
	@Column(name="QTE_STOCK")
	 private Integer qteStock;
	
//	@Column(name="SEUIL")
//	 private Integer seuil;


	
	@ManyToOne(optional=true)
    @JoinColumn(name="STOCK_ID",referencedColumnName="STOCK_ID",nullable=true)
	private Stock stock;
	
	@ManyToOne(optional=true)
    @JoinColumn(name="PROD_ID",referencedColumnName="PROD_ID",nullable=true)
	private Produit produit;

	public StockProduit() {
		super();
		// TODO Auto-generated constructor stub
	}

	public StockProduit(Stock stock, Produit produit,Integer qteStock) {
		super();
	
		this.stock = stock;
		this.produit = produit;
		this.qteStock=qteStock;
	}

	public long getStockProdId() {
		return stockProdId;
	}

	public void setStockProdId(long stockProdId) {
		this.stockProdId = stockProdId;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public Produit getProduit() {
		return produit;
	}

	public void setProduit(Produit produit) {
		this.produit = produit;
	}

	public Integer getQteStock() {
		return qteStock;
	}



	public void setQteStock(Integer qteStock) {
		this.qteStock = qteStock;
	}

	
}
