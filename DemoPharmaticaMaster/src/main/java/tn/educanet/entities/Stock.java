package tn.educanet.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.Size;


@Entity(name="STOCK")
public class Stock implements Serializable{
	
	private static final long serialVersionUID = 1L;
	@SequenceGenerator(name="seq", initialValue=1)
	@Id
	@Column(name = "STOCK_ID", nullable = false)
	@GeneratedValue( generator="seq")
	private long stockId;
	
	@Column(name = "NOM_STOCK",unique=true)
	@Size(max=255)
	private String nomStock;

	public Stock() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Stock( String nomStock) {
		super();
		
		this.nomStock = nomStock;
	}

	public long getStockId() {
		return stockId;
	}

	public void setStockId(long stockId) {
		this.stockId = stockId;
	}

	public String getNomStock() {
		return nomStock;
	}

	public void setNomStock(String nomStock) {
		this.nomStock = nomStock;
	}
	
	

}
