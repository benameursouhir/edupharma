package tn.educanet.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.dao.ClasseRepository;
import tn.educanet.entities.Classe;
import tn.educanet.service.ClasseService;


@Service
public class ClasseServiceImpl implements ClasseService	{
	
	@Autowired
	ClasseRepository repository;

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public Classe save(Classe a) {
		// TODO Auto-generated method stub
		return repository.save(a);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void update(Long classeId,String nomClasse) {
		Classe c=find(classeId);
		c.setNomClasse(nomClasse);
		repository.saveAndFlush(c);
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void delete(Long id) {
		Classe c=find(id);
		repository.delete(c);
		
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public Classe find(Long id) {
		Classe cmd=repository.findOne(id);
		if(cmd==null){
			throw new IllegalArgumentException("classe avec"+id+" n'existe pas");
		}
		return repository.findOne(id);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public Page<Classe> findPerPage(int s,int i) {
		// TODO Auto-generated method stub
		return repository.findAll(new PageRequest(i, s, Direction.ASC, "nomClasse"));
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public Classe findByNomClasse(String nomClasse) {
		// TODO Auto-generated method stub
		return repository.findByNomClasse(nomClasse);
	}

	@Override
	public List<Classe> findAllClasse() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

}
