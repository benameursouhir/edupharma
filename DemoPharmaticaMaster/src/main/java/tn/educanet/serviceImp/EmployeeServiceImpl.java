package tn.educanet.serviceImp;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.dao.AdresseRepository;
import tn.educanet.dao.DepartementRepository;
import tn.educanet.dao.EmployeeRepository;
import tn.educanet.entities.Adresse;
import tn.educanet.entities.Departement;
import tn.educanet.entities.Employee;
import tn.educanet.entities.Organisation;
import tn.educanet.service.AdresseService;
import tn.educanet.service.DepartementService;
import tn.educanet.service.EmployeeService;
import tn.educanet.service.OrganisationService;


@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {
	
	
	 @Autowired
	 private EmployeeRepository employeeRepository;
	 @Autowired
	 AdresseService adresseService;
	 @Autowired
	 OrganisationService organisationService;
	 @Autowired
	 DepartementService departementService;
	


	 @Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void save(String email,Employee e,String ville,String nomRue,Integer codePostal) {
		
		 String message="Verifier les informations suivantes:";
		 Employee ecin=employeeRepository.findByCin(e.getCin());
		 Employee etel=employeeRepository.findByTel(e.getTel());
		 Organisation o=organisationService.findByEmail(email);
		 if(ecin != null) message+="cin,";
		 if(etel!= null) message+="telephone,";
		if(message.equals("Verifier les informations suivantes:")) {
		 Adresse adr=adresseService.findByVilleAndNomRueAndCodePostal(ville, nomRue,codePostal);
		 if(adr==null){
			 Adresse ad=new Adresse(nomRue, codePostal, ville);
			adresseService.save(ad);
			 e.setAdresse(ad);
		 }
		 else e.setAdresse(adr);
		 
		 e.setDepartement(departementService.findByOrganisation_NomAndNomDep(o.getNom(),e.getDepartement().getNomDep()) );
		 e.setOrganisation(o);
		 
		employeeRepository.save(e) ;
	
		}
		else throw new IllegalArgumentException(message);
		
	}

	 @Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void update(Long id,String email,Employee e,String ville,String nomRue,Integer codePostal) {
		Employee emp=find(id);
		 Organisation o=organisationService.findByEmail(email);
		String message="Verifier les informations suivantes:";
		Employee  ecin=employeeRepository.findByCin(e.getCin());
		Employee eTel=employeeRepository.findByTel(e.getTel());
		
		 if( ecin != null && ecin.getEmpId()!= id) message+="CIN,";
		 if( eTel != null && eTel.getEmpId()!= id) message+="telephone,";
			if(message.equals("Verifier les informations suivantes:")){
				
				 Adresse adr=adresseService.findByVilleAndNomRueAndCodePostal(ville, nomRue,codePostal);
				 if(adr==null){
					 Adresse ad=new Adresse(nomRue, codePostal, ville);
					adresseService.save(ad);
					 emp.setAdresse(ad);
				 }
				 else e.setAdresse(adr);	
				 emp.setDepartement(departementService.findByOrganisation_NomAndNomDep(o.getNom(),e.getDepartement().getNomDep()) );
				 emp.setCin(e.getCin());
				 emp.setDatedebut(e.getDatedebut());
				 emp.setDatenaissance(e.getDatenaissance());
				 emp.setGrade(e.getGrade());
				 emp.setNom(e.getNom());
				 emp.setOrganisation(o);
				 emp.setPrenom(e.getPrenom());
				 emp.setSalaire(e.getSalaire());
				 emp.setTel(e.getTel());
					
					
				 employeeRepository.saveAndFlush(emp);
			}
			else throw new IllegalArgumentException(message);
		
	}

	 @Override
		@Transactional(propagation=Propagation.REQUIRED)
	public void delete(Long id) {
		Employee e=find(id);
		employeeRepository.delete(e);
	}

	 @Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Employee find(Long id) {
		 Employee cmd = employeeRepository.findOne(id);
			if(cmd==null){
				throw new IllegalArgumentException("employ� avec"+id+" n'existe pas");
			}
		return employeeRepository.findOne(id);
	}

	 @Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Employee> getAllEmployee() {
		// TODO Auto-generated method stub
		return employeeRepository.findAll();
	}
	
	 @Override
		@Transactional(propagation=Propagation.SUPPORTS)
	public List<Employee> findByGrade(String grade) {
		
		return employeeRepository.findByGrade(grade);
	}
    
	 @Override
		@Transactional(propagation=Propagation.SUPPORTS)
	public List<Employee> findByNomAndPrenom(String nom,String prenom){
		
		return employeeRepository.findByNomAndPrenom(nom, prenom);
	}
	
	 @Override
		@Transactional(propagation=Propagation.SUPPORTS)
	public Employee findByCin(int cin){
		
		return employeeRepository.findByCin(cin);
	}
	 @Override
		@Transactional(propagation=Propagation.SUPPORTS)
	public Employee findByTel(Integer tel){
	
		return employeeRepository.findByTel(tel);
	}
	 @Override
		@Transactional(propagation=Propagation.SUPPORTS)
	public List<Employee> findByOrganisation(Organisation o) {
		// TODO Auto-generated method stub
		return employeeRepository.findByOrganisation(o);
	}

	
}
