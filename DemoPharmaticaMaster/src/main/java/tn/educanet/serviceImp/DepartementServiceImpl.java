package tn.educanet.serviceImp;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.dao.DepartementRepository;
import tn.educanet.entities.Departement;
import tn.educanet.entities.Organisation;
import tn.educanet.service.DepartementService;
@Service
@Transactional
public class DepartementServiceImpl implements DepartementService{
	
	protected final Log logger = LogFactory.getLog(getClass());
	
	
	
	@Autowired
	DepartementRepository repository;




	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void save(Departement d) {
		
		 repository.save(d);

	}



	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void update(Departement departement) {
				 try{
					 find(departement.getDepId());
			 departement.setNomDep(departement.getNomDep());
			 repository.saveAndFlush(departement);

		 }
		 catch (Exception e) {
			// TODO: handle exception
			 System.out.println(e.getMessage());
		}

	}



	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void delete(Long id) {
		find(id);
		repository.delete(id);
	}



	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Departement find(Long id) {
		Departement cmd=repository.findOne(id);
		if(cmd==null){
			throw new IllegalArgumentException("Departement avec donn�es suivantes "+id+" n'existe pas");
		}
		return repository.findOne(id);
		
	}



	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Departement> findAllDepartement() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Departement findByNomDep(String nomDep) {
		
		return repository.findByNomDep(nomDep);
		
	}



	public void delete(Departement departement) {
		find(departement.getDepId());
		repository.delete(departement);
	}



	@Override
	public List<Departement> findByOrganisation(Organisation o) {
		// TODO Auto-generated method stub
		return repository.findByOrganisation(o);
	}






	@Override
	public Departement findByOrganisation_NomAndNomDep(String nom, String nomDep) {
	
		return repository.findByOrganisation_NomAndNomDep(nom, nomDep);
	}

}
