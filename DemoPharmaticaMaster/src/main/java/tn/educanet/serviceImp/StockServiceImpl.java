package tn.educanet.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.educanet.dao.StockRepository;
import tn.educanet.entities.Stock;
import tn.educanet.service.StockService;


@Service
public class StockServiceImpl implements StockService{
	
	@Autowired
	StockRepository repository;

	@Override
	public void save(Stock s) {
		// TODO Auto-generated method stub
		repository.save(s);
	}

	@Override
	public void update(Stock s) {
		find(s.getStockId());
		repository.saveAndFlush(s);
	}

	@Override
	public void delete(Long id) {
		find(id);
		repository.delete(id);
	}

	@Override
	public Stock find(Long id) {
		Stock cmd=repository.findOne(id);
		if(cmd==null){
			throw new IllegalArgumentException("Stock avec id="+id+" n'existe pas");
		}
		return repository.findOne(id);
	}

	@Override
	public List<Stock> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Stock findByNomStock(String nomStock) {
		
		return repository.findByNomStock(nomStock);
	}

}
