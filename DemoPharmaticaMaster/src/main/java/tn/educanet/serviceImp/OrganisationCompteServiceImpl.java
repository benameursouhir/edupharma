package tn.educanet.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.educanet.dao.OrganisationCompteRepository;
import tn.educanet.entities.Organisation;
import tn.educanet.entities.OrganisationCompte;
import tn.educanet.service.OrganisationCompteService;

@Service
public class OrganisationCompteServiceImpl implements OrganisationCompteService{
	
	@Autowired
	OrganisationCompteRepository repository;

	@Override
	public void save(OrganisationCompte a) {
		
		repository.save(a);
	}

	@Override
	public void update(OrganisationCompte d) {
		find(d.getOrgCompteId());
		repository.saveAndFlush(d);
	}

	@Override
	public void delete(Long id) {
		find(id);
		repository.delete(id);
	}

	@Override
	public OrganisationCompte find(Long id) {
		OrganisationCompte cmd=repository.findOne(id);
		if(cmd==null){
			throw new IllegalArgumentException("Organisation avec"+id+" n'existe pas");
		}
		return repository.findOne(id);
	}

	@Override
	public List<OrganisationCompte> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public List<OrganisationCompte> findByCompte(long compte) {
		// TODO Auto-generated method stub
		return repository.findByCompte(compte);
	}

	@Override
	public List<OrganisationCompte> findByOrganisationS(
			Organisation organisationS) {
		// TODO Auto-generated method stub
		return repository.findByOrganisationS(organisationS);
	}

	@Override
	public OrganisationCompte findByOrganisationDesireAndOrganisationS(
			Organisation organisationdesire, Organisation organisationS) {
	
		return repository.findByOrganisationDesireAndOrganisationS(organisationdesire, organisationS);
	}
	


	

}
