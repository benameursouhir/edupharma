package tn.educanet.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.controller.MediaController;
import tn.educanet.dao.CommandeRepository;
import tn.educanet.entities.Commande;
import tn.educanet.entities.Organisation;
import tn.educanet.service.CommandeService;
import tn.educanet.service.OrganisationService;
@Service("commandeService")
public class CommandeServiceImpl implements CommandeService{
	
	@Autowired
	CommandeRepository repository;

	@Autowired
	OrganisationService organisationService;
	
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void create(Commande c) {
		
	 repository.save(c);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void update(Commande c) {
		
		 repository.saveAndFlush(c);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void delete(long id) {
		Commande c = find(id);
		
		repository.delete(c);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Commande find(long id) {
	Commande cmd = repository.findOne(id);
	if(cmd==null){
		throw new IllegalArgumentException("commande avec"+id+" n'existe pas");
	}
		return repository.findOne(id);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> getAllCommande() {
		return repository.findAll();
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationEnvoiOrderByEtatAsc(Organisation orgE) {
			return repository.findByOrganisationEnvoiOrderByEtatAsc(orgE);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationOrderByEtatAsc(Organisation orgR) {
		// TODO Auto-generated method stub
		return repository.findByOrganisationOrderByEtatAsc(orgR);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationEnvoiAndEtatTrue(Organisation orgE) {
		// TODO Auto-generated method stub
		return repository.findByOrganisationEnvoiAndEtatTrue(orgE);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationAndEtatTrue(Organisation orgR) {
		// TODO Auto-generated method stub
		return repository.findByOrganisationAndEtatTrue(orgR);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationEnvoiAndEtatFalse(Organisation orgE) {
		// TODO Auto-generated method stub
		return repository.findByOrganisationEnvoiAndEtatFalse(orgE);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationAndEtatFalse(Organisation orgR) {
		// TODO Auto-generated method stub
		return repository.findByOrganisationAndEtatFalse(orgR);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Commande findByDateComm(String date) {
		
		return repository.findByDateComm(date);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Commande findByBonLivraison(int bonLivraison) {
		
		return repository.findByBonLivraison(bonLivraison);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationAndExisteDTrueOrderByEtatAsc(
			Organisation orgR) {
		// TODO Auto-generated method stub
		return repository.findByOrganisationAndExisteDTrueOrderByEtatAsc(orgR);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationEnvoiAndExisteSTrueOrderByEtatAsc(
			Organisation orgE) {
		// TODO Auto-generated method stub
		return repository.findByOrganisationEnvoiAndExisteSTrueOrderByEtatAsc(orgE);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Commande> findByOrganisationAndExisteDTrue(Organisation orgE, int s,
			int i) {
		return repository.findByOrganisationAndExisteDTrue(orgE, new PageRequest(i, s, Direction.ASC, "etat"));
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Commande> findByOrganisationEnvoiAndExisteSTrue(
			Organisation orgE, int s, int i) {
				return repository.findByOrganisationEnvoiAndExisteSTrue(orgE, new PageRequest(i, s, Direction.ASC, "etat"));
	}
	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Commande> findByOrganisationAndEtatFalse(Organisation orgR,int s,int i) {
		// TODO Auto-generated method stub
		return repository.findByOrganisationAndEtatFalseAndExisteDTrue(orgR,new PageRequest(i, s, Direction.ASC, "etat"));
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisation_NomAndDateCommAndEtat(String nom,String dateComm,boolean etat) {
		String name = MediaController.getUser();
		String nom1=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisation_NomAndExisteDTrueAndOrganisationEnvoi_NomAndDateCommContainingAndEtat(nom1, nom, dateComm,etat);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisation_NomAndDateComm(String nom,
			String dateComm) {
		String name = MediaController.getUser();
		String nom1=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisation_NomAndExisteDTrueAndOrganisationEnvoi_NomAndDateCommContaining(nom1, nom, dateComm);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByNomOrgAndEtat(String nom, boolean etat) {
		String name = MediaController.getUser();
		String nom1=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisation_NomAndExisteDTrueAndOrganisationEnvoi_NomAndEtat(nom1, nom, etat);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByNomOrg(String nom) {
		String name = MediaController.getUser();
		String nom1=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisation_NomAndExisteDTrueAndOrganisationEnvoi_Nom(nom1, nom);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByDateAndEtat(String date, boolean etat) {
		String name = MediaController.getUser();
		String nom=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisation_NomAndExisteDTrueAndDateCommContainingAndEtat(nom, date, etat);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByDate(String date) {
		String name = MediaController.getUser();
		String nom=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisation_NomAndExisteDTrueAndDateCommContaining(nom, date);
	}

	
	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationEnvoi_NomAndDateCommAndEtat(String nom,String dateComm,boolean etat){
		String name = MediaController.getUser();
		String nomEnvoi=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisationEnvoi_NomAndExisteSTrueAndOrganisation_NomAndDateCommContainingAndEtat(nomEnvoi, nom, dateComm, etat);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationEnvoi_NomAndDateComm(String nom,
			String dateComm) {
		String name = MediaController.getUser();
		String nomEnvoi=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisationEnvoi_NomAndExisteSTrueAndOrganisation_NomAndDateCommContaining(nomEnvoi, nom, dateComm);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationEnvoi_NomAndEtat(String nom,
			boolean etat) {
		String name = MediaController.getUser();
		String nomEnvoi=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisationEnvoi_NomAndExisteSTrueAndOrganisation_NomAndEtat(nomEnvoi, nom, etat);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findByOrganisationEnvoi_Nom(String nom) {
		String name = MediaController.getUser();
		String nomEnvoi=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisationEnvoi_NomAndExisteSTrueAndOrganisation_Nom(nomEnvoi, nom);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Commande> findEnvoiByDateAndEtat(String date, boolean etat) {
		String name = MediaController.getUser();
		String nomEnvoi=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisationEnvoi_NomAndExisteSTrueAndDateCommContainingAndEtat(nomEnvoi, date, etat);
	}

	@Override
	public List<Commande> findEnvoiByDate(String date) {
		String name = MediaController.getUser();
		String nomEnvoi=organisationService.findByEmail(name).getNom();
		return repository.findByOrganisationEnvoi_NomAndExisteSTrueAndDateCommContaining(nomEnvoi, date);
	}


}
