package tn.educanet.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.dao.AdresseRepository;
import tn.educanet.dao.OrganisationRepository;
import tn.educanet.dao.ProduitRepository;
import tn.educanet.dao.StockRepository;
import tn.educanet.entities.Adresse;
import tn.educanet.entities.Organisation;
import tn.educanet.entities.Produit;
import tn.educanet.entities.Roles;
import tn.educanet.entities.Stock;
import tn.educanet.entities.StockProduit;
import tn.educanet.service.AdresseService;
import tn.educanet.service.OrganisationService;
import tn.educanet.service.ProduitService;
import tn.educanet.service.RoleService;
import tn.educanet.service.StockProduitService;


@Service("organisationService")

public class OrganisationServiceImpl implements OrganisationService{
	
	@Autowired
	OrganisationRepository repository;
	@Autowired
	StockRepository stockRepository;
	@Autowired
	ProduitService produitRepository;
	@Autowired
	RoleService roleService;
	@Autowired
	AdresseService adresseService;
	@Autowired
	StockProduitService stockProduitService;
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void save(Organisation organisation,String rue,String ville,Integer codePostal,String type) {
	
		 Organisation org1=repository.findBytelfix(organisation.getTelfix());
		 Organisation org2=repository.findByfax(organisation.getFax());
		 Organisation org3=repository.findBytelmobile(organisation.getTelmobile());
		 Organisation org4=repository.findByEmail(organisation.getEmail());
		 String message="Verifier les informations suivantes:";
		 
if(type.equals("admin")){ repository.save(organisation);}
else{
 		 if(org1 != null) message+="telephone fix,";
	 if(org2!= null) message+="fax,";
	 if(org3!= null) message+="tel mobile,";
	 if(org4!= null) message+="email,";
	 
	 Adresse a=new Adresse(rue, codePostal, ville);
		Adresse adr=adresseService.findByVilleAndNomRueAndCodePostal(a.getVille(), a.getNomRue(), a.getCodePostal());
	    	if(adr!=null){
	    		message+="adresse";
	    		}
	    	
			  	
	 	//ajout de tous les produits au stock de l'organisation avec qte 0 
	 	

		if(message.equals("Verifier les informations suivantes:")){
			
			
			adresseService.save(a);
			 organisation.setAdresse(a);
			 organisation.setNumComm(0);
				organisation.setType(type);
				
				Stock s=new Stock(organisation.getEmail());
				stockRepository.save(s);
			 	organisation.setStock(s);
			 	Roles r=roleService.findByNomRole("ROLE_USER");
			 	java.util.List<Produit> p=produitRepository.getAllProduits();
			 	System.out.println("list"+p.size());
			 	if(!p.isEmpty()){
			 	
			 	for(int i=0;i<p.size();i++){
			 		StockProduit sp=new StockProduit(s, p.get(i),0);
			 		stockProduitService.save(sp);
			 		 	}}
			 	organisation.setRoles(r);
			
		repository.save(organisation);
		message="Ajout r�ussis";
		
		}
		else throw new IllegalArgumentException(message);
}
	}
	

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void update(Long id,Organisation org,String rue,String ville,Integer codePostal,String type) {
		System.out.println("adresse organisation");
		Organisation o=find(id);
		 Integer fix=org.getTelfix();
		
         Integer mob=org.getTelmobile();
         Integer fax=org.getFax();
       String mail=org.getEmail();
       
  	 String message="Verifier les informations suivantes:";
  	 Organisation telfix=repository.findBytelfix(fix);
  	 Organisation telmob=repository.findBytelmobile(mob);
  	 Organisation telfax=repository.findByfax(fax);
  	Organisation email=repository.findByEmail(mail);
  	 
  	 
  	 if( telfix != null && telfix.getOrgId()!= id) message+="telephone fix,";
   	 if(telmob != null && telmob.getOrgId() != id) message+="telephone mobile,";
  	 if(telfax != null && telfax.getOrgId() != id) message+="fax,";
  	 if(email != null && email.getOrgId() != id) message+="adresse email,";
 
  	
	
		Organisation adresse=repository.findByAdresse_VilleAndAdresse_NomRueAndAdresse_CodePostal(ville, rue, codePostal);
		System.out.println("fin verif org adresse1");
	    	if(adresse != null && adresse.getOrgId() != id){
	    		message+="adresse";
	    		}
	    	
	    	if(message.equals("Verifier les informations suivantes:")){
	    		System.out.println("id org � modifier"+id);
	    		
	    		Stock s=stockRepository.findByNomStock(o.getEmail());
	    		System.out.println("stock"+s.getNomStock());
	    		s.setNomStock(mail);
	    		stockRepository.saveAndFlush(s);
	    		System.out.println("fin update stock");
	    		Adresse adr=adresseService.findByVilleAndNomRueAndCodePostal(o.getAdresse().getVille(), o.getAdresse().getNomRue(),o.getAdresse().getCodePostal());
	    		adr.setVille(ville);
	    		adr.setCodePostal(codePostal);
	    		adr.setNomRue(rue);
	    		adresseService.update(adr.getAdrId(),adr);
	    		System.out.println("fin update adresse");
	    		
	    		org.setType(type);
	    		
	    		org.setAdresse(adr);
	    		org.setRoles(roleService.findByNomRole("ROLE_USER"));
	    		org.setStock(s);
		 repository.saveAndFlush(org)	;
		 
	    	}
	    	else throw new IllegalArgumentException(message);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void delete(long id) {
		Organisation o=find(id);
		repository.delete(o);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Organisation find(long id) {
		Organisation cmd = repository.findOne(id);
		if(cmd==null){
			throw new IllegalArgumentException("organisation avec"+id+" n'existe pas");
		}
		return repository.findOne(id);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Organisation> getAllOrganisation() {
		
		return repository.findAll();
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Organisation> findByNom(String nom) {
		
		return repository.findByNom(nom);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Organisation findByNomAndAdresse(String nom, Adresse adr) {
		
		return repository.findByNomAndAdresse(nom, adr);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Organisation findByEmail(String email) {
		
		return repository.findByEmail(email);
	}

	
	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Organisation findBytelfix(Integer telfix) {
		
		return repository.findBytelfix(telfix);
	}
	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Organisation findByfax(Integer fax) {
		
		return repository.findByfax( fax);
	}
	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Organisation findBytelmobile(Integer telmobile) {
		
		return repository.findBytelmobile(telmobile);
	}
	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Organisation findByAdresse(Adresse adr) {
		
		return repository.findByAdresse(adr);
	}
	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	 public List<Organisation> findByType(String type) {

	  return repository.findByType(type);
	 }

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Organisation> findByTypeAndNom(String type, String nom) {
	
		return repository.findByTypeAndNom(type, nom);
	}
	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	 public List<Organisation> findByTypeNotLike(String type){
	 return repository.findByTypeNotLike(type);}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Organisation findByAdresse_VilleAndAdresse_NomRueAndAdresse_CodePostal(
			String ville, String nomRue, Integer codePostal) {
	
		return repository.findByAdresse_VilleAndAdresse_NomRueAndAdresse_CodePostal(ville, nomRue, codePostal);
	}


	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public void updateNumCommande(Organisation o, int numCommande) {
		o.setNumComm(numCommande);
		repository.saveAndFlush(o);
		
	}


	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Organisation> findByTypeNotLike(String type, int s, int i) {
		// TODO Auto-generated method stub
		return repository.findByTypeNotLike(type, new PageRequest(i, s, Direction.ASC, "type"));
	}


	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Organisation> findByType(String type,int s,int i) {
		// TODO Auto-generated method stub
		return repository.findByTypeNotLikeAndType("admin",type,  new PageRequest(i, s, Direction.ASC, "type"));
	}


	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Organisation> findByTypeAndAdresse_VilleAndAdresse_NomRueAndNom(
		String type, String ville, String nomRue, String nom,
			int s, int i) {
			return repository.findByTypeAndAdresse_VilleAndAdresse_NomRueAndNom( type, ville, nomRue, nom, new PageRequest(i, s, Direction.ASC, "nom"));
		
	}


	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Organisation> findByTypeAndAdresse_VilleAndAdresse_NomRue(
			String type, String ville, String nomRue, int s, int i) {
		
		return repository.findByTypeAndAdresse_VilleAndAdresse_NomRue(type, ville, nomRue,  new PageRequest(i, s, Direction.ASC, "nom"));
	}


	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Organisation> findByNomAndAdresse_VilleAndAdresse_NomRue(
			String nom, String ville, String nomRue, int s, int i) {
		return repository.findByNomAndAdresse_VilleAndAdresse_NomRue(nom, ville, nomRue, new PageRequest(i, s, Direction.ASC, "nom"));
	}


	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Organisation> findByTypeAndAdresse_Ville(String type,
			String ville, int s, int i) {
		return repository.findByTypeAndAdresse_Ville(type, ville, new PageRequest(i, s, Direction.ASC, "nom"));
	}


	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Organisation> findByNomAndAdresse_Ville(String nom,
			String ville, int s, int i) {
		return repository.findByNomAndAdresse_Ville(nom, ville, new PageRequest(i, s, Direction.ASC, "nom"));
	}


	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Organisation> findByNom(String nom, int s, int i) {
		return repository.findByNom(nom,  new PageRequest(i, s, Direction.ASC, "nom"));
	}


	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Page<Organisation> findByAdresse_Ville(String ville, int s, int i) {
			return repository.findByAdresse_Ville(ville, new PageRequest(i, s, Direction.ASC, "nom"));
	}
}
