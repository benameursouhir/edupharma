package tn.educanet.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.dao.ClasseRepository;
import tn.educanet.dao.OrganisationRepository;
import tn.educanet.dao.ProduitRepository;
import tn.educanet.dao.StockProduitRepository;
import tn.educanet.dao.StockRepository;
import tn.educanet.entities.Classe;
import tn.educanet.entities.Organisation;
import tn.educanet.entities.Produit;
import tn.educanet.entities.StockProduit;
import tn.educanet.service.OrganisationService;
import tn.educanet.service.ProduitService;
import tn.educanet.service.StockProduitService;
import tn.educanet.service.StockService;


@Service
@Transactional
public class ProduitServiceImpl implements ProduitService{

	@Autowired
	ProduitRepository repository;
	@Autowired
	ClasseRepository classeRepository;
	@Autowired
	StockProduitService stockProduitRepository;
	@Autowired
	OrganisationService organisationRepository;
	@Autowired
	StockService stockRepository;
	
	
	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void create(Produit p,String email,String tab,String classe,String classe1) {
		String message="Verifier les informations suivantes:";
		Produit plib=repository.findByLibelle(p.getLibelle());
		Organisation o=organisationRepository.findByEmail(email);
		if(plib!= null) message+="libelle,";
		Produit pref=repository.findByRefP(p.getRefP());
		if(pref!= null) message +="reference,";
		Produit pcode=repository.findByCodeBarre(p.getCodeBarre());
		if(pcode != null) message+="code � barre,";
		if(message.equals("Verifier les informations suivantes:")) {
		
		
		if(o.getType().equals("admin")){
			
		p.setConfirme(true)	;
		}
		else{ 
			p.setConfirme(false);
		}
		
		 if(classe.equals("")){
			 Classe c=new Classe(classe1);
			Classe cla= classeRepository.save(c);
			 p.setClasse(cla);
		 }
		 else    	p.setClasse(classeRepository.findByNomClasse(classe));
		 p.setValide(true);
		
		 p.setTableau(tab);
		 p.setOrganisation(o);
		 repository.save(p);
		 
		 if(o.getType().equals("admin")){
		 List<Organisation> o1=organisationRepository.findByTypeNotLike("admin");
		 for(int i=0;i<o1.size();i++){
		 		StockProduit sp=new StockProduit(stockRepository.findByNomStock(o1.get(i).getEmail()), p,0);
		 		stockProduitRepository.save(sp);
		 		 	}
		 }
		}else throw new IllegalArgumentException(message);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void update(Long id,String refP, String libelle, String forme, String codeBarre,
			Integer qteCarton,  String datePer, String tableau,
			Long prixAchatHt, Long prixVenteHt, Long tauxTva,
			Long prixVenteTtc, Long prixPublic,String classe,String classe1) {
		Produit prod=find(id);
		String message="Verifier les informations suivantes:";
		
		Produit plib=repository.findByLibelle(libelle);
		if(plib!= null && plib.getProdId()!= id) message+="libelle,";
		Produit pref=repository.findByRefP(refP);
		if(pref!= null && pref.getProdId()!= id) message +="reference,";
		Produit pcode=repository.findByCodeBarre(codeBarre);
		if(pcode != null && pcode.getProdId() != id) message+="code � barre,";
		if(message.equals("Verifier les informations suivantes:")){
		
		if(classe.equals("")){
			 Classe c=new Classe(classe1);
			Classe cla= classeRepository.save(c);
			 prod.setClasse(cla);
		 }
		 else    	prod.setClasse(classeRepository.findByNomClasse(classe));
		 
		 prod.setQteCarton(qteCarton);
	    	prod.setRefP(refP);
	    	prod.setLibelle(libelle);
	    	prod.setForme(forme);
	    	prod.setCodeBarre(codeBarre);
	    	prod.setPrixAchatHt(prixAchatHt);
	    	prod.setPrixVenteHt(prixVenteHt);
	    	prod.setTauxTva(tauxTva);
	    	prod.setPrixVenteTtc(prixVenteTtc);
	    	prod.setPrixPublic(prixPublic);
	    	prod.setTableau(tableau);
	    	prod.setDatePer(datePer);
	    	
		repository.saveAndFlush(prod);
		}else throw new IllegalArgumentException(message);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void delete(long id) {
		Produit p=find(id);
		
		List<StockProduit> sp=stockProduitRepository.findByProduitAndQteStockGreaterThan(p, 0);
		
		if(sp.isEmpty()){
			List<StockProduit> sp1=stockProduitRepository.findByProduit(p);
			if(!sp1.isEmpty()){
			for(int i=0;i<sp1.size();i++)
				stockProduitRepository.delete(sp1.get(i).getStockProdId());
								}
						}
			

		p.setValide(false);
		repository.saveAndFlush(p);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Produit find(long id) {
		Produit cmd = repository.findOne(id);
		if(cmd==null){
			throw new IllegalArgumentException("commande avec id="+id+" n'existe pas");
		}
		return repository.findOne(id);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Produit> getAllProduits() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Produit findByRefP(String refP) {
	
		return repository.findByRefP(refP);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public Produit findByLibelle(String libelle) {
		
		return repository.findByLibelle(libelle);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Produit> findByDatePerAfter(String date) {
		// TODO Auto-generated method stub
		return repository.findByDatePerAfter(date);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public Page<Produit> findPerPage(int s,int i) {
	
		//return repository.findAll(new PageRequest(0, 10, Direction.DESC, "publishedData"));
		return repository.findAll(new PageRequest(i, s, Direction.DESC, "confirme"));
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public Page<Produit> findByValide(Boolean valide, int s, int i) {
		// TODO Auto-generated method stub
		return repository.findByValide(valide,new PageRequest(i, s, Direction.ASC, "confirme"));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void confirm(long id) {
		Produit p=repository.findOne(id);
		p.setConfirme(true);
		
		repository.saveAndFlush(p);
		List<Organisation> o=organisationRepository.findByTypeNotLike("admin");
		 for(int i=0;i<o.size();i++){
		 		StockProduit sp=new StockProduit(stockRepository.findByNomStock(o.get(i).getEmail()), p,0);
		 		stockProduitRepository.save(sp);
		 		 	}
		
		
		
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public Produit findByCodeBarre(String codeBarre) {
		
		return repository.findByCodeBarre(codeBarre);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public Page<Produit> findByOrganisation_Nom(String nomOrg, int s, int i) {
		// TODO Auto-generated method stub
		return repository.findByOrganisation_Nom(nomOrg, new PageRequest(i, s, Direction.ASC, "confirme"));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void deleteDef(long id) {
		find(id);
		repository.delete(id);
		
	}
	
}
