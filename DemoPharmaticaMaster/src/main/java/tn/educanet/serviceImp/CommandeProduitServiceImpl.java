package tn.educanet.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.dao.CommandeProduitRepository;
import tn.educanet.entities.Commande;
import tn.educanet.entities.CommandeProduit;
import tn.educanet.service.CommandeProduitService;


@Service("commandeproduitService")
public class CommandeProduitServiceImpl implements CommandeProduitService{
	
	@Autowired
	CommandeProduitRepository repository;

	

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void delete(long id) {
		find(id);
		repository.delete(id);
		
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public CommandeProduit find(long id) {
		CommandeProduit cmd=repository.findOne(id);
		if(cmd==null){
			throw new IllegalArgumentException("Commande Produit avec"+id+" n'existe pas");
		}
		return repository.findOne(id);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<CommandeProduit> getAllCommandeProduit() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void create(CommandeProduit c) {
		// TODO Auto-generated method stub
		repository.save(c);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void update(Long id,CommandeProduit c) {
		
		find(id);
		repository.saveAndFlush(c);
	}

	@Override
	@Transactional(propagation=Propagation.SUPPORTS)
	public List<CommandeProduit> findByCommande(Commande c) {
		// TODO Auto-generated method stub
		return repository.findByCommande(c);
	}

}
