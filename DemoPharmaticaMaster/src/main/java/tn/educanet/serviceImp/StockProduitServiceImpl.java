package tn.educanet.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import tn.educanet.dao.StockProduitRepository;
import tn.educanet.entities.Produit;
import tn.educanet.entities.Stock;
import tn.educanet.entities.StockProduit;
import tn.educanet.service.StockProduitService;

@Service
public class StockProduitServiceImpl implements StockProduitService{
	
	@Autowired
	StockProduitRepository repository;

	@Override
	public void save(StockProduit sp) {
		// TODO Auto-generated method stub
		repository.save(sp);
		
	}

	@Override
	public void update(StockProduit sp) {
		find(sp.getStockProdId());
		repository.saveAndFlush(sp);
	}

	@Override
	public void delete(Long id) {
		find(id);
		repository.delete(id);
		
	}

	@Override
	public StockProduit find(Long id) {
		StockProduit cmd=repository.findOne(id);
		if(cmd==null){
			throw new IllegalArgumentException("produit avec id="+id+" n'existe pas");
		}
		return repository.findOne(id);
	}

	@Override
	public List<StockProduit> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

	@Override
	public Page<StockProduit> findByStock_NomStockAndProduit_ValideAndProduit_Confirme(int s,int i,String nomStock,Boolean valide,Boolean confirme){
		// TODO Auto-generated method stub
		return repository.findByStock_NomStockAndProduit_ValideAndProduit_Confirme(nomStock,valide,confirme,new PageRequest(i, s, Direction.ASC, "qteStock"));
	}

	@Override
	public StockProduit findByStockAndProduit(Stock s, Produit p) {
		
		return repository.findByStockAndProduit(s, p);
	}



	@Override
	public List<StockProduit> findByStockAndQteStockLessThan(Stock s,
			Integer seuil) {
		// TODO Auto-generated method stub
		return repository.findByStockAndQteStockLessThan(s, seuil);
	}

	@Override
	public List<StockProduit> findByStockAndProduit_DatePerAfter(Stock s, String date) {
		// TODO Auto-generated method stub
		return repository.findByStockAndProduit_DatePerAfter(s, date);
	}

	@Override
	public List<StockProduit> findByStockAndQteStock(Stock s, Integer qteStock) {
		// TODO Auto-generated method stub
		return repository.findByStockAndQteStock(s, qteStock);
	}

	@Override
	public List<StockProduit> findByProduitAndQteStockGreaterThan(Produit p,
			Integer qteStock) {
		// TODO Auto-generated method stub
		return repository.findByProduitAndQteStockGreaterThan(p, qteStock);
	}

	@Override
	public List<StockProduit> findByProduit(Produit p) {
		// TODO Auto-generated method stub
		return repository.findByProduit(p);
	}

	@Override
	public List<StockProduit> findByStock_NomStock(String nomStock) {
		// TODO Auto-generated method stub
		return repository.findByStock_NomStock(nomStock);
	}

	@Override
	public Page<StockProduit> findByStock_NomStockAndProduit_RefPContaining(
			String nomStock, String refP,int s,int i) {
		// TODO Auto-generated method stub
		return repository.findByStock_NomStockAndProduit_RefPContaining(nomStock, refP,new PageRequest(i, s, Direction.ASC, "qteStock"));
	}

	@Override
	public Page<StockProduit> findByStock_NomStockAndProduit_LibelleContaining(
			String nomStock, String libelle,int s,int i) {
		// TODO Auto-generated method stub
		return repository.findByStock_NomStockAndProduit_LibelleContaining(nomStock, libelle,new PageRequest(i, s, Direction.ASC, "qteStock"));
	}


	@Override
	public Page<StockProduit> findByStock_NomStockAndProduit_Classe_NomClasse(
			String nomStock, String nomClasse, int s, int i) {
		// TODO Auto-generated method stub
		return repository.findByStock_NomStockAndProduit_Classe_NomClasse(nomStock, nomClasse, new PageRequest(i, s, Direction.ASC, "qteStock"));
	}

	@Override
	public List<StockProduit> findByStock_NomStockAndProduit_RefPContaining(
			String nomStock, String refP) {
		// TODO Auto-generated method stub
		return repository.findByStock_NomStockAndProduit_RefPContaining(nomStock, refP);
	}

	@Override
	public List<StockProduit> findByStock_NomStockAndProduit_LibelleContaining(
			String nomStock, String libelle) {
		// TODO Auto-generated method stub
		return repository.findByStock_NomStockAndProduit_LibelleContaining(nomStock, libelle);
	}

	@Override
	public List<StockProduit> findByStock_NomStockAndProduit_Classe_NomClasse(
			String nomStock, String nomClasse) {
		// TODO Auto-generated method stub
		return repository.findByStock_NomStockAndProduit_Classe_NomClasse(nomStock, nomClasse);
	}

	




}
