package tn.educanet.serviceImp;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import tn.educanet.dao.RolesRepository;
import tn.educanet.entities.Roles;
import tn.educanet.service.RoleService;


@Service
public class RolesServiceImpl implements RoleService{
	
	
	@Autowired
	RolesRepository repository;

//	@Autowired
//	private SessionFactory sessionFactory;
//	
//	private Session getCurrentSession() {
//		return sessionFactory.getCurrentSession();
//	}
	
	
	public Roles findByNomRole(String nomRole) {
		// TODO Auto-generated method stub
		return repository.findByNomRole(nomRole);
		//(Roles) getCurrentSession().load(Roles.class, nomRole)
	}

	public void create(Roles p) {
		// TODO Auto-generated method stub
		repository.save(p);
		
	}

	public void update(Roles p) {
		// TODO Auto-generated method stub
		repository.saveAndFlush(p);
	}

	public void delete(long id) {
		// TODO Auto-generated method stub
		repository.delete(id);
	}

	public Roles find(long id) {
		// TODO Auto-generated method stub
		return repository.findOne(id);
	}

	public List<Roles> getAllRoles() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

}
