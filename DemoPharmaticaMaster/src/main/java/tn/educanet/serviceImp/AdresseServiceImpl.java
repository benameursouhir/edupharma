package tn.educanet.serviceImp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.dao.AdresseRepository;
import tn.educanet.entities.Adresse;
import tn.educanet.service.AdresseService;

@Service
public class AdresseServiceImpl implements AdresseService{
	
	@Autowired 
	AdresseRepository repository;



 	@Override
 	@Transactional(propagation=Propagation.REQUIRED)
	public void delete(Long id) {
	find(id);
		repository.delete(id);
	}

 	@Override
 	@Transactional(propagation=Propagation.SUPPORTS)
	public Adresse find(Long id) {
 		Adresse cmd=repository.findOne(id);
		if(cmd==null){
			throw new IllegalArgumentException("Adresse avec id="+id+" n'existe pas");
		}
		return repository.findOne(id);
	}

 	@Override
 	@Transactional(propagation=Propagation.SUPPORTS)
	public List<Adresse> findAll() {
		// TODO Auto-generated method stub
		return repository.findAll();
	}

 	@Override
 	@Transactional(propagation=Propagation.REQUIRED)
	public void save(Adresse a) {
		// TODO Auto-generated method stub
	 repository.save(a);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED)
	public void update(Long id,Adresse adresse) {
		find(id);		
		adresse.setCodePostal(adresse.getCodePostal());
    	adresse.setNomRue(adresse.getNomRue());
    	adresse.setVille(adresse.getVille());
		repository.saveAndFlush(adresse);
	}

 	@Override
 	@Transactional(propagation=Propagation.SUPPORTS)
	public Adresse findByVilleAndNomRueAndCodePostal(String ville,
			String nomRue, int codePostal) {
		
		return repository.findByVilleAndNomRueAndCodePostal(ville, nomRue, codePostal);
	}

}
