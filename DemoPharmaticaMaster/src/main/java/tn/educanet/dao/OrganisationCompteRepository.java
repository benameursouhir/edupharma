package tn.educanet.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.entities.Organisation;
import tn.educanet.entities.OrganisationCompte;


@Transactional(readOnly=true)
public interface OrganisationCompteRepository extends JpaRepository<OrganisationCompte,Long>, CrudRepository<OrganisationCompte, Long> {
	
	
	List<OrganisationCompte> findByCompte(long compte);
	List<OrganisationCompte> findByOrganisationS(Organisation organisationS);
	OrganisationCompte findByOrganisationDesireAndOrganisationS(Organisation organisationdesire,Organisation organisationS);
	
	

}
