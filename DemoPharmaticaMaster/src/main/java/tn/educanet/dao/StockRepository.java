package tn.educanet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.entities.Stock;


@Transactional(readOnly=true)
public interface StockRepository  extends JpaRepository<Stock, Long>,CrudRepository<Stock, Long>{
	
	Stock findByNomStock(String nomStock);

}
