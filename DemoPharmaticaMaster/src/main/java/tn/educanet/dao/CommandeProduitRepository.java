package tn.educanet.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;


import tn.educanet.entities.Commande;
import tn.educanet.entities.CommandeProduit;
@Transactional(readOnly=true)
public interface CommandeProduitRepository extends JpaRepository<CommandeProduit,Long>, CrudRepository<CommandeProduit, Long>{
	
	List<CommandeProduit> findByCommande(Commande c);

}
