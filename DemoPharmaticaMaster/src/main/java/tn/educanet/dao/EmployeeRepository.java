package tn.educanet.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.entities.Employee;
import tn.educanet.entities.Organisation;


/**
 * Created by suresh on 17/12/13.
 */
@Transactional(readOnly=true)
public interface EmployeeRepository extends JpaRepository<Employee, Long>,CrudRepository<Employee, Long> {
	
	
	List<Employee> findByGrade(String grade);
	List<Employee> findByNomAndPrenom(String nom,String prenom);
	Employee findByCin(int cin);
	Employee findByTel(Integer tel);
	List<Employee> findByOrganisation(Organisation o);
	
	 
	  /* // Example with named params
	   @Modifying
	   @Query("update Employee p set e.fistName = :fistName where e.id = :id")
	   Integer setNewDescriptionForProduct(@Param("id") Integer id,
	      @Param("fistName") String fistName);
	      */
}
