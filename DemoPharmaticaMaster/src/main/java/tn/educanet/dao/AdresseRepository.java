package tn.educanet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.entities.Adresse;


@Transactional(readOnly=true)
public interface AdresseRepository extends JpaRepository<Adresse,Long>, CrudRepository<Adresse, Long>{

	Adresse findByVilleAndNomRueAndCodePostal(String ville,String nomRue,int codePostal);
	
}
