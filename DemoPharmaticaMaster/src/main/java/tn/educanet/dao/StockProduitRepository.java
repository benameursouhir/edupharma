package tn.educanet.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.entities.Produit;
import tn.educanet.entities.Stock;
import tn.educanet.entities.StockProduit;


@Transactional(readOnly=true)
public interface StockProduitRepository extends JpaRepository<StockProduit,Long>, CrudRepository<StockProduit, Long>{
	
	Page<StockProduit> findByStock_NomStockAndProduit_ValideAndProduit_Confirme(String nomStock,Boolean valide,Boolean confirme,Pageable p);
	StockProduit findByStockAndProduit(Stock s,Produit p);
	List<StockProduit> findByStockAndQteStock(Stock s,Integer qteStock);
	List<StockProduit> findByStockAndQteStockLessThan(Stock s,Integer seuil);
	List<StockProduit> findByStockAndProduit_DatePerAfter(Stock s,String date);
	
	List<StockProduit> findByProduitAndQteStockGreaterThan(Produit p,Integer qteStock );
	List<StockProduit> findByProduit(Produit p);
	List<StockProduit> findByStock_NomStock(String nomStock);
	
	Page<StockProduit> findByStock_NomStockAndProduit_RefPContaining(String nomStock,String refP,Pageable pageable);
	Page<StockProduit> findByStock_NomStockAndProduit_LibelleContaining(String nomStock,String libelle,Pageable pageable);
	Page<StockProduit> findByStock_NomStockAndProduit_Classe_NomClasse(String nomStock,String nomClasse,Pageable pageable);
	
	List<StockProduit> findByStock_NomStockAndProduit_RefPContaining(String nomStock,String refP);
	List<StockProduit> findByStock_NomStockAndProduit_LibelleContaining(String nomStock,String libelle);
	List<StockProduit> findByStock_NomStockAndProduit_Classe_NomClasse(String nomStock,String nomClasse);
	
}
