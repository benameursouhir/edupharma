package tn.educanet.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.entities.Departement;
import tn.educanet.entities.Organisation;

@Transactional(readOnly=true)
public interface DepartementRepository  extends JpaRepository<Departement,Long>, CrudRepository<Departement, Long> {
	
	
	Departement findByNomDep(String nomDep);
	
	List<Departement> findByOrganisation(Organisation o);
	Departement findByOrganisation_NomAndNomDep(String nom,String nomDep);

}
