package tn.educanet.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.entities.Produit;

@Transactional(readOnly = true)
public interface ProduitRepository extends JpaRepository<Produit, Long>,
		CrudRepository<Produit, Long>,
		PagingAndSortingRepository<Produit, Long> {

	Produit findByRefP(String refP);
	Produit findByCodeBarre(String codeBarre);

	Produit findByLibelle(String libelle);

	List<Produit> findByDatePerAfter(String date);
	Page<Produit> findByValide(Boolean valide,Pageable p);
	Page<Produit> findByOrganisation_Nom(String nomOrg,Pageable p);
}
