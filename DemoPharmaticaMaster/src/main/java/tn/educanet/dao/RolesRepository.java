package tn.educanet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.entities.Roles;


@Transactional(readOnly=true)
public interface RolesRepository extends JpaRepository<Roles,Long>, CrudRepository<Roles, Long>{
		Roles findByNomRole(String nomRole);
}
