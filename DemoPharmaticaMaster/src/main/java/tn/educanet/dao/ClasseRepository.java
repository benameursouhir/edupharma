package tn.educanet.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.entities.Classe;

@Transactional(readOnly=true)
public interface ClasseRepository extends JpaRepository<Classe,Long>, CrudRepository<Classe, Long> {
		Classe findByNomClasse(String nomClasse);
}
