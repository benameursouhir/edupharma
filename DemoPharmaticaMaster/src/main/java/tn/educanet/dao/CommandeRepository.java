package tn.educanet.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.entities.Commande;
import tn.educanet.entities.Organisation;
import tn.educanet.entities.Produit;
@Transactional(readOnly=true)
public interface CommandeRepository extends JpaRepository<Commande,Long>, CrudRepository<Commande, Long>,
PagingAndSortingRepository<Commande, Long>{
	
	List<Commande> findByOrganisationEnvoiOrderByEtatAsc(Organisation orgE);
	List<Commande> findByOrganisationOrderByEtatAsc(Organisation orgR);
	List<Commande> findByOrganisationAndExisteDTrueOrderByEtatAsc(Organisation orgR);
	List<Commande> findByOrganisationEnvoiAndExisteSTrueOrderByEtatAsc(Organisation orgE);
	
	List<Commande> findByOrganisationEnvoiAndEtatTrue(Organisation orgE);
	List<Commande> findByOrganisationAndEtatTrue(Organisation orgR);
	
	List<Commande> findByOrganisationEnvoiAndEtatFalse(Organisation orgE);
	List<Commande> findByOrganisationAndEtatFalse(Organisation orgR);
	Commande findByDateComm(String date);
	Commande findByBonLivraison(int bonLivraison);
	
	
	Page<Commande> findByOrganisationAndExisteDTrue(Organisation orgE,Pageable  pageable);
	Page<Commande> findByOrganisationEnvoiAndExisteSTrue(Organisation orgE, Pageable pageable);
	Page<Commande> findByOrganisationAndEtatFalseAndExisteDTrue(Organisation orgR,Pageable pageable);
	
	
	
	List<Commande> findByOrganisation_NomAndExisteDTrueAndOrganisationEnvoi_NomAndDateCommContainingAndEtat(String nom,String nomEnvoi,String dateComm,boolean etat);
	List<Commande> findByOrganisation_NomAndExisteDTrueAndOrganisationEnvoi_NomAndDateCommContaining(String nom,String nomEnvoi,String dateComm);
	List<Commande> findByOrganisation_NomAndExisteDTrueAndOrganisationEnvoi_NomAndEtat(String nom,String nomEnvoi,boolean etat);
	List<Commande> findByOrganisation_NomAndExisteDTrueAndOrganisationEnvoi_Nom(String nom,String nomEnvoi);
	List<Commande> findByOrganisation_NomAndExisteDTrueAndDateCommContainingAndEtat(String nom,String dateComm,boolean etat);
	List<Commande> findByOrganisation_NomAndExisteDTrueAndDateCommContaining(String nom,String dateComm);
	
	List<Commande> findByOrganisationEnvoi_NomAndExisteSTrueAndOrganisation_NomAndDateCommContainingAndEtat(String orgE,String nomorg,String dateComm,boolean etat);
	List<Commande> findByOrganisationEnvoi_NomAndExisteSTrueAndOrganisation_NomAndDateCommContaining(String orgE,String nomorg,String dateComm);
	List<Commande> findByOrganisationEnvoi_NomAndExisteSTrueAndOrganisation_NomAndEtat(String orgE,String nomorg,boolean etat);
	List<Commande> findByOrganisationEnvoi_NomAndExisteSTrueAndOrganisation_Nom(String orgE,String nomorg);	
	List<Commande> findByOrganisationEnvoi_NomAndExisteSTrueAndDateCommContainingAndEtat(String orgE,String date,boolean etat);
	List<Commande> findByOrganisationEnvoi_NomAndExisteSTrueAndDateCommContaining(String orgE,String date);	
}
