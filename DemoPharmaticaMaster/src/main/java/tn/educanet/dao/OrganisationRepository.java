package tn.educanet.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Transactional;

import tn.educanet.entities.Adresse;
import tn.educanet.entities.Commande;
import tn.educanet.entities.Organisation;

@Transactional(readOnly=true)
public interface OrganisationRepository extends JpaRepository<Organisation,Long>, CrudRepository<Organisation, Long>,
PagingAndSortingRepository<Organisation, Long>{
	
	List<Organisation> findByNom(String nom);
	Organisation findByEmail(String email);
	Organisation findByNomAndAdresse(String nom,Adresse  adr);
	Organisation findBytelfix(Integer telfix);
	Organisation findByfax(Integer fax);
	Organisation findBytelmobile(Integer telmobile);
	Organisation findByAdresse(Adresse  adr);
	Organisation findByAdresse_VilleAndAdresse_NomRueAndAdresse_CodePostal(String ville,String nomRue,Integer codePostal);
	List<Organisation> findByType(String type);
	List<Organisation> findByTypeAndNom(String type,String nom);
	Page<Organisation> findByTypeNotLike(String type,Pageable pageable);
	Page<Organisation> findByTypeNotLikeAndType(String not,String type,Pageable pageable);

	
	Page<Organisation> findByTypeAndAdresse_NomRue(String type,String nomRue,Pageable pageable);
	List<Organisation> findByTypeNotLike(String type);
	
	Page<Organisation> findByTypeAndAdresse_VilleAndAdresse_NomRueAndNom(String type,String ville,String nomRue,String nom,Pageable pageable);
	Page<Organisation> findByTypeAndAdresse_VilleAndAdresse_NomRue(String type,String ville,String nomRue,Pageable pageable);
	Page<Organisation> findByNomAndAdresse_VilleAndAdresse_NomRue(String nom,String ville,String nomRue,Pageable pageable);
	Page<Organisation> findByTypeAndAdresse_Ville(String type,String ville,Pageable pageable);
	Page<Organisation> findByNomAndAdresse_Ville(String nom,String ville,Pageable pageable);
	Page<Organisation> findByType(String type,Pageable pageable);
	Page<Organisation> findByNom(String nom,Pageable pageable);
	Page<Organisation> findByAdresse_Ville(String ville,Pageable pageable);

}
