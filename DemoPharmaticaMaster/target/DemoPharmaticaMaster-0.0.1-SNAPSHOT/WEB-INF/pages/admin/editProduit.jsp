<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>Produit - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/datepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/colorpicker.css" />

		<!-- fonts -->

		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>

		

		<script type="text/javascript">

  
		function test(str) {
            return /^ *[0-9]+ *$/.test(str);
        }
		 function VerifFloat(f) { 
		      var reg =/^[\+\-]?[0-9]+(([\.\,][0-9]{1})|([\.\,][0-9]{2}))?$/;
		      return (reg.exec(f)!=null);
		   }		
      
 
function valider ()
{
    if ( document.formulaire.refP.value == "" )
    {
        alert ( "Veuillez entrer votre refP !" );
        valid = false;
return valid;
    }
    if ( document.formulaire.libelle.value == "" )
    {
        alert ( "Veuillez entrer votre libelle !" );
        valid = false;
return valid;
    }

    if ( document.formulaire.forme.value == "" )
    {
        alert ( "Veuillez entrer votre forme !" );
        valid = false;
return valid;
    }

    if (test( document.formulaire.codeBarre.value) == "" )
    {
        alert ( "Veuillez entrer votre codeBarre!" );
        valid = false;
return valid;
    }
    if ( test(document.formulaire.qteCarton.value) == "" )
    {
        alert ( "Veuillez entrer votre qteCarton!" );
        valid = false;
return valid;
    }
    if (VerifFloat(document.formulaire.prixVenteHt.value) == "" )
    {
        alert ( "Veuillez entrer votre prixVenteHt!" );
        valid = false;
return valid;
    } 
   
    if ( (document.formulaire.datePer.value) == "" )
    {
        alert ( "Veuillez entrer votre datePer!" );
        valid = false;
return valid;
    }
    if (VerifFloat(document.formulaire.prixAchatHt.value) == "" )
    {
        alert ( "Veuillez entrer votre prixAchatHt!" );
        valid = false;
return valid;
    }
   
    if (VerifFloat(document.formulaire.tauxTva.value) == "" )
    {
        alert ( "Veuillez entrer votre tauxTva!" );
        valid = false;
return valid;
    } 

    if (VerifFloat(document.formulaire.prixVenteTtc.value) == "" )
    {
        alert ( "Veuillez entrer votre prixVenteTtc!" );
        valid = false;
return valid;
    } 
    
    if (VerifFloat(document.formulaire.prixPublic.value) == "" )
    {
        alert ( "Veuillez entrer votre prixPublic!" );
        valid = false;
return valid;
    } 
   
    if (document.formulaire.cl.value == "" )
    {
        alert ( "Veuillez entrer votre classe!" );
        valid = false;
return valid;
    } 
    } 
    

</script>
		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/produits"%>">Produit</a>
							</li>
							<li class="active">Modifier</li>
						</ul><!-- .breadcrumb -->

						<div class="nav-search" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="icon-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- #nav-search -->
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
								Produit
								<small>
									<i class="icon-double-angle-right"></i>
									Modifier
								</small>
								 <small>
        <p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font></p> </small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								 <form:form   name="formulaire"  onsubmit="return valider ()" action="${pageContext.request.contextPath}/modifprod/${produit.prodId}" method="Post" commandName="produit" class="form-horizontal">
									<div class="form-group">
									<table>
									<tr>
									<td>
									<table><tr><td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="refP">Reference produit:</form:label>
                	</td>
                	<td>
                	<br><p><font color="red" size="2" face="Georgia, Arial" >
                    <c:out value="${message1}"/></font></p></td></tr></table>
                	<div class="col-sm-9">
                    <form:input path="refP" id="refP"  class="col-xs-10 col-sm-5" />
                    
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
</td>
									<div class="space-4"></div>
									<td>
									<div class="form-group">
									<table><tr><td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="libelle">Libelle:</form:label>
                	</td>
                	<td>
                	<br><p><font color="red" size="2" face="Georgia, Arial" >
                    <c:out value="${message2}"/></font></p></td></tr></table>
                	<div class="col-sm-9">
                    <form:input path="libelle" id="libelle" class="col-xs-10 col-sm-5"/>
                    
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
									<div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="forme">Forme:</form:label>
                	<div class="col-sm-9">
                    <form:input path="forme"   id="forme" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 
           		 </tr>
           		 <tr>
           		 
									<div class="space-4"></div>
	<td>								
<div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="codeBarre">Code � barre:</form:label>
                	<div class="col-sm-9">
                    <form:input path="codeBarre"  id="codeBarre" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>

				</td>
				<td>
									<div class="space-4"></div>
									

									<div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="qteCarton">Quantit� Carton:</form:label>
                	<div class="col-sm-9">
                    <form:input path="qteCarton"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
                      </td>
                      <td>
									<div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="prixVenteHt">prix Vente Ht:</form:label>
                	<div class="col-sm-9">
                    <form:input path="prixVenteHt"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
						</td>     
				</tr>
				<tr>
<td>
									<div class="form-group">
										<form:label class="col-sm-3 control-label no-padding-right" path="datePer">Date Perimation:</form:label>

										<div class="col-sm-9">
											<form:input type="date" path="datePer" class="col-xs-10 col-sm-5" />
											<span class="help-inline col-xs-12 col-sm-7">
												
										</div>
									</div>
									
									</td>
									<div class="space-4"></div>
									<td>
									<div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="prixAchatHt">Prix Achat HT:</form:label>
                	<div class="col-sm-9">
                    <form:input path="prixAchatHt"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
						
						</td>
						<td>
									<div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="tauxTva">Taux TVA:</form:label>
                	<div class="col-sm-9">
                    <form:input path="tauxTva"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 
           		 </td>

</tr>

	 <tr>
           		 <td>
									<div class="space-4"></div>
									

									<div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="prixVenteTtc">Prix Vente TTC:</form:label>
                	<div class="col-sm-9">
                    <form:input path="prixVenteTtc"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		
           	
           		 <td>
									<div class="space-4"></div>
									

									<div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="prixPublic">Prix public:</form:label>
                	<div class="col-sm-9">
                    <form:input path="prixPublic"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
           			<div class="form-group">
								<label class="col-sm-3 control-label no-padding-right" path="tableau">Tableau:</LABEL>

										<div class="col-sm-9">
											<select name="tab">
									<option value="${produit.tableau}">${produit.tableau}</option>
									<option value="">--</option>
                                    <option value="+80">  +80 </option>
                                    
                                    
  
                                              </select>
										</div>
									</div>
									
           		 <td>
			</tr>
									
<tr>    
                        <div class="space-4"></div>
									
<td>
									<div class="form-group">
										<label class="col-sm-3 control-label no-padding-right" path="classe">Classe:</label>

										<div class="col-sm-9">
                   
											
												<select name="cl">
									<option value="${produit.classe}">${produit.classe}</option>
                                    <option value="medecament">  medecament  </option>
                                    <option value="accessoire"> accessoire </option>
                                    <option value="para">para</option>
  
                                              </select>
										</div>
									</div>
									<div class="space-4"></div>
						</td>
						<td>
					
</td>
						</tr>			                                     
									                                     
									

			
					
</table>
								
										<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" type="submit" value="Submit" name="modifier">
												<i class="icon-ok bigger-110"></i>
												Modifier
												
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn btn-danger"" type="submit">
												<i class="icon-reply icon-only"></i>
												Annuler et retourner
											</button>
										</div>
									</div>
									     </form:form>
 	

								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				
			
			
			
			<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/jquery.dataTables.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/jquery.dataTables.bootstrap.js"></script>

		<!-- ace scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

						
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
