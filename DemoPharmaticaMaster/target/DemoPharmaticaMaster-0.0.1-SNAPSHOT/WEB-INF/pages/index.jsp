<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<html>
<head>
    <title>Spring Page Redirection</title>
</head>
<body>
<h2>Spring Page Redirection</h2>
<p>Click below button to redirect the result to new page</p>
<form:form method="GET" action="/DemoPharmaticaMaster/listDepartments">
<table>
    <tr>
    <td>
    <input type="submit" value="listDepartments"/>
    </td>
    </tr>
</table>  
</form:form>
<form:form method="GET" action="/DemoPharmaticaMaster/produits">
<table>
    <tr>
    <td>
    <input type="submit" value="produits"/>
    </td>
    </tr>
</table>  
</form:form>
<form:form method="GET" action="/DemoPharmaticaMaster/adresses">
<table>
    <tr>
    <td>
    <input type="submit" value="adresses"/>
    </td>
    </tr>
</table>  
</form:form>
<form:form method="GET" action="/DemoPharmaticaMaster/employee">
<table>
    <tr>
    <td>
    <input type="submit" value="employee"/>
    </td>
    </tr>
</table>  
</form:form>
<form:form method="GET" action="/DemoPharmaticaMaster/organisationn">
<table>
    <tr>
    <td>
    <input type="submit" value="organisation"/>
    </td>
    </tr>
</table>  
</form:form>

<form:form method="GET" action="/DemoPharmaticaMaster/commandes">
<table>
    <tr>
    <td>
    <input type="submit" value="commande"/>
    </td>
    </tr>
</table>  
</form:form>

<form:form method="GET" action="/DemoPharmaticaMaster/admin">
<table>
    <tr>
    <td>
    <input type="submit" value="admin"/>
    </td>
    </tr>
</table>  
</form:form>

</body>
</html>