<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>details produit - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
	

		<!-- fonts -->

		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>

		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/produits"%>">Produit</a>
							</li>
							<li class="active">Details</li>
						</ul><!-- .breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
							   Produit
								<small>
									<i class="icon-double-angle-right"></i>
									Details
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
 <fieldset  name="Details produit" style="width:550px">
								<form:form action="${pageContext.request.contextPath}/detailsProd/${produit.prodId}" method="POST" commandName="produit" class="form-horizontal">
									
									
									
						<div class="profile-user-info profile-user-info-striped">
								
								<div class="profile-info-row">
													<div class="profile-info-name"> Reference produit </div>

													<div class="profile-info-value">
														<span class="editable" id="refP">${produit.refP}</span>
													</div>
													
													
								</div>
												
								<div class="profile-info-row">
									<div class="profile-info-name"> Libelle produit </div>

													<div class="profile-info-value">
														<span class="editable" id="libelle">${produit.libelle}</span>
													</div>			
							</div>
							
							<div class="profile-info-row">
									<div class="profile-info-name"> Forme produit </div>

													<div class="profile-info-value">
														<span class="editable" id="forme">${produit.forme}</span>
													</div>			
							</div>
							
							<div class="profile-info-row" style="height: 30px">
									<div class="profile-info-name"> Code � barre </div>

													<div class="profile-info-value">
														<span class="editable" id="codeBarre">${produit.codeBarre}</span>
													</div>			
							</div>
							
							<div class="profile-info-row">
									<div class="profile-info-name"> Quantit� carton </div>

													<div class="profile-info-value">
														<span class="editable" id="qteCarton">${produit.qteCarton}</span>
													</div>			
							</div>
							
							<div class="profile-info-row" style="height: 30px">
									<div class="profile-info-name"> Classe </div>

													<div class="profile-info-value">
														<span class="editable" id="classe">${produit.classe.nomClasse}</span>
													</div>			
							</div>
							
							<div class="profile-info-row">
									<div class="profile-info-name"> Libelle produit </div>

													<div class="profile-info-value">
														<span class="editable" id="libelle">${produit.libelle}</span>
													</div>			
							</div>
							
							<div class="profile-info-row">
									<div class="profile-info-name"> Date p�rimation </div>

													<div class="profile-info-value">
														<span class="editable" id="datePer">${produit.datePer}</span>
													</div>			
							</div>
							
							<div class="profile-info-row" style="height: 30px">
									<div class="profile-info-name">Tableau </div>

													<div class="profile-info-value">
														<span class="editable" id="tableau">${produit.tableau}</span>
													</div>			
							</div>
								
								
							<div class="profile-info-row">
									<div class="profile-info-name">Prix Achat Ht </div>

													<div class="profile-info-value">
														<span class="editable" id="prixAchatHt">${produit.prixAchatHt}</span>
													</div>			
							</div>
								
								
								<div class="profile-info-row">
									<div class="profile-info-name">Prix Vente Ht </div>

													<div class="profile-info-value">
														<span class="editable" id="prixVenteHt">${produit.prixVenteHt}</span>
													</div>			
							</div>
								
								
								<div class="profile-info-row" style="height: 30px">
									<div class="profile-info-name">Taux TVA </div>

													<div class="profile-info-value">
														<span class="editable" id="tauxTva">${produit.tauxTva}</span>
													</div>			
							</div>
							
							<div class="profile-info-row">
									<div class="profile-info-name">Prix Vente TTC </div>

													<div class="profile-info-value">
														<span class="editable" id="prixVenteTtc">${produit.prixVenteTtc}</span>
													</div>			
							</div>
							
							<div class="profile-info-row">
									<div class="profile-info-name">Prix Public</div>

													<div class="profile-info-value">
														<span class="editable" id="prixPublic">${produit.prixPublic}</span>
													</div>			
							</div>
								<c:if test="${produit.organisation.type != 'admin'}">
							<div class="profile-info-row">
									<div class="profile-info-name">Ajout� par</div>

													<div class="profile-info-value">
														<span class="editable" id="organisation">${produit.organisation.nom}</span>
													</div>			
							</div>
							</c:if>
							
							
									
								
								
								
								
								
								

								
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<c:if test="${!produit.confirme}">
											<button class="btn btn-info" type="submit" value="Submit" name="modifier">
												<i class="icon-ok bigger-110"></i>
												Modifier
												
											</button>
</c:if>
<c:if test="${produit.confirme}">
<c:if test="${username == 'admin'}">

											<button class="btn btn-info" type="submit" value="Submit" name="modifier">
												<i class="icon-ok bigger-110"></i>
												Modifier
												
											</button>
</c:if>
</c:if>
											&nbsp; &nbsp; &nbsp;
											<button class="btn btn-danger"" type="submit">
												<i class="icon-reply icon-only"></i>
											retour � la liste
											</button>
										</div>
									</div>

										
                             
                        </form:form>
                        </fieldset>
               
							
									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
					
			
			
			<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

		

		<!-- ace scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

						
													
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
