<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>Employee - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/datepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/colorpicker.css" />

		<!-- fonts -->

			<script type="text/javascript">
			 function test(str) {
                 return /^ *[0-9]+ *$/.test(str);
             }
			 function cin(str) {
		            return /^ *[0-9]{7} *$/.test(str);
		        }
			 function tel(str) {
		            return /^(20|21|22|23|24|25|26|27|28|99|98|97|96|95|94|93|92|50|51|52|53) *[0-9]{6} *$/.test(str);
		        }
			 function VerifFloat(f) { // v�rif validit� email par REGEXP
			      var reg =/^[\+\-]?[0-9]+(([\.\,][0-9]{1})|([\.\,][0-9]{2}))?$/;
			      return (reg.exec(f)!=null);
			   }		
			function bonmail(mailteste)

			{
				var reg = new RegExp('^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i');

				return(reg.test(mailteste));
			}
function valider ( )
{
    if ( document.formulaire.nom.value == "" )
    {
        alert ( "Veuillez entrer votre nom !" );
        valid = false;
return valid;
    }
    if ( document.formulaire.prenom.value == "" )
    {
        alert ( "Veuillez entrer votre prenom !" );
        valid = false;
return valid;
    }
    if ( cin(document.formulaire.cin.value) == "" )
    {
        alert ( "Veuillez entrer votre cin !" );
        valid = false;
return valid;
    }

    if ( tel(document.formulaire.tel.value) == "" )
    {
        alert ( "Veuillez entrer votre tel !" );
        valid = false;
return valid;
    }
    if ( document.formulaire.grade.value == "" )
    {
        alert ( "Veuillez entrer votre grade!" );
        valid = false;
return valid;
    }

    if ( VerifFloat(document.formulaire.salaire.value) == "" )
    {
        alert ( "Veuillez entrer votre salaire!" );
        valid = false;
return valid;
    }
   
    if ( document.formulaire.datenaissance.value == "" )
    {
        alert ( "Veuillez entrer votre date de naissance!" );
        valid = false;
return valid;
    }
    if ( document.formulaire.datedebut.value == "" )
    {
        alert ( "Veuillez entrer votre date de debut!" );
        valid = false;
return valid;
    }

    if ( document.formulaire.departement.value == "" )
    {
        alert ( "Veuillez entrer votre departemet!" );
        valid = false;
return valid;
    }
    
    if(document.formulaire.adr.value == ""){
        if(document.formulaire.ville.value == ""){
          alert ( "Veuillez entrer votre ville!" );
                valid = false;
           return valid;
            }
        if(document.formulaire.codePostal.value == ""){
         alert ( "Veuillez entrer votre code postal!" );
               valid = false;
          return valid;
           }
        if(document.formulaire.nomRue.value == ""){
         alert ( "Veuillez entrer votre nom de rue " );
               valid = false;
          return valid;
           }
               

       }
       
  
}
</script>
	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>

		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
							<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/employee"%>">Employ�</a>
							</li>
							<li class="active">Modifier</li>
						</ul><!-- .breadcrumb -->

					
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
							   Employ�
								<small>
									<i class="icon-double-angle-right"></i>
									Modifier
								</small>
								 <small>
        <p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font></p> </small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
 <fieldset  name="Update employee" style="width:350px">
<form:form  action="${pageContext.request.contextPath}/updateEmp/${employee.empId}" commandName="employee" class="form-horizontal" name="formulaire" onsubmit="return valider()">
				<div class="form-group" style="width: 700px;">
									
                	<form:label class="col-sm-3 control-label no-padding-right" path="empId"></form:label>
                	<div class="col-sm-9">
                    <form path="empId"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>

								<div class="form-group">
									
                	<table>
									<tr>
									<td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="nom">nom:</form:label>
                	<div class="col-sm-9">
                    <form:input path="nom"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
           		 <div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="prenom">prenom:</form:label>
                	<div class="col-sm-9">
                    <form:input path="prenom"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
           		  <div class="form-group">
                	 <table><tr><td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="cin">CIN:</form:label>
                	</td> <td>
                	<br>
                    <p><font color="red" size="2" face="Georgia, Arial" >
                    <c:out value="${message1}"/></font></p></td></tr></table>
                	<div class="col-sm-9">
                    <form:input path="cin"  class="col-xs-10 col-sm-5"/>
             
                   
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 </tr>
           		 <tr>
           		 <td>

							 <div class="form-group">
                	<table><tr><td>
                	<form:label class="col-sm-3 control-label no-padding-right" path="tel">Telephone:</form:label>
                	</td> <td>
                	<br>
                    <p><font color="red" size="2" face="Georgia, Arial" >
                    <c:out value="${message2}"/></font></p></td></tr></table>
                	<div class="col-sm-9">
                    <form:input path="tel"  class="col-xs-10 col-sm-5"/>
                    
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
							 <div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="grade">Grade:</form:label>
                	<div class="col-sm-9">
                    <form:input path="grade"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
							 <div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="salaire">salaire:</form:label>
                	<div class="col-sm-9">
                    <form:input path="salaire"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 </tr>
           		 <tr>
           		 <td>
							 <div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="datenaissance">datenaissance:</form:label>
                	<div class="col-sm-9">
                    <form:input   path="datenaissance" type = "Date" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
           		  <div class="form-group">
                	<form:label class="col-sm-3 control-label no-padding-right" path="datedebut">datedebut:</form:label>
                	<div class="col-sm-9">
                    <form:input  type="Date" path="datedebut"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 <td>
									
										 <div class="form-group">
                <form:label class="col-sm-3 control-label no-padding-right" path="departement" >Departement:</form:label>
                <div class="col-sm-9">
                     <form:select name="dep" path="departement">
                    <option value="${employee.departement}">${employee.departement}</option>
                    <c:forEach var="cl" items="${departements}">
                        <option value="${cl}">${cl.nomDep}</option>
                    </c:forEach>
                </form:select>
                </div>
            </div>
             </td>
             </tr>
             <tr>
             <td>
             <td>
             <div class="control-group">
               <label class="col-sm-3 control-label no-padding-right" path="adresse" > Adresse:</label>
                <div class="controls">
                    <select name="adr" >
                    <option value="${employee.adresse.ville},${employee.adresse.nomRue},${employee.adresse.codePostal}">${employee.adresse.ville},${employee.adresse.nomRue},${employee.adresse.codePostal}</option>
                    <c:forEach var="cl1" items="${adresses}">
                        <option value="${cl1.ville},${cl1.nomRue},${cl1.codePostal}" >${cl1.ville} ${cl1.nomRue} ${cl1.codePostal}</option>
                        
                    </c:forEach>
                </select>
                </div>
            </div>
</td>
</tr>
  <tr>
           <td colspan="3">
           <div class="space-4"></div>
           <div class="page-content">
						<div class="page-header">
						<h4>
           Nouvelle Adresse:</h4>
           </div></div>
           </td>
           </tr> 
           		<tr>
<td>
									<div class="space-4"></div>
							
<div class="form-group">
                	<label class="col-sm-3 control-label no-padding-right" >Nom de rue:</label>
                	<div class="col-sm-9">
                    <input  name="nomRue"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>

			</td>
			<td>
									<div class="space-4"></div>
									

									<div class="form-group">
                	<label class="col-sm-3 control-label no-padding-right" >Ville:</label>
                	<div class="col-sm-9">
                    <input name="ville" class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
                           
                   </td>
                   <td>
                        
									<div class="space-4"></div>
								                                     
									
									<div class="form-group">
                	<label class="col-sm-3 control-label no-padding-right" >Code postal:</label>
                	<div class="col-sm-9">
                    <input name="codePostal"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>
           		 </td>
           		 </tr>
</table>
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" type="submit" value="Submit" name="modifier">
												<i class="icon-ok bigger-110"></i>
												modifier
												
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn btn-danger"" type="submit">
												<i class="icon-reply icon-only"></i>
												Annuler et retourner
											</button>
										</div>
									</div>
									
									
									
 	
									

										
                             
     </form:form></fieldset>
							
									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->					
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
