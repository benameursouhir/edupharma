<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>Organisation - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/datepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/colorpicker.css" />

		<!-- fonts -->

		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>

		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
							<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/organisationn"%>">Organisation</a>
							</li>
							<li class="active">Details</li>
						</ul><!-- .breadcrumb -->

					
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
							   Organisation
								<small>
									<i class="icon-double-angle-right"></i>
									Details
								</small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
 <fieldset  name="Details Organisation" style="width:550px">
								<form:form action="${pageContext.request.contextPath}/detailsUser/${organisation.orgId }" method="post" commandName="organisation" class="form-horizontal">
									
									
									
						<div class="profile-user-info profile-user-info-striped" style="height:230px;">
								
								<div class="profile-info-row" style="height:30px;">
									<div class="profile-info-name"> Nom Organisation </div>
			<input type="hidden" value="${organisation.orgId }"/> 
																<div class="profile-info-value">
														<span class="editable" id="nom">${organisation.nom}</span>
													</div>
									</div>
												
								<div class="profile-info-row" style="height:30px;">
													<div class="profile-info-name" >Telephone mobile </div>

													<div class="profile-info-value">
														<span class="editable" id="telmobile">${organisation.telmobile}</span>
													</div>
								</div>
								<div class="profile-info-row"  style="height:30px;">
													<div class="profile-info-name">Numero du fax</div>

													<div class="profile-info-value">
														<span class="editable" id="fax">${organisation.fax}</span>
													</div>
								</div>
								
								
								<div class="profile-info-row" style="height:30px;">
									<div class="profile-info-name">Telephone fix</div>

													<div class="profile-info-value">
														<span class="editable" id="telfix">${organisation.telfix}</span>
													</div>
										</div>		
								<div class="profile-info-row" style="height:30px;">
													<div class="profile-info-name">Type </div>

													<div class="profile-info-value">
														<span class="editable" id="type">${organisation.type}</span>
													</div>
								</div>
								<div class="profile-info-row"  style="height:30px;">
													<div class="profile-info-name">Email </div>

													<div class="profile-info-value">
														<span class="editable" id="email">${organisation.email}</span>
													</div>
								</div>
								
								
								<div class="profile-info-row" style="height:50px;">
													<div class="profile-info-name">Adresse </div>

													<div class="profile-info-value">
														<span class="editable" id="adresse">${organisation.adresse.nomRue}  ${organisation.adresse.ville}  ${organisation.adresse.codePostal}</span>
													</div>
								</div>
								
								
												
							</div>
								
								
								
								<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
										
											<button class="btn btn-info" type="submit" value="Submit" name="modifier">
												<i class="icon-ok bigger-110"></i>
												Modifier
												
											</button>
											
											
											&nbsp; &nbsp; &nbsp;
											<button class="btn btn-danger" type="submit">
												<i class="icon-reply icon-only"></i>
											retour � la liste
											</button>
										</div>
									</div>
								
                             
                        </form:form></fieldset>
               
							
									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				
			<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

	

		<!-- ace scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

												
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
