<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>Commande - Reçu</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>
	<script type="text/javascript">


 function valider() {
	 if(document.search.nomOrg.value==""&& document.search.dateComm.value=="" && document.search.etatComm.value==""){
			alert("Choisir le terme à chercher");
			 valid = false;
			 return valid;
		}


	 if(document.search.nomOrg.value==""&& document.search.dateComm.value=="" && document.search.etatComm.value!=""){
			alert("Choisir le terme à chercher");
			 valid = false;
			 return valid;
		}
	
	 
	 }
		
	</script>	
		
	</head>

	<body>
	
			<div class="main-content">

						
				
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

													</ul><!-- .breadcrumb -->

						
					</div>

					<div class="page-content">
					
 <small>
        <p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font>
                    <c:out value="${message1}"/></font></p> </small>
                    
                    
 		<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								<div class="col-xs-12 col-sm-6 widget-container-span" style="width: 50%">
		
		
		<div class="widget-box">
											<div class="widget-header widget-header-large">
												<h4>Recherche Commande</h4>

												<div class="widget-toolbar">
													
 												<a href="#" data-action="collapse">
														<i class="icon-chevron-up"></i>
													</a>

													
												</div>
											</div>

												<div class="widget-body" style="height:200px">
												<div class="widget-main" >
												
												<form:form method="get" commandName="page"  name="search"  onsubmit="return valider()"  action="searchCommRecu"  class="form-horizontal">
														<table>
														<tr>
												<td>Etat Commande
											<select name="etatComm" id="etatComm" style="margin-top: -2px;width: 150px" class="form-control search-query" >
												<option value="">-----</option>
                                   				<option value="true">  traitée </option>
                                   				<option value="false">  non traitée </option>
                                   			</select>
                                   			
															
										</td>
											<td>Date Commande <input type="date"  name="dateComm"  id="dateComm" class="col-xs-10 col-sm-5"/></td>
										<td>Nom Organisation
											<input type="text" name="nomOrg"  id="nomOrg" class="form-control search-query" placeholder="recherche par nom"/>
										</td>
												
									
														</tr>
														
														
														<tr>
														<td colspan="3">
															<div class="clearfix form-actions" >
										<div class="col-md-offset-3 col-md-9" style="width:100%;float: right"  >
											<button class="btn btn-info" type="submit" value="Submit">
												<i class="icon-search icon-on-right bigger-110"></i>
											Rechercher
												
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												annuler
											</button>
										</div>
									</div></td>
														
														</tr>
														</table>
							</form:form>
													
												</div>
											</div>
										</div>
										</div>		                   
                    
                    
                    
                    
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
	<c:if test="${empty page}">
	<font color="blue"><b>Liste vide</b></font>
	</c:if>
 <c:if test="${!empty page}">
                <div class="table-header">
										Liste des commandes reçues</div>
										
			<table id="sample-table-2" cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
				<thead>
													 <tr>
                         <th></th>
                        <th>Etat</th>
                         <th>Date commande</th>
                         <th>Total</th>
                         <th>Details</th>
                         <th>Imprimer</th>
                         <th>Supprimer</th>
                         
                        
                        
                    </tr>
				</thead>

												
				<tbody role="alert" aria-live="polite" aria-relevant="all">
					<c:forEach items="${page.content}" var="commande" varStatus="loopStatus">
											<tr class="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}">
											
														<td class="center  sorting_1">
															<input type="hidden" name="comId"  value="${commande.comId}"/>
														</td>

														<td class=" ">
															<c:choose>
														<c:when test="${commande.etat==false}">
														
														<span class="label label-warning">non traitée</span>
														</c:when>	
														<c:otherwise>
														<span class="label label-success arrowed-in arrowed-in-right">traitée</span>
														</c:otherwise>
														</c:choose>
														</td>
														
														<td class=" " >	${commande.dateComm}							
					</td>

					<td class="hidden-480 ">
														
						${commande.totalTtc}
					</td>
					
					<td class=" ">
														
									<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="detailsCom/${commande.comId}" method="get">
								
								<button class="btn btn-xs btn-success">
							    <i class="icon-ok bigger-120"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
						
						</form>
					</div>
							
														
														
														</td>
							<td>
					<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="imprime/${commande.comId}" method="get">
								
								<button class="btn btn-xs btn-info">
							  <i class="icon-print  align-top bigger-125 icon-on-right"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
						
						</form>
						
					</div>
					
							
														</td>
														
														<td class="hidden-480 ">
														<form action="deleteComm/${commande.comId}" method="post">
							<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                               
							         <button class="btn btn-xs btn-danger">
																	<i class="icon-trash bigger-120"></i>
								
																</button>
							</div>
							<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																	<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
																				<span class="red">
																					<i class="icon-trash bigger-120"></i>
																				</span>
																			</a>
																	</li>
																	</ul>
								</div>
                                </div>								
						</form>
					
														</td>
				</tr>
													
											</c:forEach>
					</tbody>						
             </table>
               <div class="row-fluid">
			<util:pagination thispage="${page}"></util:pagination></div>
    </c:if>		
			
					
									

										
                             
                       
									

										
                             
                       
							
									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				
			
			
			
			<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

	

		<!-- inline scripts related to this page -->
<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>


		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

						
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
