<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>Ajout Commande - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/core/demos.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.all.css">
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery-1.10.2.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.core.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.widget.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.button.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.position.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.menu.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.autocomplete.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.tooltip.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/chosen.jquery.min.js"></script>
		<!-- fonts -->

		<style>
	.custom-combobox {
		position: relative;
		display: inline-block;
	}
	.custom-combobox-toggle {
		position: absolute;
		top: 0;
		bottom: 0;
		margin-left: -1px;
		padding: 0;
		/* support: IE7 */
		*height: 1.7em;
		*top: 0.1em;
	}
	.custom-combobox-input {
		margin: 0;
		padding: 0.3em;
	}
	</style>
		<script type="text/javascript">

	
		  
    function test(str) {
                 return /^ *[0-9]+ *$/.test(str);
             }
       
   

function addLigne(link) {
	// 1. r�cuperer le node "TABLE" � manipuler
var prod=document.formulaire.prod.value;
	//var i=document.formulaire.numRow.value;
	var qt=document.formulaire.quantite.value;
	
	if(prod=="")
		alert("veuillez choisir un produit");
		else if(test(qt)=="")
			alert("Entrer la quantit� � commander ");
			else{
				//alert("ok");
				//i=0;
				//alert(i);	
	
	
	var col1 = document.createElement('td');
	var col2 = document.createElement('td');
	var col3 = document.createElement('td');

	//var num=parseFloat(i);
	//num++;
	//document.formulaire.numRow.value=num;
	
	
	col1.innerHTML ='<td><input style="width:30%" readonly="" type="text" class="col-xs-10 col-sm-5"  name="prod" value="'+prod+'"/></td>' ;
	col2.innerHTML ='<td><input style="width:30%" type="text" class="col-xs-10 col-sm-5"  name="qte" value="'+qt+'"/></td>' ;
	col3.innerHTML ='<td  style="width:30%" class="visible-md visible-lg hidden-sm hidden-xs btn-group"> <button onclick=delLigne(this.parentNode.rowIndex); return false; class="btn btn-xs btn-danger"><i class="icon-trash bigger-120"></i></button></td></tr>';
		
	

	var newRow = document.createElement('tr');

	newRow.appendChild(col1) ;
	newRow.appendChild(col2) ;
	newRow.appendChild(col3);

	//document.getElementById("listeProd").value = document.getElementById("listeProd").value+"," +prod;
	//document.getElementById("listeQte").value = document.getElementById("listeQte").value+"," +qt;
	
	document.getElementById("t").appendChild(newRow);	
	document.formulaire.prod.value="";
	document.formulaire.quantite.value="";

			}
}

/* supprimer une ligne */
function delLigne(num) {
	// 1. r�cuperer le node "TABLE" � manipuler

	document.getElementById("t").deleteRow(num+1);
	
}



function valider(){

alert(document.getElementsByTagName('input').length);
if(document.formulaire.org.value==""){
	alert("Entrer le fournisseur");
	 valid = false;
	 return valid;
}





	
</script>


		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>
 <style type="text/css">
  .hidden{
   visibility:hidden;
}
  </style>
  
  <script>
	(function( $ ) {
		$.widget( "custom.combobox", {
			_create: function() {
				this.wrapper = $( "<span>" )
					.addClass( "custom-combobox" )
					.insertAfter( this.element );

				this.element.hide();
				this._createAutocomplete();
				this._createShowAllButton();
			},

			_createAutocomplete: function() {
				var selected = this.element.children( ":selected" ),
					value = selected.val() ? selected.text() : "";

				this.input = $( "<input>" )
					.appendTo( this.wrapper )
					.val( value )
					.attr( "title", "" )
					.addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
					.autocomplete({
						delay: 0,
						minLength: 0,
						source: $.proxy( this, "_source" )
					})
					.tooltip({
						tooltipClass: "ui-state-highlight"
					});

				this._on( this.input, {
					autocompleteselect: function( event, ui ) {
						ui.item.option.selected = true;
						this._trigger( "select", event, {
							item: ui.item.option
						});
					},

					autocompletechange: "_removeIfInvalid"
				});
			},

			_createShowAllButton: function() {
				var input = this.input,
					wasOpen = false;

				$( "<a>" )
					.attr( "tabIndex", -1 )
					.attr( "title", "Show All Items" )
					.tooltip()
					.appendTo( this.wrapper )
					.button({
						icons: {
							primary: "ui-icon-triangle-1-s"
						},
						text: false
					})
					.removeClass( "ui-corner-all" )
					.addClass( "custom-combobox-toggle ui-corner-right" )
					.mousedown(function() {
						wasOpen = input.autocomplete( "widget" ).is( ":visible" );
					})
					.click(function() {
						input.focus();

						// Close if already visible
						if ( wasOpen ) {
							return;
						}

						// Pass empty string as value to search for, displaying all results
						input.autocomplete( "search", "" );
					});
			},

			_source: function( request, response ) {
				var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
				response( this.element.children( "option" ).map(function() {
					var text = $( this ).text();
					if ( this.value && ( !request.term || matcher.test(text) ) )
						return {
							label: text,
							value: text,
							option: this
						};
				}) );
			},

			_removeIfInvalid: function( event, ui ) {

				// Selected an item, nothing to do
				if ( ui.item ) {
					return;
				}

				// Search for a match (case-insensitive)
				var value = this.input.val(),
					valueLowerCase = value.toLowerCase(),
					valid = false;
				this.element.children( "option" ).each(function() {
					if ( $( this ).text().toLowerCase() === valueLowerCase ) {
						this.selected = valid = true;
						return false;
					}
				});

				// Found a match, nothing to do
				if ( valid ) {
					return;
				}

				// Remove invalid value
				this.input
					.val( "" )
					.attr( "title", value + " didn't match any item" )
					.tooltip( "open" );
				this.element.val( "" );
				this._delay(function() {
					this.input.tooltip( "close" ).attr( "title", "" );
				}, 2500 );
				this.input.data( "ui-autocomplete" ).term = "";
			},

			_destroy: function() {
				this.wrapper.remove();
				this.element.show();
			}
		});
	})( jQuery );

	$(function() {
		$( "#org" ).combobox();
		$( "#toggle" ).click(function() {
			$( "#org" ).toggle();
		});
	});
	$(function() {
		$( "#emp" ).combobox();
		$( "#toggle" ).click(function() {
			$( "#emp" ).toggle();
		});
	});
	$(function() {
		$( "#prod" ).combobox();
		$( "#toggle" ).click(function() {
			$( "#prod" ).toggle();
		});
	});
	</script>
		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Commande</a>
								
							</li>
							<li class="active">Nouveau</li>
						</ul><!-- .breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
							   Commande
								<small>
									<i class="icon-double-angle-right"></i>
									Ajouter
								</small>
							</h1>
						</div><!-- /.page-header -->
 <small>
        <p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font></p> </small>
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
<p><br>
								<form:form  name="formulaire" method="get"  onsubmit="return valider()" action="ajoutCommandes" commandName="commande"  class="form-horizontal">
									<div class="form-group">
									<table>
									<tr>
									
									<td>
           		<div class="form-group" >
           		<div class="ui-widget" >
                <form:label class="col-sm-3 control-label no-padding-right" path="organisation" >Organisation:</form:label>
                <div class="col-sm-9" >
             
                     <select name="org"  id="org"  path="organisation" >
                    <option value="">-------------</option>
                    <c:forEach var="cl" items="${organisations}">
                        <option  value="${cl.nom},${cl.adresse.ville},${cl.adresse.nomRue},${cl.adresse.codePostal}">${cl.nom} : ${cl.adresse.ville} ${cl.adresse.nomRue}  ${cl.adresse.codePostal} </option>
                        
                    </c:forEach>
                </select>
               
                </div>
                </div>
                
            </div>
            
           		 </td>
           		<td>
           		<button class="btn btn-app btn-warning" style="height: 50px;width: 80px">
												<i class="icon-share-alt bigger-200"></i>
												Suivant
											</button>
           		</td> 
           		 </tr>
           		 
				
				
				          		 
</table>

										</div><!-- /.table-responsive -->
									</div><!-- /span -->
								</div><!-- /row -->

								
									
							
									</form:form>
									
									
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				
				
				
		
				
		

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
	
		<!-- basic scripts -->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

		

		<!-- ace scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

						
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
