<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>Produit - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.all.css">
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery-1.10.2.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.core.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.widget.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.button.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.position.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.menu.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.autocomplete.js"></script>

		<!-- fonts -->


		<style>
	  .hidden{
   visibility:hidden;
}	
		
	.custom-combobox {
		position: relative;
		display: inline-block;
	}
	.custom-combobox-toggle {
		position: absolute;
		top: 0;
		bottom: 0;
		margin-left: -1px;
		padding: 0;
		/* support: IE7 */
		*height: 1.7em;
		*top: 0.1em;
	}
	.custom-combobox-input {
		margin: 0;
		padding: 0.3em;
	}
	</style>

		<!-- fonts -->

		
	<script type="text/javascript">

 function test(str) {
                 return /^ *[0-9]+ *$/.test(str);
             }

 function valider(){
	 if(document.formulaire.prod.value==""){
			alert("Choisir le produit à modifier sa quantité du stock");
			 valid = false;
			 return valid;
		}
	 if(test(document.formulaire.qte.value)==""){
			alert("Entrer la quantité du stock");
			 valid = false;
			 return valid;
		}
	 }
 
 function valider_search() {
	 if(document.search.terme.value==""&& document.search.option.value==""){
			alert("Choisir le produit à chercher");
			 valid = false;
			 return valid;
		}
	 if((document.search.option.value=="refProd" || document.search.option.value=="libelle") && document.search.terme.value=="" ){
			alert("Choisir le terme à rechercher");
			 valid = false;
			 return valid;
		}
	 
	 }
		
	</script>	
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>
<script>
	(function( $ ) {
		$.widget( "custom.combobox", {
			_create: function() {
				this.wrapper = $( "<span>" )
					.addClass( "custom-combobox" )
					.insertAfter( this.element );

				this.element.hide();
				this._createAutocomplete();
				this._createShowAllButton();
			},

			_createAutocomplete: function() {
				var selected = this.element.children( ":selected" ),
					value = selected.val() ? selected.text() : "";

				this.input = $( "<input>" )
					.appendTo( this.wrapper )
					.val( value )
					.attr( "title", "" )
					.addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
					.autocomplete({
						delay: 0,
						minLength: 0,
						source: $.proxy( this, "_source" )
					})
					.tooltip({
						tooltipClass: "ui-state-highlight"
					});

				this._on( this.input, {
					autocompleteselect: function( event, ui ) {
						ui.item.option.selected = true;
						this._trigger( "select", event, {
							item: ui.item.option
						});
					},

					autocompletechange: "_removeIfInvalid"
				});
			},

			_createShowAllButton: function() {
				var input = this.input,
					wasOpen = false;

				$( "<a>" )
					.attr( "tabIndex", -1 )
					.attr( "title", "Show All Items" )
					.tooltip()
					.appendTo( this.wrapper )
					.button({
						icons: {
							primary: "ui-icon-triangle-1-s"
						},
						text: false
					})
					.removeClass( "ui-corner-all" )
					.addClass( "custom-combobox-toggle ui-corner-right" )
					.mousedown(function() {
						wasOpen = input.autocomplete( "widget" ).is( ":visible" );
					})
					.click(function() {
						input.focus();

						// Close if already visible
						if ( wasOpen ) {
							return;
						}

						// Pass empty string as value to search for, displaying all results
						input.autocomplete( "search", "" );
					});
			},

			_source: function( request, response ) {
				var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
				response( this.element.children( "option" ).map(function() {
					var text = $( this ).text();
					if ( this.value && ( !request.term || matcher.test(text) ) )
						return {
							label: text,
							value: text,
							option: this
						};
				}) );
			},

			_removeIfInvalid: function( event, ui ) {

				// Selected an item, nothing to do
				if ( ui.item ) {
					return;
				}

				// Search for a match (case-insensitive)
				var value = this.input.val(),
					valueLowerCase = value.toLowerCase(),
					valid = false;
				this.element.children( "option" ).each(function() {
					if ( $( this ).text().toLowerCase() === valueLowerCase ) {
						this.selected = valid = true;
						return false;
					}
				});

				// Found a match, nothing to do
				if ( valid ) {
					return;
				}

				// Remove invalid value
				this.input
					.val( "" )
					.attr( "title", value + " didn't match any item" )
					.tooltip( "open" );
				this.element.val( "" );
				this._delay(function() {
					this.input.tooltip( "close" ).attr( "title", "" );
				}, 2500 );
				this.input.data( "ui-autocomplete" ).term = "";
			},

			_destroy: function() {
				this.wrapper.remove();
				this.element.show();
			}
		});
	})( jQuery );

	$(function() {
		$( "#prod" ).combobox();
		$( "#toggle" ).click(function() {
			$( "#prod" ).toggle();
		});
	});
	
	
	</script>
		
		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/stockProduits"%>">Produits</a>
							</li>
							<li class="active">Liste</li>
						</ul><!-- .breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
								Produits
								<small>
									<i class="icon-double-angle-right"></i>
									Liste
								</small>
								 <small>
        <p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font></p> </small>
							</h1>
						</div><!-- /.page-header -->

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								<div class="col-xs-12 col-sm-6 widget-container-span" style="width: 50%">
										<div class="widget-box">
											<div class="widget-header widget-header-large">
												<h4>Modifier quantite produit</h4>

												<div class="widget-toolbar">
													
 												<a href="#" data-action="collapse">
														<i class="icon-chevron-up"></i>
													</a>

													
												</div>
											</div>

												<div class="widget-body" style="height:250px">
												<div class="widget-main" >
												
														<form method="post"  name="formulaire"  onsubmit="return valider ()"  action="modifQte"  class="form-horizontal">
														<table>
														<tr>
														<td>
														
														<div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" path="produit" >Produit:</label>
                <div class="col-sm-9" >
                     <select name="prod" id="prod" >
                    <option value="">-------------</option>
                    <c:forEach var="cl1" items="${page.content}">
                        <option value="${cl1.produit.refP},${cl1.produit.libelle}" >${cl1.produit.refP} : ${cl1.produit.libelle}</option>
                    </c:forEach>
                </select>
                
                </div>
            </div>
														</td>
														<td>
														<div class="form-group">
                <label class="col-sm-3 control-label no-padding-right" path="qte" >Quantité: </label>
                <input type="text" name="qte" id="qte" style="width:100px" />"
														</td>
														</tr>
														
														
														<tr>
														<td colspan="2">
															<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9" style="width:100%; float:right"  >
											<button class="btn btn-info" type="submit" value="Submit">
												<i class="icon-ok bigger-110"></i>
												Modifier
												
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												annuler
											</button>
										</div>
									</div></td>
														
														</tr>
														</table>
									</form>
													
												</div>
											</div>
										</div>
									
										<div class="widget-box">
											<div class="widget-header widget-header-large">
												<h4>Recherche produit</h4>

												<div class="widget-toolbar">
													
 												<a href="#" data-action="collapse">
														<i class="icon-chevron-up"></i>
													</a>

													
												</div>
											</div>

												<div class="widget-body" style="height:200px">
												<div class="widget-main" >
												
												<form:form method="get" commandName="page"  name="search"  onsubmit="return valider_search ()"  action="searchItems"  class="form-horizontal">
														<table>
														<tr>
														<td>
											<select path="page" name="option" id="option" >
												<option value="${option}">${option}</option>
                                   				<option value="refProd">  Refenrence </option>
                                   				<option value="libelle">  Libelle </option>
                                   				<c:forEach var="cla" items="${classe}">
                       							<option value="${cla.nomClasse}" >${cla.nomClasse}</option>
                   								</c:forEach>
                                   			</select>
															
										</td>
										<td>
										
												<input type="text" name="terme" value="${terme}"   id="terme" class="form-control search-query" placeholder="terme de recherche"/>
												
										</td>
														</tr>
														
														
														<tr>
														<td colspan="2">
															<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9" style="width:100%; float:right"  >
											<button class="btn btn-info" type="submit" value="Submit">
												<i class="icon-search icon-on-right bigger-110"></i>
											Rechercher
												
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												annuler
											</button>
										</div>
									</div></td>
														
														</tr>
														</table>
							</form:form>
													
												</div>
											</div>
										</div>
										</div>
	
	
	
	
	
	
	
	
	
	
	
	
	
	
				
				
											
			
<div class="row">
		<div class="col-xs-12">
		
    <c:if test="${!empty page}">
                <h3 class="header smaller lighter blue">produits</h3>
				<div class="table-header">
										Listes des produits
										</div>
										
			<table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
				<thead>
													 <tr>
                        <th></th>
                        <th>refProduit</th>
                        <th>Libelle</th>
                         <th>Quantité stock</th>
                          <th>&nbsp;</th>
                        <th>&nbsp;</th>
                         <th>&nbsp;</th>
                    </tr>
				</thead>

												
											<tbody role="alert" aria-live="polite" aria-relevant="all">
											<c:forEach items="${page.content}" var="p" varStatus="loopStatus">
											<tr class="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}">
											
														

														<td class=" ">
															<input type="hidden" name="comId"  value="${p.stockProdId}"/>
															<input type="hidden" name="comId"  value="${p.produit.prodId}"/>
														</td>
														<td class=" ">${p.produit.refP}</td>
														<td class="hidden-480 ">${p.produit.libelle}</td>
														<td class="center  sorting_1">
															${p.qteStock}
														</td>
														<td>
														<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="detailsStockProd/${p.stockProdId}" method="get">
								
								<button class="btn btn-xs btn-success">
							    <i class="icon-ok bigger-120"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
						</form>
					</div>	
														</td>
														<td class=" ">
					<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="modifprodUser/${p.stockProdId}" method="get">
								
								<button class="btn btn-xs btn-info">
							    <i class="icon-edit bigger-120"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
								                                          
								 
								 
								 
								 
                         
						</form>
					</div>
																
					</td>

					<td class="hidden-480 ">
														
						<form name="suppression" onSubmit="return Demande_verification()" action="deleteStockProd/${p.stockProdId}" method="post">
							<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                               
							         <button class="btn btn-xs btn-danger">
																	<i class="icon-trash bigger-120"></i>
								
																</button>
							</div>
							<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																	<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
																				<span class="red">
																					<i class="icon-trash bigger-120"></i>
																				</span>
																			</a>
																	</li>
																	</ul>
								</div>
                                </div>								
						</form>
					</td>

														
				</tr>
													
											</c:forEach>
					</tbody>						
             </table>
             <div class="row-fluid">
			<util:pagination thispage="${page}"></util:pagination></div>
    </c:if>
							
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				
				</div>
				
		
				
		

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="icon-double-angle-up icon-only bigger-110"></i>
			</a>
	
		<!-- basic scripts -->

		<!--[if !IE]> -->

		<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

		<!-- page specific plugin scripts -->


		<!-- ace scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
			
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
