<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/datepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/colorpicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/core/demos.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.all.css">
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery-1.10.2.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.core.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.widget.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.button.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.position.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.menu.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.autocomplete.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/core/jquery.ui.tooltip.js"></script>

		<!-- fonts -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>
		<style>
	.custom-combobox {
		position: relative;
		display: inline-block;
	}
	.custom-combobox-toggle {
		position: absolute;
		top: 0;
		bottom: 0;
		margin-left: -1px;
		padding: 0;
		/* support: IE7 */
		*height: 1.7em;
		*top: 0.1em;
	}
	.custom-combobox-input {
		margin: 0;
		padding: 0.3em;
		width:250px;
	}
	</style>
	<script type="text/javascript">

	  function test(str) {
          return /^ *[0-9]+ *$/.test(str);
      }

	function addLigne(link) {
		// 1. r�cuperer le node "TABLE" � manipuler
	
	var prod=document.formulaire.prod.value;
	
	var qt=document.formulaire.quantite.value;
	
		if(prod=="")
			alert("veuillez choisir un produit");
			else if(test(qt)=="")
				alert("Entrer la quantit� � commander ");
		
				else{
					
		var col1 = document.createElement('td');
		var col2 = document.createElement('td');
		var col3 = document.createElement('td');

		col1.innerHTML ='<td ><input style="width:30%;margin-left:40px" readonly="" type="text" class="col-xs-10 col-sm-5"  name="produit" value="'+prod+'"/></td>' ;
		col2.innerHTML ='<td><input style="width:30%;margin-left:130px" type="text" class="col-xs-10 col-sm-5"   name="quant" value="'+qt+'"/></td>' ;
		col3.innerHTML ='<td  style="width:30%" class="visible-md visible-lg hidden-sm hidden-xs btn-group"> <button onclick="delLigne(this.parentNode.rowIndex); return false;" class="btn btn-xs btn-danger"><i class="icon-trash bigger-120"></i></button></td></tr>';
			
		

		var newRow = document.createElement('tr');

		newRow.appendChild(col1) ;
		newRow.appendChild(col2) ;
		newRow.appendChild(col3);
		document.getElementById("nv").appendChild(newRow);	
		document.formulaire.prod.value="";
		document.formulaire.quantite.value="";

				}
	}
			
			/* supprimer une ligne */
			function delLigne(num) {
				// 1. r�cuperer le node "TABLE" � manipuler

				document.getElementById("nv").deleteRow(num+1);
				
			}

			function delLigne1(num) {
				// 1. r�cuperer le node "TABLE" � manipuler

				document.getElementById("p").deleteRow(num+1);
				
			}

			
			function compteur(){
				
				var l=document.getElementsByTagName('input').length;
				var l1=document.getElementsByTagName('select').length;
		
				var pc=parseInt(document.getElementById("listeProd").value);
				document.getElementById("listeProd").value =null;
				for(var i=4;i<(4+pc);i++){
					document.getElementById("listeQte").value =document.getElementById("listeQte").value+","+document.getElementsByTagName('input')[i].value;
						}
				for(var i=(4+pc+2+1);i<l;i+=2){
					document.getElementById("listeQte").value =document.getElementById("listeQte").value+","+document.getElementsByTagName('input')[i].value;
											}
				for(var i=3;i<3+pc;i++){
					document.getElementById("listeProd").value =document.getElementById("listeProd").value+","+document.getElementsByTagName('select')[i].value;
						}
				
				for(var i=(4+pc+2);i<l;i+=2){
					
					document.getElementById("listeProd").value =document.getElementById("listeProd").value+","+document.getElementsByTagName('input')[i].value;
											}
					}
		
	
	</script>
		
			<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>
 <style type="text/css">
  .hidden{
   visibility:hidden;
}
  </style>
  
  <script>
	(function( $ ) {
		$.widget( "custom.combobox", {
			_create: function() {
				this.wrapper = $( "<span>" )
					.addClass( "custom-combobox" )
					.insertAfter( this.element );

				this.element.hide();
				this._createAutocomplete();
				this._createShowAllButton();
			},

			_createAutocomplete: function() {
				var selected = this.element.children( ":selected" ),
					value = selected.val() ? selected.text() : "";

				this.input = $( "<input>" )
					.appendTo( this.wrapper )
					.val( value )
					.attr( "title", "" )
					.addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
					.autocomplete({
						delay: 0,
						minLength: 0,
						source: $.proxy( this, "_source" )
					})
					.tooltip({
						tooltipClass: "ui-state-highlight"
					});

				this._on( this.input, {
					autocompleteselect: function( event, ui ) {
						ui.item.option.selected = true;
						this._trigger( "select", event, {
							item: ui.item.option
						});
					},

					autocompletechange: "_removeIfInvalid"
				});
			},

			_createShowAllButton: function() {
				var input = this.input,
					wasOpen = false;

				$( "<a>" )
					.attr( "tabIndex", -1 )
					.attr( "title", "Show All Items" )
					.tooltip()
					.appendTo( this.wrapper )
					.button({
						icons: {
							primary: "ui-icon-triangle-1-s"
						},
						text: false
					})
					.removeClass( "ui-corner-all" )
					.addClass( "custom-combobox-toggle ui-corner-right" )
					.mousedown(function() {
						wasOpen = input.autocomplete( "widget" ).is( ":visible" );
					})
					.click(function() {
						input.focus();

						// Close if already visible
						if ( wasOpen ) {
							return;
						}

						// Pass empty string as value to search for, displaying all results
						input.autocomplete( "search", "" );
					});
			},

			_source: function( request, response ) {
				var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
				response( this.element.children( "option" ).map(function() {
					var text = $( this ).text();
					if ( this.value && ( !request.term || matcher.test(text) ) )
						return {
							label: text,
							value: text,
							option: this
						};
				}) );
			},

			_removeIfInvalid: function( event, ui ) {

				// Selected an item, nothing to do
				if ( ui.item ) {
					return;
				}

				// Search for a match (case-insensitive)
				var value = this.input.val(),
					valueLowerCase = value.toLowerCase(),
					valid = false;
				this.element.children( "option" ).each(function() {
					if ( $( this ).text().toLowerCase() === valueLowerCase ) {
						this.selected = valid = true;
						return false;
					}
				});

				// Found a match, nothing to do
				if ( valid ) {
					return;
				}

				// Remove invalid value
				this.input
					.val( "" )
					.attr( "title", value + " didn't match any item" )
					.tooltip( "open" );
				this.element.val( "" );
				this._delay(function() {
					this.input.tooltip( "close" ).attr( "title", "" );
				}, 2500 );
				this.input.data( "ui-autocomplete" ).term = "";
			},

			_destroy: function() {
				this.wrapper.remove();
				this.element.show();
			}
		});
	})( jQuery );

	$(function() {
		$( "#org" ).combobox();
		$( "#toggle" ).click(function() {
			$( "#org" ).toggle();
		});
	});
	$(function() {
		$( "#emp" ).combobox();
		$( "#toggle" ).click(function() {
			$( "#emp" ).toggle();
		});
	});
	$(function() {
		$( "#prod" ).combobox();
		$( "#toggle" ).click(function() {
			$( "#prod" ).toggle();
		});
	});
	$(function() {
		$( "#d" ).combobox();
		$( "#toggle" ).click(function() {
			$( "#prod" ).toggle();
		});
	});
	

	</script>
		
	</head>

	<body>
	
		


				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
							<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Commande</a>
							</li>
							<li class="active">Modifier</li>
						</ul><!-- .breadcrumb -->

					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
							   Commande
								<small>
									<i class="icon-double-angle-right"></i>
									Modifier
								</small>
								
							</h1>
						</div><!-- /.page-header -->
 <small>
        <p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font></p> </small>
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
 <fieldset  name="Update commande" style="width:350px">
<form:form  action="${pageContext.request.contextPath}/editComm/${commande.comId}" commandName="commande" class="form-horizontal" name="formulaire" onsubmit="return valider()">
				<div class="form-group" style="width: 700px;">
									
                	<form:label class="col-sm-3 control-label no-padding-right" path="comId"></form:label>
                	<div class="col-sm-9">
                    <form path="comId"  class="col-xs-10 col-sm-5"/>
                    <span class="help-inline col-xs-12 col-sm-7">
               		 </div>
           		 </div>

								
									
         <table>
         	<tr>
         	<td>
					<input type="hidden" class="col-sm-3 control-label no-padding-right" id="listeProd" name="listeProd" value="${nbrCP }"/>
					<input type="hidden" class="col-sm-3 control-label no-padding-right" id="listeQte" name="listeQte" value=""/>
					
				
					</td>
         	</tr>
			<tr>
			
				<td>
                	<div class="form-group" >
           		<div class="ui-widget" >
                <form:label class="col-sm-3 control-label no-padding-right" path="organisation" >Organisation:</form:label>
                <div class="col-sm-9" >
             
                     <select name="org" id="org"  >
                    <option value="${commande.organisation.nom },${commande.organisation.adresse.ville},${commande.organisation.adresse.nomRue},${commande.organisation.adresse.codePostal}"> ${commande.organisation.nom }:${commande.organisation.adresse.ville},${commande.organisation.adresse.nomRue},${commande.organisation.adresse.codePostal}</option>
                    <c:forEach var="cl" items="${organisations}">
                        <option  value="${cl.nom},${cl.adresse.ville},${cl.adresse.nomRue},${cl.adresse.codePostal}">${cl.nom} : ${cl.adresse.ville} ${cl.adresse.nomRue}  ${cl.adresse.codePostal} </option>
                        
                    </c:forEach>
                </select>
               
                </div>
                </div>
                
            </div>
           		 </td>
           		 <td>
           		<div class="form-group">
                <form:label class="col-sm-3 control-label no-padding-right" path="employee" >Employ�:</form:label>
                <div class="col-sm-9">
                     <select name="emp" id="emp" >
                    <option value="${commande.employee.nom},${commande.employee.prenom},${commande.employee.cin}">${commande.employee.nom}  ${commande.employee.prenom}: ${commande.employee.cin}</option>
                    <c:forEach var="cl1" items="${employees}">
                        <option value="${cl1.nom},${cl1.prenom},${cl1.cin}">${cl1.nom}  ${cl1.prenom}: ${cl1.cin}</option>
                    </c:forEach>
                </select>
                </div>
            	</div>
           		 </td>
           	</tr>
           	<tr>
           	 	<td>
           		 <div class="form-group" >
            	 <label class="col-sm-3 control-label no-padding-right"  >Produit:</label>
           		 </div>
           		</td>
           		 <td>
           	 	<div class="form-group">
             	<label class="col-sm-3 control-label no-padding-right" >Quantit�:</label>
           	 	</div>
           		</td>
           		
           		</tr>
           		
           		<tr>
        <td colspan="2">
        
         		<table id="p">
           		<c:forEach items="${cp}" var="cp" >
           			<tr>
           		 
           		        <td style="width:370px">
           		 			<div class="form-group">
	              				<div class="col-sm-9">
               						
               						<select  id="prod1">
                   					 <option value="${cp.produit.refP}">${cp.produit.refP} : ${cp.produit.libelle}</option>
                 					 <c:forEach var="cl1" items="${produits}">
                      					<option value="${cl1.produit.refP}"> ${cl1.produit.refP} : ${cl1.produit.libelle}
                 							<c:if test="${cl1.qteStock == 0}"> : fini</c:if>
                   						</option>
                   					 </c:forEach>
               					 </select>
               
               				   </div>
            				</div>
            	
           				</td>
           				<td>
           					 <div class="form-group">
                				<div class="col-sm-9">
                   				   <input type="text" id="qte" value="${cp.qte}" class="col-xs-10 col-sm-5"/>
                  
               					</div>
           		 			</div>
           			  </td>	
           			 
           		   </tr>
           		 
           		</c:forEach>
           		
           		</table>
           		</td>
         
           </tr>
           <tr>
           		<td>
           		<b><font color="blue">Veuiller choisir le produit et la quantit� � ajouter:</font></b><br><p>
           		</td>
           		</tr>
          <tr>
					<td>
					<div class="form-group">
                <form:label class="col-sm-3 control-label no-padding-right" path="employee" >Produit:</form:label>
                <div class="col-sm-9">
                     <select  id="prod" >
                    <option value="" selected="selected">-------------</option>
                    <c:forEach var="cl1" items="${produits}">
                      <option value="${cl1.produit.refP}"> ${cl1.produit.refP} : ${cl1.produit.libelle}
                 
						<c:if test="${cl1.qteStock == 0}">
                        : fini
                        </c:if>
                        
                </option>
                    </c:forEach>
                </select>
                
                </div>
            </div>
          
					</td>
					<td>
					 
					 <div class="form-group">
                <form:label class="col-sm-3 control-label no-padding-right" path="employee" >Quantite:</form:label>
                <div class="col-sm-9">
                     <input type="text" name="quantite"/>
                
               <a href="#"  onclick="addLigne(this); return false;" class="btn btn-app btn-yellow btn-xs">
												<i class="icon-shopping-cart bigger-160" style="height: 25px"></i>
												
											</a>
                </div>
            </div>
					 
					 
					</td>
					
				</tr>
		<tr>
        <td colspan="2">
        
         		
           		
           	    	
           	
           	
</table>
<table id="nv" style="width:100%" >
         		
         		</table>
         	
 
									<div class="clearfix form-actions">
										<div class="col-md-offset-3 col-md-9">
											<button class="btn btn-info" type="submit" value="Submit" name="modifier" onclick="compteur()">
												<i class="icon-ok bigger-110"></i>
												modifier
												
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn btn-danger"" type="submit">
												<i class="icon-reply icon-only"></i>
												Annuler et retourner
											</button>
										</div>
									</div>
									
									
									
 	
									

										
                             
     </form:form></fieldset>
							
									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

		

		<!-- ace scripts -->

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->
					
						
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
