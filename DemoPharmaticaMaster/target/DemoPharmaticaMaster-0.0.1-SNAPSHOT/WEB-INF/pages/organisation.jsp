<!doctype html>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib tagdir="/WEB-INF/tags/util" prefix="util"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:35 GMT -->
<head>
	<jsp:directive.include file="/WEB-INF/pages/template.jsp" />
		
		<title>Organisation - pharma</title>

		<link href="${pageContext.request.contextPath}/resources/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/font-awesome.min.css" />

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/jquery-ui-1.10.3.custom.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/chosen.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/datepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/bootstrap-timepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/daterangepicker.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/colorpicker.css" />

		<!-- fonts -->

		
		<script type="text/javascript">
		 
function valider ( )
{
    if(document.search.typeS.value=="" && document.search.nomS.value=="" && document.search.villeS.value==""  && document.search.nomrueS.value=="")
        {  alert ( "Choisir le terme de recherche !" );
        valid = false;
        return valid;
        }
    if(document.search.villeS.value==""  && document.search.nomrueS.value!="")
    {  alert ( "Recherche avec nom de rue necessite le nom de ville !" );
    valid = false;
    return valid;
    }
 }
</script>
		
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-fonts.css" />

		

		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/assets/css/ace-skins.min.css" />

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-extra.min.js"></script>

		
	</head>

	<body>
		

				<div class="main-content">
					<div class="breadcrumbs" id="breadcrumbs">
						<script type="text/javascript">
							try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
						</script>

						<ul class="breadcrumb">
							<li>
								<i class="icon-home home-icon"></i>
								<a href="<%=request.getContextPath()+"/listeCommande"%>">Accueil</a>
							</li>

							<li>
								<a href="<%=request.getContextPath()+"/organisationn"%>">Organisation</a>
							</li>
							<li class="active">Liste</li>
						</ul><!-- .breadcrumb -->

						
					</div>

					<div class="page-content">
						<div class="page-header">
							<h1>
							    Organisation
								<small>
									<i class="icon-double-angle-right"></i>
									Liste
								</small>
								<small>
								<p  align="right"><font color="red" size="3" face="Georgia, Arial" >
                    <c:out value="${message}"/></font></p>
                    </small> 
							</h1>
						</div><!-- /.page-header -->
						
						
						
				
		
		
		<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								<div class="col-xs-12 col-sm-6 widget-container-span" style="width: 100%">
		
		
		<div class="widget-box">
											<div class="widget-header widget-header-large">
												<h4>Recherche organisation</h4>

												<div class="widget-toolbar">
													
 												<a href="#" data-action="collapse">
														<i class="icon-chevron-up"></i>
													</a>

													
												</div>
											</div>

												<div class="widget-body" style="height:200px">
												<div class="widget-main" >
												
												<form:form method="get" commandName="page"  name="search"  onsubmit="return valider()"  action="searchOrg"  class="form-horizontal">
														<table>
														<tr>
														
											<td>Type:
											<select name="typeS" id="typeS" >
												<option value="${typeS}">${typeS}</option>
                                   				<option value="fournisseur">  fournisseur </option>
                                   				<option value="pharmacie">  pharmacie </option>
                                   			</select>
                                   			
															
										</td>
										<td>
											<input type="text" name="nomS" value="${nomS}"   id="nomS" class="form-control search-query" placeholder="recherche par nom"/>
										</td>
										<td>
											<input type="text" name="villeS" value="${villeS}"   id="villeS" class="form-control search-query" placeholder="recherche par ville"/>
										</td>
										<td>
											<input type="text" name="nomrueS" value="${nomrueS}"   id="nomrueS" class="form-control search-query" placeholder="recherche par rue"/>
										</td>
										
										
														</tr>
														
														
														<tr>
														<td colspan="4">
															<div class="clearfix form-actions" >
										<div class="col-md-offset-3 col-md-9" style="width:100%; "  >
											<button class="btn btn-info" type="submit" value="Submit">
												<i class="icon-search icon-on-right bigger-110"></i>
											Rechercher
												
											</button>

											&nbsp; &nbsp; &nbsp;
											<button class="btn" type="reset">
												<i class="icon-undo bigger-110"></i>
												annuler
											</button>
										</div>
									</div></td>
														
														</tr>
														</table>
							</form:form>
													
												</div>
											</div>
										</div>
										</div>						

						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->
	
 <c:if test="${!empty page}">
                <h3 class="header smaller lighter blue"></h3>
				<div class="table-header">
										Listes des 
										</div>
										
			<table id="sample-table-2" class="table table-striped table-bordered table-hover dataTable" aria-describedby="sample-table-2_info">
				<thead>
												<tr>
		                <th>Id</th>
                        <th>Nom Organisation</th>
                        <th>Tel Fix</th>
                        <th>Email</th>
                         <th>Fax</th>
                          
                        <th>Adresse</th>
                         <th>&nbsp;</th>
                    </tr>
				</thead>

												
											<tbody role="alert" aria-live="polite" aria-relevant="all">
											<c:forEach items="${page.content}" var="organisation" varStatus="loopStatus">
											<tr class="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}">
											
														

														<td class=" ">
															${organisation.orgId}
														</td>
														<td class=" ">${organisation.nom}</td>
														<td class="hidden-480 ">${organisation.telfix}</td>
														<td class="center  sorting_1">
														${organisation.email}
														</td>
														<td>
															${organisation.fax}
														</td>
														<td class=" ">${organisation.adresse.nomRue}  ${organisation.adresse.ville}  ${organisation.adresse.codePostal}				
					</td>

					<td>
					
					
					<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="detailsOrg/${organisation.orgId}" method="get">
								
								<button class="btn btn-xs btn-success">
							    <i class="icon-ok bigger-120"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
						
						</form>
					</div>
					
							
														</td>
														<c:if test="${username == 'admin'}">
														<td class=" ">
					<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
						<form action="updateOrg/${organisation.orgId}" method="get">
								
								<button class="btn btn-xs btn-info">
							    <i class="icon-edit bigger-120"></i>
								</button>
								
								<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																		<li>
																			<a href="#" class="tooltip-info" data-rel="tooltip" title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
																			</a>
																		</li>
																     </ul>
                                       </div>																	
								 </div>
								                                          
								 
								 
								 
								 
                         
						</form>
					</div>
																
					</td></c:if>
<c:if test="${username == 'admin'}">
					<td class="hidden-480 ">
														
						<form action="deleteOrg/${organisation.orgId}" method="post">
							<div class="visible-md visible-lg hidden-sm hidden-xs btn-group">
                               
							         <button class="btn btn-xs btn-danger">
																	<i class="icon-trash bigger-120"></i>
								
																</button>
							</div>
							<div class="visible-xs visible-sm hidden-md hidden-lg">
                                     <div class="inline position-relative">
									 <button class="btn btn-minier btn-primary dropdown-toggle" data-toggle="dropdown">
																		<i class="icon-cog icon-only bigger-110"></i>
																	</button>
																	<ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
																	<li>
																			<a href="#" class="tooltip-error" data-rel="tooltip" title="Delete">
																				<span class="red">
																					<i class="icon-trash bigger-120"></i>
																				</span>
																			</a>
																	</li>
																	</ul>
								</div>
                                </div>								
						</form>
					</td>
</c:if>
														
				</tr>
													
											</c:forEach>
					</tbody>						
             </table>
               <div class="row-fluid">
			<util:pagination thispage="${page}"></util:pagination></div>
    </c:if>
					
									

										
                             
                       
							
									

								
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div><!-- /.main-content -->
				
			
			
			
			<script type="text/javascript">
			window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-2.0.3.min.js'>"+"<"+"/script>");
		</script>

		<!-- <![endif]-->

		<!--[if IE]>
<script type="text/javascript">
 window.jQuery || document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif]-->

		<script type="text/javascript">
			if("ontouchend" in document) document.write("<script src='${pageContext.request.contextPath}/resources/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
		</script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/bootstrap.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/typeahead-bs2.min.js"></script>

		

		<script src="${pageContext.request.contextPath}/resources/assets/js/ace-elements.min.js"></script>
		<script src="${pageContext.request.contextPath}/resources/assets/js/ace.min.js"></script>

		<!-- inline scripts related to this page -->

						
	</body>

<!-- Mirrored from 198.74.61.72/themes/preview/ace/form-elements.html by HTTrack Website Copier/3.x [XR&CO'2013], Mon, 10 Feb 2014 15:11:55 GMT -->
</html>
